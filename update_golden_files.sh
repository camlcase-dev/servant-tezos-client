#!/bin/bash

mkdir -p golden/

curl -s 127.0.0.1:9323/chains/main/blocks | jq '.' --sort-keys > golden/getBlockchainHeads.json

curl -s 127.0.0.1:9323/chains/main/blocks/head/context/contracts/KT1G2D45tpJ9f1iGVwQHqvupv2syhvMqeWPe/storage | jq '.' --sort-keys > golden/getContractStorage.json

curl -s 127.0.0.1:9323/chains/main/blocks/head/header | jq '.' --sort-keys > golden/getBlockHeader.json


cat transactionBlock7.json | jq '.'  --sort-keys > transactionBlock8.json


curl -s https://mainnet.smartpy.io/chains/main/blocks/head | jq '.' --sort-keys > golden/getBlock.json


cat getBlock7.json | jq '.'  --sort-keys > getBlock8.json


curl -s https://mainnet.smartpy.io/chains/main/blocks/1288177 | jq '.' --sort-keys > golden/getBlock10.json
