# Changelog for servant-tezos-client

## 0.4.1.0 -- 2021-04-27
* Support new VotingPeriodKind constructor Adoption.

## 0.4.0.0 -- 2021-02-12
* Support edo.

## 0.3.0.0 -- 2021-01-21
* More mutez functions.

## 0.2.6.0 -- 2021-01-15
* Add missing EMPTY_BIG_MAP to micheline and related tests.

## 0.2.5.0 -- 2020-11-19
* Add and test parseBytes function for ContractId.

## 0.2.4.0 -- 2020-11-19
* Change destination field value from PublicKey to ContractId.

## 0.2.3.0 -- 2020-11-19
* In Micheline.Expression, change CodeExpression to Expressions and ContractExpression to Expression.

## 0.2.2.0 -- 2020-11-19
* Remove managerPubkey from OperationOrigination. It no longer exists.

## 0.2.1.0 -- 2020-11-19
* Add DIG, DUG and APPLY.

## 0.2.0.0 -- 2020-11-19
* Update Block types to be compatible with delphinet.

## 0.1.1.0 -- 2020-10-27
* Add sample-app.
* Update lts to 16.19 and clean up compile errors.
