{-# LANGUAGE OverloadedStrings #-}

module Main where

-- base
import Data.Proxy (Proxy(Proxy))

-- network
import Network.HTTP.Client (Manager, newManager, defaultManagerSettings)
import Servant.Client (BaseUrl(BaseUrl), Scheme(Http), ClientM, client, mkClientEnv, runClientM)

-- servant-tezos-client
import Tezos.Client.Protocol.Alpha.Context (GetContractStorage)
import Tezos.Client.Protocol.Alpha (GetBlock)
import Tezos.Client.Types
  (ChainId(ChainIdMain), ContractId(ContractId), BlockId(BlockIdHead)
  , Block, Expression, Unistring(UnistringText))

getBlock :: ChainId -> BlockId -> ClientM Block
getBlock chainId = client (Proxy :: Proxy GetBlock) chainId

getContractStorage :: ChainId -> BlockId -> ContractId -> ClientM Expression
getContractStorage chainId blockId = client (Proxy :: Proxy GetContractStorage) chainId blockId

main :: IO ()
main = do
  let tezosNodeScheme = Http -- Https
      tezosNodeHost   = "127.0.0.1"
      tezosNodePort   = 9323
  
  manager <- newManager defaultManagerSettings :: IO Manager
  let tezosNodeClientEnv = mkClientEnv manager (BaseUrl tezosNodeScheme tezosNodeHost tezosNodePort "")

  -- this just changes the order of parameters for convenience
  -- instead of `runClientM (getBlock ChainIdMain BlockIdHead) tezosNodeClientEnv` you can call
  -- `runClientMWithTezosNodeClientEnv (getBlock ChainIdMain BlockIdHead)`
  let runClientMWithTezosNodeClientEnv = flip runClientM tezosNodeClientEnv

  headBlock <- runClientMWithTezosNodeClientEnv $ getBlock ChainIdMain BlockIdHead
  print headBlock

  tzBTCDexterStorageHead <- runClientMWithTezosNodeClientEnv $ getContractStorage ChainIdMain BlockIdHead (ContractId $ UnistringText "KT1DrJV8vhkdLEj76h1H9Q4irZDqAkMPo1Qf")
  print tzBTCDexterStorageHead
