{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Aeson.Hspec where

import Control.Exception

import Data.Aeson
import Data.Aeson.Encode.Pretty (encodePretty', defConfig, Config(confCompare, confIndent,confTrailingNewline), Indent(Spaces))
import Data.ByteString.Lazy (readFile, writeFile)
import Data.Typeable
import Prelude hiding (readFile, writeFile)

import System.Directory (doesFileExist)

import Test.Hspec
import Test.Hspec.QuickCheck

import Test.QuickCheck (Arbitrary(arbitrary))

-- | put brackets around a String.
addBrackets :: String -> String
addBrackets s =
  if ' ' `elem` s
    then "(" ++ s ++ ")"
    else s

-- | Used to eliminate the need for an Eq instance
newtype JsonShow a = JsonShow a 

instance ToJSON a => Show (JsonShow a) where 
  show (JsonShow v) = show . encode $ v 

instance ToJSON a => ToJSON (JsonShow a) where
  toJSON (JsonShow a) = toJSON a

instance FromJSON a => FromJSON (JsonShow a) where
  parseJSON v = JsonShow <$> (parseJSON v)

instance Arbitrary a => Arbitrary (JsonShow a) where
  arbitrary = JsonShow <$> arbitrary 

-- | encode and decode a value, then test the encoded against the decoded form.
checkAesonEncodingEquality :: forall a . (ToJSON a, FromJSON a) => JsonShow a -> Bool
checkAesonEncodingEquality (JsonShow a) =  
  let byteStrA       = encode a
      decodedVal     = (eitherDecode byteStrA) :: Either String a
      eitherByteStrB = encode <$> decodedVal  
  in (Right byteStrA) == eitherByteStrB

-- | roundtrip aeson test
aesonEqualitySpec :: forall a .
  (Arbitrary a, Typeable a, ToJSON a, FromJSON a) =>
  Proxy a -> Spec
aesonEqualitySpec proxy = do
  let typeIdentifier = show (typeRep proxy)
      checkAesonEncodingEquality' :: JsonShow a -> Bool
      checkAesonEncodingEquality' = checkAesonEncodingEquality
  
  describe ("JSON encoding of " ++ addBrackets (typeIdentifier)) $
    prop "allows to encode values with aeson and read them back"  
          (checkAesonEncodingEquality')

-- |
checkGoldenFile :: forall a. (ToJSON a, FromJSON a) => Proxy a -> FilePath -> IO ()
checkGoldenFile Proxy filePath = do
  file    <- readFile filePath
  decoded :: a <-
    either (throwIO . ErrorCall) pure $ eitherDecode' file
  if encodePretty decoded == file
    then pure ()
    else do
    writeComparisonFile decoded
    expectationFailure $ "The serialization does not match. Compare golden file with " ++ faultyReencodedFilePath ++ "."
  where
    encodePretty = encodePretty' (defConfig { confIndent = Spaces 2, confTrailingNewline = True, confCompare = compare})
    faultyReencodedFilePath = filePath ++ ".faulty"
    writeComparisonFile decoded = do
      writeFile faultyReencodedFilePath (encodePretty decoded)
      putStrLn $
        "\n" ++
        "INFO: Written the reencoded goldenFile into " ++ faultyReencodedFilePath ++ "."    

-- |  
checkGoldenFileSpec :: forall a. (Typeable a, ToJSON a, FromJSON a) => Proxy a -> FilePath -> Spec
checkGoldenFileSpec proxy filePath = do
  let typeIdentifier = show (typeRep proxy)  
  describe ("JSON encoding of " ++ addBrackets  (typeIdentifier)) $
    it ("produces the same JSON as is found in " ++ filePath) $ do
      exists <- doesFileExist filePath
      if exists
        then checkGoldenFile proxy filePath
        else expectationFailure $ "The file path " ++ filePath ++ " does not exist."
