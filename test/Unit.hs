{-# LANGUAGE OverloadedStrings   #-}

module Unit where

-- string
import Data.Text (Text)

-- tezos
import Tezos.Client.Types.Core (ContractId(..), Unistring(..), parseBytes)

-- testing
import Test.Hspec (it, describe, shouldBe, Spec)

sampleAddresses :: [(Text, Text)]
sampleAddresses =
  [ ("0000dac9f52543da1aed0bc1d6b46bf7c10db7014cd6", "tz1faswCTDciRzE4oJ9jn2Vm2dvjeyA9fUzU")
  , ("000079943a60100e0394ac1c8f6ccfaeee71ec9c2d94", "tz1Wit2PqodvPeuRRhdQXmkrtU8e8bRYZecd")
  , ("000145b5e7d31bf6612e61ebfa7a6d929ce7800a55a4", "tz2EfqCbLmpfv7mNiLcMmhxAwdgHtPTcwR4W")
  , ("000139ccf913874519b2d20917bf5f2de537420d2726", "tz2Darj3LyQzekU98ZK8diHvuyn1YYjcHpc6")
  , ("00025cfa532f50de3e12befc0ad21603835dd7698d35", "tz3UoffC7FG7zfpmvmjUmUeAaHvzdcUvAj6r")
  , ("000214fa2b36471a318d2f244997c48c5f23b8001eed", "tz3NExpXn9aPNZPorRE4SdjJ2RGrfbJgMAaV")
  , ("0000861299624c9a3b52be10762c64bac282b1c02316", "tz1XrwX7i9Nzh8e6UmG3VnFkAeoyWdTqDf3U")
  , ("01521139f84791537d54575df0c74a8084cc68861c00", "KT1G4hcQj2STN86GwC1XAkPtwPPhgfPKuE45")
  , ("01afab866e7f1e74f9bba388d66b246276ce50bf4700", "KT1QbdJ7M7uAQZwLpvzerUyk7LYkJWDL7eDh")
  ]

spec :: Spec
spec =
  describe "unit tests" $ do  
    it "parseBytes" $ do
      mapM_ (\(bytes, contract) -> parseBytes bytes `shouldBe` (Just . ContractId . UnistringText $ contract)) sampleAddresses
      
