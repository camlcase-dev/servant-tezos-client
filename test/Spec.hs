{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications    #-}
{-# LANGUAGE TypeInType          #-}

module Main where

import Aeson.Hspec (aesonEqualitySpec, checkGoldenFileSpec)
import Arbitrary ()
import Data.Proxy (Proxy(Proxy))
import Test.Hspec (hspec, parallel)
import Tezos.Client.Types
  (Ballot, Bignum, BigMapDiff, BlockHash, BlockId, ChainId, ContextHash, ContractHash,
   ContractId, CycleNonce, Entrypoint, Error, Expression, Fitness, IdPoint, Int64AsString, Mutez,
   OperationHash, PositiveBignum, ProtocolHash, PeerId, PublicKeyHash, ScriptExpr, Unistring)
import Tezos.Client.Types.Block
  (BalanceUpdate, Block, BlockHeaderLevel, BlockHeaderMetadata, Delegation, FullHeader
  , InternalOperationResult, InternalOperationResultsMetadata
  , MaxOperationLength, OperationTransaction, OperationOrigination
  , OperationDelegation, Operation, OperationReveal, OperationBallot, OperationProposals, OperationActivateAccount
  , OperationSeedNonceRevelation, OperationDoubleEndorsementEvidence, OperationDoubleBakingEvidence
  , OperationContents, OperationContentsAndResult, OperationEndorsementMetadata
  , Origination, RawBlockHeader, Reveal, TestChainStatus, Transaction)
import Tezos.Client.Types.BlockHeader (BlockHeader)
import Tezos.Client.Types.Vote
  (Vote, BallotCount, VotingPeriodKind, DelegateWeight)

import qualified Tezos.Client.Types.Micheline as Micheline
import qualified Unit as Unit

main :: IO ()
main = do
  hspec $ do
    Unit.spec
    
    parallel $ do
      
      
      aesonEqualitySpec $ Proxy @[[BlockHash]]
      checkGoldenFileSpec (Proxy @[[BlockHash]]) "golden/getBlockchainHeads.json"

      -- Tezos.Client.Types.Micheline
      aesonEqualitySpec $ Proxy @Expression
      checkGoldenFileSpec (Proxy @Expression) "golden/getContractStorage.json"

      -- Tezos.Client.Types.BlockHeader
      aesonEqualitySpec $ Proxy @BlockHeader
      checkGoldenFileSpec (Proxy @BlockHeader) "golden/getBlockHeader.json"

      -- Tezos.Client.Types.BlockHeader
      aesonEqualitySpec $ Proxy @Block
      checkGoldenFileSpec (Proxy @Block) "golden/getBlock.json"
      checkGoldenFileSpec (Proxy @Block) "golden/getBlock2.json"
      checkGoldenFileSpec (Proxy @Block) "golden/getBlock3.json"
      checkGoldenFileSpec (Proxy @Block) "golden/getBlock4.json"
      checkGoldenFileSpec (Proxy @Block) "golden/getBlock5.json"
      checkGoldenFileSpec (Proxy @Block) "golden/getBlock6.json"
      checkGoldenFileSpec (Proxy @Block) "golden/getBlock7.json"
      checkGoldenFileSpec (Proxy @Block) "golden/getBlock8.json"
      -- block 1168849
      checkGoldenFileSpec (Proxy @Block) "golden/getBlock9.json"      
      -- block 1288177
      checkGoldenFileSpec (Proxy @Block) "golden/getBlock10.json"
      checkGoldenFileSpec (Proxy @Block) "golden/getBlock11.json"
      
      checkGoldenFileSpec (Proxy @Block) "golden/transactionBlock.json"
      checkGoldenFileSpec (Proxy @Block) "golden/transactionBlock2.json"
      checkGoldenFileSpec (Proxy @Block) "golden/transactionBlock7.json"


      -- Tezos.Client.Types.Micheline
      checkGoldenFileSpec (Proxy @Micheline.Parameters) "golden/parameters0.json"

      -- Tezos.Client.Types.Core
      aesonEqualitySpec $ Proxy @Ballot
      aesonEqualitySpec $ Proxy @BigMapDiff
      aesonEqualitySpec $ Proxy @Bignum
      aesonEqualitySpec $ Proxy @BlockHash
      aesonEqualitySpec $ Proxy @BlockId
      aesonEqualitySpec $ Proxy @ChainId
      aesonEqualitySpec $ Proxy @ContextHash
      aesonEqualitySpec $ Proxy @ContractId
      aesonEqualitySpec $ Proxy @ContractHash
      aesonEqualitySpec $ Proxy @CycleNonce
      aesonEqualitySpec $ Proxy @Entrypoint
      aesonEqualitySpec $ Proxy @Error
      aesonEqualitySpec $ Proxy @Fitness
      aesonEqualitySpec $ Proxy @IdPoint
      aesonEqualitySpec $ Proxy @Int64AsString
      aesonEqualitySpec $ Proxy @Mutez
      aesonEqualitySpec $ Proxy @OperationHash
      aesonEqualitySpec $ Proxy @PeerId
      aesonEqualitySpec $ Proxy @PositiveBignum
      aesonEqualitySpec $ Proxy @ProtocolHash
      aesonEqualitySpec $ Proxy @PublicKeyHash
      aesonEqualitySpec $ Proxy @ScriptExpr
      aesonEqualitySpec $ Proxy @Unistring

      aesonEqualitySpec $ Proxy @Block
      aesonEqualitySpec $ Proxy @BalanceUpdate
      aesonEqualitySpec $ Proxy @BlockHeaderLevel
      aesonEqualitySpec $ Proxy @BlockHeaderMetadata
      aesonEqualitySpec $ Proxy @Delegation
      aesonEqualitySpec $ Proxy @FullHeader
      aesonEqualitySpec $ Proxy @InternalOperationResult
      aesonEqualitySpec $ Proxy @(InternalOperationResultsMetadata Int)
      aesonEqualitySpec $ Proxy @MaxOperationLength
      aesonEqualitySpec $ Proxy @OperationSeedNonceRevelation
      aesonEqualitySpec $ Proxy @OperationDoubleEndorsementEvidence
      aesonEqualitySpec $ Proxy @OperationDoubleBakingEvidence      
      aesonEqualitySpec $ Proxy @OperationActivateAccount
      aesonEqualitySpec $ Proxy @OperationProposals
      aesonEqualitySpec $ Proxy @OperationBallot
      aesonEqualitySpec $ Proxy @OperationReveal
      aesonEqualitySpec $ Proxy @OperationTransaction
      aesonEqualitySpec $ Proxy @OperationOrigination
      aesonEqualitySpec $ Proxy @OperationDelegation
      aesonEqualitySpec $ Proxy @Operation
      aesonEqualitySpec $ Proxy @OperationContents
      aesonEqualitySpec $ Proxy @OperationContentsAndResult
      aesonEqualitySpec $ Proxy @OperationEndorsementMetadata
      aesonEqualitySpec $ Proxy @Origination
      -- aesonEqualitySpec $ Proxy @Parameters
      aesonEqualitySpec $ Proxy @RawBlockHeader
      aesonEqualitySpec $ Proxy @Reveal
      aesonEqualitySpec $ Proxy @TestChainStatus
      aesonEqualitySpec $ Proxy @Transaction

      aesonEqualitySpec $ Proxy @Vote
      aesonEqualitySpec $ Proxy @BallotCount
      aesonEqualitySpec $ Proxy @VotingPeriodKind
      aesonEqualitySpec $ Proxy @DelegateWeight
