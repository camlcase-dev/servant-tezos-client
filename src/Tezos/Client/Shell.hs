{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeOperators #-}

module Tezos.Client.Shell
  ( module Tezos.Client.Shell
  , module Tezos.Client.Shell.Chain
  , module Tezos.Client.Shell.Injection
  , module Tezos.Client.Shell.Monitor
  , module Tezos.Client.Shell.Network
  , module Tezos.Client.Shell.Protocols
  , module Tezos.Client.Shell.Workers  
  ) where

import Data.Aeson (Value)
import Servant.API
import Tezos.Client.Types

import Tezos.Client.Shell.Chain
import Tezos.Client.Shell.Injection
import Tezos.Client.Shell.Monitor
import Tezos.Client.Shell.Network
import Tezos.Client.Shell.Protocols
import Tezos.Client.Shell.Workers

-- | Schema for all the RPC errors from the shell
--
-- GET /errors
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-errors
type GetErrors = "errors" :> Get '[JSON] Value


-- | Fetch a protocol from the network.
--
-- GET /fetch_protocol/<Protocol_hash>
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-fetch-protocol-protocol-hash
type GetFetchProtocol = "fetch_protocol" :> Capture "protocol-hash" ProtocolHash :> Get '[JSON] Value

-- | Gets stats from the OCaml Garbage Collector
-- 
-- GET /stats/gc
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-stats-gc

-- | Gets memory usage stats
--
-- GET /stats/memory
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-stats-memory
