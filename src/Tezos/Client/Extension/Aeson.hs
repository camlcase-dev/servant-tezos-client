module Tezos.Client.Extension.Aeson
  ( mkPairOnlyIfValueIsJust
  , (.=?)
  , withInteger
  , combineValues
  ) where

import Data.Aeson (ToJSON, (.=), withScientific)
import Data.Aeson.Types (Pair, Parser, Value(Object,Array,String),typeMismatch)
import qualified Data.Scientific
import Data.Text  (Text)

-- | avoid {"key":null} if Maybe is Nothing
mkPairOnlyIfValueIsJust :: ToJSON v => Text -> Maybe v -> Maybe Pair
mkPairOnlyIfValueIsJust = (.=?)

(.=?) :: ToJSON v => Text -> Maybe v -> Maybe Pair
(.=?) name mValue =
  case mValue of
    Nothing    -> Nothing
    Just value -> Just $ name .= value
infixr 8 .=?
{-# INLINE (.=?) #-}

withInteger
  :: (Bounded n, Integral n)
  => String
  -> (n -> Parser a)
  -> Value
  -> Parser a
withInteger expected f x = withScientific expected f' x
  where
    f' = maybe (typeMismatch expected x) f . Data.Scientific.toBoundedInteger

-- | combine aeson values only if they are the same constructor and are
-- semigroups.
combineValues :: Value -> Value -> Value
combineValues (Object o1) (Object o2) = Object $ o1 <> o2
combineValues (Array  o1) (Array  o2) = Array  $ o1 <> o2
combineValues (String o1) (String o2) = String $ o1 <> o2
combineValues v1 _v2 = v1
