{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE OverloadedLists       #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeOperators         #-}

module Tezos.Client.Extension.Servant
  ( RealJSON
  ) where

import           Data.Aeson       (ToJSON, FromJSON(parseJSON), encode)
import           Data.Aeson.Types (parseEither)
import           Data.ByteString.Lazy (ByteString)
import qualified Data.Aeson.Parser
import           Data.Attoparsec.ByteString.Char8 (endOfInput, parseOnly, skipSpace, (<?>))
import           Data.String.Conversions (cs)
import           Network.HTTP.Media ((//))
import           Servant.API
  (Accept(contentTypes), MimeRender(mimeRender), MimeUnrender(mimeUnrender))

data RealJSON

instance Accept RealJSON where
    contentTypes _ =
      [ "application" // "json" ]

instance FromJSON a => MimeUnrender RealJSON a where
    mimeUnrender _ = eitherDecodeLenient

instance {-# OVERLAPPABLE #-}
         ToJSON a => MimeRender RealJSON a where
    mimeRender _ = encode

eitherDecodeLenient :: FromJSON a => ByteString -> Either String a
eitherDecodeLenient input =
    parseOnly parser (cs input) >>= parseEither parseJSON
  where
    parser = skipSpace
          *> Data.Aeson.Parser.value
          <* skipSpace
          <* (endOfInput <?> "trailing junk after valid JSON")
