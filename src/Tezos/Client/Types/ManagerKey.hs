{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings     #-}

module Tezos.Client.Types.ManagerKey where

import Data.Aeson
  (ToJSON(toJSON), FromJSON(parseJSON), (.=), (.:), (.:?), withObject, object)
import Data.Maybe (catMaybes)
import GHC.Generics (Generic)
import Tezos.Client.Types.Crypto
import Tezos.Client.Extension.Aeson ((.=?))

-- { "manager": $Signature.Public_key_hash,
--   "key"?: $Signature.Public_key }

data ManagerKey =
  ManagerKey
    { manager :: PublicKeyHash
    , key     :: Maybe PublicKey
    } deriving (Eq, Show, Generic)

instance ToJSON ManagerKey where
  toJSON (ManagerKey manager' key') =
    object $
      [ "manager" .= manager'
      ] ++ catMaybes ["key" .=? key']

instance FromJSON ManagerKey where
  parseJSON = withObject "ManagerKey" $ \o ->
    ManagerKey <$> o .:  "manager"
               <*> o .:? "key"
