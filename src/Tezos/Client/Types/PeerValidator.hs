{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings     #-}

module Tezos.Client.Types.PeerValidator where

import Data.Aeson
  (FromJSON(parseJSON), ToJSON(toJSON), Value, (.=), (.:), (.:?), object,
   withObject)
import Data.Aeson.Types (Parser)
import Data.Maybe (catMaybes)
import Data.Text (Text)
import Data.Word (Word16)
import GHC.Generics (Generic)
import Tezos.Client.Types.BlockValidator
import Tezos.Client.Types.Core
import qualified Data.HashMap.Lazy as HM

import Tezos.Client.Extension.Aeson ((.=?))

-- [ { "peer_id": $Crypto_box.Public_key_hash,
--     "status":
--       { "phase": "launching",
--         "since": $timestamp }
--       || { "phase": "running",
--            "since": $timestamp }
--       || { "phase": "closing",
--            "birth": $timestamp,
--            "since": $timestamp }
--       || { "phase": "closed",
--            "birth": $timestamp,
--            "since": $timestamp }
--       || { "phase": "crashed",
--            "birth": $timestamp,
--            "since": $timestamp,
--            "errors": $error } } ... ]

data PeerValidatorMini =
  PeerValidatorMini
    { peerId :: PeerId
    , status  :: BlockValidatorStatus
    } deriving (Eq, Show, Generic)

instance ToJSON PeerValidatorMini where
  toJSON (PeerValidatorMini peerId' status') =
    object
      [ "peer_id" .= peerId'
      , "status"  .= status'
      ]

instance FromJSON PeerValidatorMini where
  parseJSON = withObject "PeerValidatorMini" $ \o ->
    PeerValidatorMini <$> o .: "peer_id"
                      <*> o .: "status"

-- { "status":
--     { "phase": "launching",
--       "since": $timestamp }
--     || { "phase": "running",
--          "since": $timestamp }
--     || { "phase": "closing",
--          "birth": $timestamp,
--          "since": $timestamp }
--     || { "phase": "closed",
--          "birth": $timestamp,
--          "since": $timestamp }
--     || { "phase": "crashed",
--          "birth": $timestamp,
--          "since": $timestamp,
--          "errors": $error },
--   "pending_requests":
--     [ { "pushed": $timestamp,
--         "request":
--           { "request": "new_head",
--             "block": $block_hash }
--           || { "request": "new_branch",
--                "block": $block_hash,
--                "locator_length": integer ∈ [0, 2^16-1] } } ... ],
--   "backlog":
--     [ { "level":
--           "info" | "debug" | "error" | "fatal" | "warning" | "notice",
--         "events":
--           [ { "message": $unistring }
--             || { "request":
--                    { "request": "new_head",
--                      "block": $block_hash }
--                    || { "request": "new_branch",
--                         "block": $block_hash,
--                         "locator_length": integer ∈ [0, 2^16-1] },
--                  "status":
--                    { "pushed": $timestamp,
--                      "treated": $timestamp,
--                      "completed": $timestamp } }
--             || { "error": $error,
--                  "failed_request":
--                    { "request": "new_head",
--                      "block": $block_hash }
--                    || { "request": "new_branch",
--                         "block": $block_hash,
--                         "locator_length": integer ∈ [0, 2^16-1] },
--                  "status":
--                    { "pushed": $timestamp,
--                      "treated": $timestamp,
--                      "completed": $timestamp } } ... ] } ... ],
--   "current_request"?:
--     { "pushed": $timestamp,
--       "treated": $timestamp,
--       "request":
--         { "request": "new_head",
--           "block": $block_hash }
--         || { "request": "new_branch",
--              "block": $block_hash,
--              "locator_length": integer ∈ [0, 2^16-1] } } }

data PeerValidatorBacklogEvent
  = PVBEMessage { message :: Text }
  | PVBERequest { blockValidatorRequest :: PeerValidatorRequest, status :: BacklogStatus}
  | PVBEError { blockValidatorRequest :: PeerValidatorRequest, status :: BacklogStatus, error :: Value }
  deriving (Eq, Show, Generic)

instance ToJSON PeerValidatorBacklogEvent where
  toJSON (PVBEMessage message') = object [ "message" .= message' ]
  toJSON (PVBERequest request' status') = object ["request" .= request', "status" .= status']
  toJSON (PVBEError request' status' error') = object ["failed_request" .= request', "status" .= status', "error" .= error']
 
instance FromJSON PeerValidatorBacklogEvent where
  parseJSON = withObject "PeerValidatorBacklogEvent" $ \o -> do
    let hasMessage             = HM.member "message" o
        hasSuccesfulValidation = HM.member "request" o
        hasFailedValidation    = HM.member "failed_request" o
        parsePeerValidatorBacklogEvent
          | hasMessage             = PVBEMessage <$> o .: "message"
          | hasSuccesfulValidation = PVBERequest <$> o .: "request" <*> o .: "status"
          | hasFailedValidation    = PVBEError   <$> o .: "failed_request" <*> o .: "status" <*> o .: "error"
          | otherwise = fail $ "expected PeerValidatorBacklogEvent, encountered " ++ (show o)
    parsePeerValidatorBacklogEvent


data PeerValidatorBacklog =
  PeerValidatorBacklog
    { level :: Text
    , events :: [PeerValidatorBacklogEvent]
    } deriving (Eq, Show, Generic)

instance ToJSON PeerValidatorBacklog
instance FromJSON PeerValidatorBacklog

data PeerValidatorRequest
  = PVRNewHead { block :: BlockHash }
  | PVRNewBranch { block :: BlockHash, locatorLength :: Word16 }
  deriving (Eq, Show, Generic)

instance ToJSON PeerValidatorRequest where
  toJSON (PVRNewHead block') =
    object
      [ "request" .= ("new_head" :: Text)
      , "block"   .= block'
      ]
  toJSON (PVRNewBranch block' locatorLength') =
    object
      ["request"         .= ("new_branch" :: Text)
      , "block"          .= block'
      , "locator_length" .= locatorLength'
      ]

instance FromJSON PeerValidatorRequest where
  parseJSON = withObject "PeerValidatorRequest" $ \o -> do    
    let match s =
          case s of
            "new_head"   -> PVRNewHead   <$> o .: "block"
            "new_branch" -> PVRNewBranch <$> o .: "block" <*> o .: "locator_length"
            _ -> fail $ "expected PeerValidatorRequest, encountered " ++ (show s)
    request' <- (o .: "request") :: Parser Text
    match request'
    
data PeerValidatorPendingRequest =
  PeerValidatorPendingRequest
    { pushed :: Timestamp
    , request :: PeerValidatorRequest
    } deriving (Eq, Show, Generic)

instance ToJSON PeerValidatorPendingRequest
instance FromJSON PeerValidatorPendingRequest

data PeerValidatorCurrentRequests =
  PeerValidatorCurrentRequests
    { pushed  :: Timestamp
    , treated :: Timestamp
    , request :: PeerValidatorRequest
    } deriving (Eq, Show, Generic)

instance ToJSON PeerValidatorCurrentRequests
instance FromJSON PeerValidatorCurrentRequests

data PeerValidator =
  PeerValidator
    { status          :: BlockValidatorStatus
    , pendingRequests :: [PeerValidatorPendingRequest]
    , backlog         :: [PeerValidatorBacklog]
    , currentRequests :: Maybe PeerValidatorCurrentRequests
    } deriving (Eq, Show, Generic)

instance ToJSON PeerValidator where
  toJSON (PeerValidator status' pending_request' backlog' currentRequests') =
    object $
      [ "status"           .=  status'
      , "pending_requests" .=  pending_request'
      , "backlog"          .=  backlog'
      ] ++ catMaybes 
      [ "current_requests" .=? currentRequests'
      ]

instance FromJSON PeerValidator where
  parseJSON = withObject "PeerValidator" $ \o ->
    PeerValidator <$> o .:  "status"
                  <*> o .:  "pending_requests"
                  <*> o .:  "backlog"
                  <*> o .:? "current_requests"
