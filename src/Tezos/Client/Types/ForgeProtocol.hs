{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings     #-}

module Tezos.Client.Types.ForgeProtocol where

import Data.Aeson
  (FromJSON(parseJSON), ToJSON(toJSON), (.=), (.:), (.:?),
   object, withObject)
import Data.Maybe (catMaybes)
import Data.Text (Text)
import Data.Word (Word16)
import GHC.Generics (Generic)
import Tezos.Client.Types.Core
import Tezos.Client.Extension.Aeson ((.=?))

data ForgeProtocolData =
  ForgeProtocolData
    { priority         :: Word16
    , nonceHash        :: CycleNonce
    , proofOfWorkNonce :: Maybe Text
    } deriving (Eq, Show, Generic)

instance ToJSON ForgeProtocolData where
  toJSON (ForgeProtocolData priority' nonceHash' proofOfWorkNonce') =
    object $
      [ "priority"   .= priority'
      , "nonce_hash" .= nonceHash'
      ] ++ catMaybes ["proof_of_work_nonce" .=? proofOfWorkNonce']
      
instance FromJSON ForgeProtocolData where
  parseJSON = withObject "ForgeProtocolData" $ \o ->
    ForgeProtocolData <$> o .:  "priority"
                      <*> o .:  "nonce_hash"
                      <*> o .:? "proof_of_work_nonce"

data ForgedProtocolData =
  ForgedProtocolData
    { protocolData :: Text
    } deriving (Eq, Show, Generic)

instance ToJSON ForgedProtocolData where
  toJSON (ForgedProtocolData protocolData') =
    object ["protocol_data" .= protocolData']

instance FromJSON ForgedProtocolData where
  parseJSON = withObject "ForgedProtocolData" $ \o ->
    ForgedProtocolData <$> o .: "protocol_data"
