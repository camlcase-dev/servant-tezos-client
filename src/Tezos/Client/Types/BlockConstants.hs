{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings     #-}

module Tezos.Client.Types.BlockConstants where

import Data.Aeson
  (FromJSON(parseJSON), ToJSON(toJSON), (.=), (.:), object, withObject)
import Data.Int (Int32, Int64)
import Data.Word (Word8, Word16)
import GHC.Generics (Generic)
import Tezos.Client.Types.Core

-- { "proof_of_work_nonce_size": integer ∈ [0, 255],
--   "nonce_length": integer ∈ [0, 255],
--   "max_revelations_per_block": integer ∈ [0, 255],
--   "max_operation_data_length": integer ∈ [-2^30-2, 2^30+2],
--   "max_proposals_per_delegate": integer ∈ [0, 255],
--   "preserved_cycles": integer ∈ [0, 255],
--   "blocks_per_cycle": integer ∈ [-2^31-2, 2^31+2],
--   "blocks_per_commitment": integer ∈ [-2^31-2, 2^31+2],
--   "blocks_per_roll_snapshot": integer ∈ [-2^31-2, 2^31+2],
--   "blocks_per_voting_period": integer ∈ [-2^31-2, 2^31+2],
--   "time_between_blocks": [ $int64 ... ],
--   "endorsers_per_block": integer ∈ [0, 2^16-1],
--   "hard_gas_limit_per_operation": $bignum,
--   "hard_gas_limit_per_block": $bignum,
--   "proof_of_work_threshold": $int64,
--   "tokens_per_roll": $mutez,
--   "michelson_maximum_type_size": integer ∈ [0, 2^16-1],
--   "seed_nonce_revelation_tip": $mutez,
--   "origination_size": integer ∈ [-2^30-2, 2^30+2],
--   "block_security_deposit": $mutez,
--   "endorsement_security_deposit": $mutez,
--   "block_reward": $mutez,
--   "endorsement_reward": $mutez,
--   "cost_per_byte": $mutez,
--   "hard_storage_limit_per_operation": $bignum,
--   "test_chain_duration": $int64,
--   "quorum_min": integer ∈ [-2^31-2, 2^31+2],
--   "quorum_max": integer ∈ [-2^31-2, 2^31+2],
--   "min_proposal_quorum": integer ∈ [-2^31-2, 2^31+2],
--   "initial_endorsers": integer ∈ [0, 2^16-1],
--   "delay_per_missing_endorsement": $int64 }

data BlockConstants =
  BlockConstants
    { proofOfWorkNonceSize         :: Word8
    , nonceLength                  :: Word8
    , maxRevelationsPerBlock       :: Word8
    , maxOperationDataLength       :: Int64
    , maxProposalsPerDelegate      :: Word8
    , preservedCycles              :: Word8
    , blocksPerCycle               :: Int64
    , blocksPerCommitment          :: Int64
    , blocksPerRollSnapshot        :: Int64
    , blocksPerVotingPeriod        :: Int64
    , timeBetweenBlocks            :: [Int64]
    , endorsersPerBlock            :: Int32
    , hardGasLimitPerOperation     :: Integer
    , hardGasLimitPerBlock         :: Integer
    , proofOfWorkThreshold         :: Integer
    , tokensPerRoll                :: Mutez
    , michelsonMaximumTypeSize     :: Word16
    , seedNonceRevelationTip       :: Mutez
    , originationSize              :: Int64
    , blockSecurityDeposit         :: Mutez
    , endorsementSecurityDeposit   :: Mutez
    , blockReward                  :: Mutez
    , endorsementReward            :: Mutez
    , costPerByte                  :: Mutez
    , hardStorageLimitPerOperation :: Integer
    } deriving (Eq, Show, Generic)

instance ToJSON BlockConstants where
  toJSON (BlockConstants proofOfWorkNonceSize' nonceLength'
          maxRevelationsPerBlock' maxOperationDataLength'
          maxProposalsPerDelegate' preservedCycles' blocksPerCycle'
          blocksPerCommitment' blocksPerRollSnapshot' blocksPerVotingPeriod'
          timeBetweenBlocks' endorsersPerBlock' hardGasLimitPerOperation'
          hardGasLimitPerBlock' proofOfWorkThreshold' tokensPerRoll'
          michelsonMaximumTypeSize' seedNonceRevelationTip' originationSize'
          blockSecurityDeposit' endorsementSecurityDeposit' blockReward'
          endorsementReward' costPerByte' hardStorageLimitPerOperation') =
    object
      [ "proof_of_work_nonce_size"         .= proofOfWorkNonceSize'
      , "nonce_length"                     .= nonceLength'
      , "max_revelations_per_block"        .= maxRevelationsPerBlock'
      , "max_operation_data_length"        .= maxOperationDataLength'
      , "max_proposals_per_delegate"       .= maxProposalsPerDelegate'
      , "preserved_cycles"                 .= preservedCycles'
      , "blocks_per_cycle"                 .= blocksPerCycle'
      , "blocks_per_commitment"            .= blocksPerCommitment'
      , "blocks_per_roll_snapshot"         .= blocksPerRollSnapshot'
      , "blocks_per_voting_period"         .= blocksPerVotingPeriod'
      , "time_between_blocks"              .= timeBetweenBlocks'
      , "endorsers_per_block"              .= endorsersPerBlock'
      , "hard_gas_limit_per_operation"     .= hardGasLimitPerOperation'
      , "hard_gas_limit_per_block"         .= hardGasLimitPerBlock'  
      , "proof_of_work_threshold"          .= proofOfWorkThreshold'
      , "tokens_per_roll"                  .= tokensPerRoll'
      , "michelson_maximum_type_size"      .= michelsonMaximumTypeSize'
      , "seed_nonce_revelation_tip"        .= seedNonceRevelationTip'
      , "origination_size"                 .= originationSize'
      , "block_security_deposit"           .= blockSecurityDeposit'
      , "endorsement_security_deposit"     .= endorsementSecurityDeposit'
      , "block_reward"                     .= blockReward'
      , "endorsement_reward"               .= endorsementReward'
      , "cost_per_byte"                    .= costPerByte'
      , "hard_storage_limit_per_operation" .= hardStorageLimitPerOperation'
      ]

instance FromJSON BlockConstants where
  parseJSON = withObject "BlockConstants" $ \o ->
    BlockConstants <$> o .: "proof_of_work_nonce_size"
                   <*> o .: "nonce_length"
                   <*> o .: "max_revelations_per_block"
                   <*> o .: "max_operation_data_length"
                   <*> o .: "max_proposals_per_delegate"
                   <*> o .: "preserved_cycles"
                   <*> o .: "blocks_per_cycle"
                   <*> o .: "blocks_per_commitment"
                   <*> o .: "blocks_per_roll_snapshot"
                   <*> o .: "blocks_per_voting_period"
                   <*> o .: "time_between_blocks"
                   <*> o .: "endorsers_per_block"
                   <*> o .: "hard_gas_limit_per_operation"
                   <*> o .: "hard_gas_limit_per_block"
                   <*> o .: "proof_of_work_threshold"
                   <*> o .: "tokens_per_roll"
                   <*> o .: "michelson_maximum_type_size"
                   <*> o .: "seed_nonce_revelation_tip"
                   <*> o .: "origination_size"
                   <*> o .: "block_security_deposit"
                   <*> o .: "endorsement_security_deposit"
                   <*> o .: "block_reward"
                   <*> o .: "endorsement_reward"
                   <*> o .: "cost_per_byte"
                   <*> o .: "hard_storage_limit_per_operation"
