{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE OverloadedStrings     #-}

module Tezos.Client.Types.Contract where

import Data.Aeson (ToJSON(..), FromJSON(..), object, withObject, (.=), (.:?), (.:))
import qualified Data.HashMap.Lazy as HM
import Data.Maybe (catMaybes)
import Data.Text (Text)
import GHC.Generics (Generic)
import Tezos.Client.Types.Core
import Tezos.Client.Types.Micheline
import Tezos.Client.Extension.Aeson ((.=?))

-- "big_map_diff"?:
--   [ { /* update */
--       "action": "update",
--       "big_map": $bignum,
--       "key_hash": $script_expr,
--       "key": $micheline.alpha.michelson_v1.expression,
--       "value"?: $micheline.alpha.michelson_v1.expression }
--     || { /* remove */
--          "action": "remove",
--          "big_map": $bignum }
--     || { /* copy */
--          "action": "copy",
--          "source_big_map": $bignum,
--          "destination_big_map": $bignum }
--     || { /* alloc */
--          "action": "alloc",
--          "big_map": $bignum,
--          "key_type": $micheline.alpha.michelson_v1.expression,
--          "value_type": $micheline.alpha.michelson_v1.expression } ... ],

data BigMapDiff
  = BigMapDiffUpdate
    { bigMap  :: Bignum
    , keyHash :: ScriptExpr
    , key     :: Expression
    , value   :: Maybe Expression
    }

  | BigMapDiffRemove
    { bigMap :: Bignum }

  | BigMapDiffCopy
    { sourceBigMap :: Bignum
    , destinationBigMap :: Bignum
    }

  | BigMapDiffAlloc
    { bigMap    :: Bignum
    , keyType   :: Expression
    , valueType :: Expression
    }
  deriving (Eq, Show, Generic)

instance ToJSON BigMapDiff where
  toJSON = \case
    BigMapDiffUpdate bigMap' keyHash' key' value' ->
      object $ [ "action" .= ("update" :: Text)
               , "big_map" .= bigMap'
               , "key_hash" .= keyHash'
               , "key" .= key'
               ] ++ catMaybes ["value" .=? value']

    BigMapDiffRemove bigMap' ->
      object [ "action" .= ("remove" :: Text)
             , "big_map" .= bigMap'
             ]

    BigMapDiffCopy sourceBigMap' destinationBigMap' ->
      object [ "action" .= ("copy" :: Text)
             , "source_big_map" .= sourceBigMap'
             , "destination_big_map" .= destinationBigMap'
             ]

    BigMapDiffAlloc bigMap' keyType' valueType' ->
      object [ "action"     .= ("alloc" :: Text)
             , "big_map"    .= bigMap'
             , "key_type"   .= keyType'
             , "value_type" .= valueType'
             ]

instance FromJSON BigMapDiff where
  parseJSON = withObject "BigMapDiff" $ \o ->
    case HM.lookup "action" o of

      Just "update" ->
        BigMapDiffUpdate <$> o .:  "big_map"
                         <*> o .:  "key_hash"
                         <*> o .:  "key"
                         <*> o .:? "value"

      Just "remove" ->
        BigMapDiffRemove <$> o .: "big_map"

      Just "copy"   ->
        BigMapDiffCopy   <$> o .: "source_big_map"
                         <*> o .: "destination_big_map"

      Just "alloc"  ->
        BigMapDiffAlloc  <$> o .: "big_map"
                         <*> o .: "key_type"
                         <*> o .: "value_type"

      _ -> fail "BigMapDiff expected a key 'kind'."
