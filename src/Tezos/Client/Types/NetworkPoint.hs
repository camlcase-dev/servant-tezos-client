{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings     #-}

module Tezos.Client.Types.NetworkPoint where

import Data.Aeson
  (FromJSON(parseJSON), ToJSON(toJSON), (.=), (.:), (.:?), object, withObject)
import Data.Aeson.Types (Parser)
import Data.Maybe (catMaybes)
import Data.Text (Text)
import qualified Data.Text as T
import Data.Word (Word16)
import GHC.Generics (Generic)
import Tezos.Client.Types.Core
import Tezos.Client.Types.Crypto
import Tezos.Client.Extension.Aeson ((.=?))

-- [ [ $unistring,
--     { "trusted": boolean,
--       "greylisted_until"?: $timestamp,
--       "state":
--         { "event_kind": "requested" }
--         || { "event_kind": "accepted",
--              "p2p_peer_id": $Crypto_box.Public_key_hash }
--         || { "event_kind": "running",
--              "p2p_peer_id": $Crypto_box.Public_key_hash }
--         || { "event_kind": "disconnected" },
--       "p2p_peer_id"?: $Crypto_box.Public_key_hash,
--       "last_failed_connection"?: $timestamp,
--       "last_rejected_connection"?:
--         [ $Crypto_box.Public_key_hash, $timestamp ],
--       "last_established_connection"?:
--         [ $Crypto_box.Public_key_hash, $timestamp ],
--       "last_disconnection"?: [ $Crypto_box.Public_key_hash, $timestamp ],
--       "last_seen"?: [ $Crypto_box.Public_key_hash, $timestamp ],
--       "last_miss"?: $timestamp } ] ... ]

data P2PPoint =
  P2PPoint
    { trusted                :: Bool
    , greylistedUntil        :: Maybe Timestamp
    , state                  :: P2PEvent
    , p2PPeerId              :: Maybe PublicKeyHash
    , lastFailedConnection   :: Maybe Timestamp
    , lastRejectedConnection :: Maybe (PublicKeyHash, Timestamp)
    , lastDisconnection      :: Maybe (PublicKeyHash, Timestamp)
    , lastSeen               :: Maybe (PublicKeyHash, Timestamp)
    , lastMiss               :: Maybe Timestamp
    } deriving (Eq, Show, Generic)

instance ToJSON P2PPoint where
  toJSON (P2PPoint trusted' greylistedUntil' state' p2PPeerId'
          lastFailedConnection' lastRejectedConnection' lastDisconnection'
          lastSeen' lastMiss') =
    object $
      [ "trusted"                  .= trusted'
      , "state"                    .= state'
      ] ++ catMaybes
      [ "greylisted_until"         .=? greylistedUntil'
      , "p2p_peer_id"              .=? p2PPeerId'
      , "last_failed_connection"   .=? lastFailedConnection'
      , "last_rejected_connection" .=? lastRejectedConnection'
      , "last_disconnection"       .=? lastDisconnection'
      , "last_seen"                .=? lastSeen'
      , "last_miss"                .=? lastMiss'
      ]

instance FromJSON P2PPoint where
  parseJSON = withObject "P2PPoint" $ \o ->
    P2PPoint <$> o .:  "trusted"
             <*> o .:? "greylisted_until"
             <*> o .:  "state"
             <*> o .:? "p2p_peer_id"
             <*> o .:? "last_failed_connection"
             <*> o .:? "last_rejected_connection"
             <*> o .:? "last_disconnection"
             <*> o .:? "last_seen"
             <*> o .:? "last_miss"

-- "state":
--   { "event_kind": "requested" }
--   || { "event_kind": "accepted",
--        "p2p_peer_id": $Crypto_box.Public_key_hash }
--   || { "event_kind": "running",
--        "p2p_peer_id": $Crypto_box.Public_key_hash }
--   || { "event_kind": "disconnected" }

data P2PEvent
  = PRequested
  | PAccepted Text -- PublicKeyHash
  | PRunning Text -- PublicKeyHash
  | PDisconnected
  deriving (Eq, Show, Generic)

instance ToJSON P2PEvent where
  toJSON PRequested = object ["event_kind" .= ("requested" :: Text)]
  toJSON (PAccepted p2pPeerId) = object ["event_kind" .= ("accepted" :: Text), "p2p_peer_id" .= p2pPeerId]
  toJSON (PRunning p2pPeerId) = object ["event_kind" .= ("running" :: Text), "p2p_peer_id" .= p2pPeerId]
  toJSON PDisconnected = object ["event_kind" .= ("disconnected" :: Text)]

instance FromJSON P2PEvent where
  parseJSON = withObject "P2PEvent" $ \o -> do
    let match s =
          case s of
            "requested"    -> pure PRequested
            "accepted"     -> PAccepted <$> o .: "p2p_peer_id"
            "running"      -> PRunning <$> o .: "p2p_peer_id"
            "disconnected" -> pure PDisconnected 
            _ -> fail $ "expected P2PEvent, encountered " ++ (show s)
    eventKind <- (o .: "event_kind") :: Parser Text
    match eventKind

-- Cleanup needed

data Point
  = PointIPV4 Text Word16
  | PointIPV6 Text Word16
  deriving (Eq, Show, Generic)

instance ToJSON Point where
  toJSON (PointIPV4 host' port') = toJSON $ (host' <> ":" <> (T.pack $ show $ port'))
  toJSON (PointIPV6 host' port') = toJSON $ ("[" <> host' <> "]:" <> (T.pack $ show $ port'))

type P2PPointStateFilter = Text
