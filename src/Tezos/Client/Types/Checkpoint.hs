{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings     #-}

module Tezos.Client.Types.Checkpoint where

import Data.Aeson
  (FromJSON(parseJSON), ToJSON(toJSON), (.=), (.:), object, withObject, withText)
import Data.Int (Int64)
import Data.Text (Text)
import Data.Word (Word8)
import GHC.Generics (Generic)
import Tezos.Client.Types.Core

-- { "block":
--     { "level": integer ∈ [-2^31-2, 2^31+2],
--       "proto": integer ∈ [0, 255],
--       "predecessor": $block_hash,
--       "timestamp": $timestamp,
--       "validation_pass": integer ∈ [0, 255],
--       "operations_hash": $Operation_list_list_hash,
--       "fitness": $fitness,
--       "context": $Context_hash,
--       "protocol_data": /^[a-zA-Z0-9]+$/ },
--   "save_point": integer ∈ [-2^31-2, 2^31+2],
--   "caboose": integer ∈ [-2^31-2, 2^31+2],
--   "history_mode": "full" | "archive" | "rolling" }

data Checkpoint =
  Checkpoint
    { block       :: CheckpointBlock
    , savePoint   :: Int64
    , caboose     :: Int64
    , historyMode :: HistoryMode
    } deriving (Eq,Show,Generic)

instance ToJSON Checkpoint where
  toJSON (Checkpoint block' savePoint' caboose' historyMode') =
    object
      [ "block"        .= block'
      , "save_point"   .= savePoint'
      , "caboose"      .= caboose'
      , "history_mode" .= historyMode'
      ]

instance FromJSON Checkpoint where
  parseJSON = withObject "Checkpoint" $ \o ->
    Checkpoint <$> o .: "block"
               <*> o .: "save_point"
               <*> o .: "caboose"
               <*> o .: "history_mode"

--     { "level": integer ∈ [-2^31-2, 2^31+2],
--       "proto": integer ∈ [0, 255],
--       "predecessor": $block_hash,
--       "timestamp": $timestamp,
--       "validation_pass": integer ∈ [0, 255],
--       "operations_hash": $Operation_list_list_hash,
--       "fitness": $fitness,
--       "context": $Context_hash,
--       "protocol_data": /^[a-zA-Z0-9]+$/ },

data CheckpointBlock =
  CheckpointBlock
    { level          :: Int64
    , proto          :: Word8
    , predecessor    :: BlockHash
    , validationPass :: Timestamp
    , operationsHash :: [[InternalByteString]]
    , fitness        :: Fitness
    , context        :: ContextHash
    , protocolData   :: Text
    } deriving (Eq,Show,Generic)

instance ToJSON CheckpointBlock where
  toJSON (CheckpointBlock level' proto' predecessor' validationPass' operationsHash' fitness' context' protocolData') =
    object
      [ "level"           .= level'
      , "proto"           .= proto'
      , "predecessor"     .= predecessor'
      , "validation_pass" .= validationPass'
      , "operations_hash" .= operationsHash'
      , "fitness"         .= fitness'
      , "context"         .= context'
      , "protocol_data"   .= protocolData'
      ]

instance FromJSON CheckpointBlock where
  parseJSON = withObject "CheckPointBlock" $ \o ->
    CheckpointBlock <$> o .: "level"
          <*> o .: "proto"
          <*> o .: "predecessor"
          <*> o .: "validation_pass"
          <*> o .: "operations_hash"
          <*> o .: "fitness"
          <*> o .: "context"
          <*> o .: "protocol_data"

--   "history_mode": "full" | "archive" | "rolling"

data HistoryMode
  = Full
  | Archive
  | Rolling
  deriving (Eq,Read,Show,Generic)

instance ToJSON HistoryMode where
  toJSON Full    = "full"
  toJSON Archive = "archive"
  toJSON Rolling = "rolling"

instance FromJSON HistoryMode where
  parseJSON = withText "HistoryMode" $ \t ->
    case t of
      "full"    -> pure Full
      "archive" -> pure Archive
      "rolling" -> pure Rolling
      err       -> error (show err) 
