{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings     #-}

module Tezos.Client.Types.Helpers where

import Data.Aeson
  (FromJSON(parseJSON), ToJSON(toJSON), Value(String), (.=), (.:), (.:?),
   object, withObject)
import Data.Int (Int16, Int64)
import Data.Maybe (catMaybes)
import Data.Text (Text)
import Data.Word (Word16)
import GHC.Generics (Generic)
import Tezos.Client.Extension.Aeson ((.=?))
import Tezos.Client.Types.Block (OperationContents)
import Tezos.Client.Types.BlockHeaderShell (BlockHeaderShell)
import Tezos.Client.Types.Core
import Tezos.Client.Types.Contract
import Tezos.Client.Types.Crypto
import Tezos.Client.Types.Micheline
import Tezos.Client.Types.ProtocolData
import qualified Data.HashMap.Lazy as HM

-- { "first": integer ∈ [-2^31-2, 2^31+2],
--   "last": integer ∈ [-2^31-2, 2^31+2] }

data LevelsInCurrentCycle =
  LevelsInCurrentCycle
    { first :: Int64
    , last  :: Int64
    } deriving (Eq, Show, Generic)

instance ToJSON LevelsInCurrentCycle where
  toJSON (LevelsInCurrentCycle first' last') =
    object
      [ "first" .= first'
      , "last"  .= last'
      ]
    
instance FromJSON LevelsInCurrentCycle where
  parseJSON = withObject "LevelsInCurrentCycle" $ \o ->
    LevelsInCurrentCycle <$> o .: "first"
                         <*> o .: "last"

-- { "priority": integer ∈ [0, 2^16-1],
--   "proof_of_work_nonce": /^[a-zA-Z0-9]+$/,
--   "seed_nonce_hash"?: $cycle_nonce,
--   "signature": $Signature }

data SignedContents =
  SignedContents
    { priority         :: Int16
    , proofOfWorkNonce :: Text
    , seedNonceHash    :: Maybe CycleNonce
    , signature        :: Signature
    } deriving (Eq, Show, Generic)

instance ToJSON SignedContents where
  toJSON (SignedContents priority' proofOfWorkNonce' seedNonceHash'
             signature') =
    object $
      [ "priority"            .=  priority'
      , "proof_of_work_nonce" .=  proofOfWorkNonce'
      , "signature"           .=  signature'
      ] ++ catMaybes
      [ "seed_nonce_hash"     .=? seedNonceHash'
      ]

instance FromJSON SignedContents where
  parseJSON = withObject "SignedContents" $ \o ->
    SignedContents <$> o .:  "priority"
                   <*> o .:  "proof_of_work_nonce"
                   <*> o .:? "seed_nonce_hash"
                   <*> o .:  "signature"

-- { "operations": [ { "branch": $block_hash,
--                       "data": /^[a-zA-Z0-9]+$/ } ... ],
--   "check_signature"?: boolean }

data ParseOperationsRequest =
  ParseOperationsRequest
    { operations     :: [ParseOperationsData]
    , checkSignature :: Maybe Bool
    } deriving (Eq, Show, Generic)

instance ToJSON ParseOperationsRequest where
  toJSON (ParseOperationsRequest operations' checkSignature') =
    object $
      [ "operations"      .=  operations'
      ] ++ catMaybes
      [ "check_signature" .=? checkSignature'
      ]

instance FromJSON ParseOperationsRequest where
  parseJSON = withObject "ParseOperationsRequest" $ \o ->
    ParseOperationsRequest <$> o .:  "operations"
                           <*> o .:? "check_signature"

-- { "branch": $block_hash,
--   "data": /^[a-zA-Z0-9]+$/ }

data ParseOperationsData =
  ParseOperationsData
    { branch :: BlockHash
    , data_  :: Text
    } deriving (Eq, Show, Generic)

instance ToJSON ParseOperationsData where
  toJSON (ParseOperationsData branch' data') =
    object
      [ "branch" .= branch'
      , "data"   .= data'
      ]

instance FromJSON ParseOperationsData where
  parseJSON = withObject "ParseOperationsData" $ \o ->
    ParseOperationsData <$> o .: "branch"
                        <*> o .: "data"

-- [ { "branch": $block_hash,
--     "contents": [ $operation.alpha.contents ... ],
--     "signature": $Signature } ... ]

data ParseOperationsResponse =
  ParseOperationsResponse
    { branch    :: BlockHash
    , contents  :: [OperationContents]
    , signature :: Signature
    } deriving (Eq, Show, Generic)

instance ToJSON ParseOperationsResponse where
  toJSON (ParseOperationsResponse branch' contents' signature') =
    object
      [ "branch"    .= branch'
      , "contents"  .= contents'
      , "signature" .= signature'
      ]

instance FromJSON ParseOperationsResponse where
  parseJSON = withObject "ParseOperationsResponse" $ \o ->
    ParseOperationsResponse <$> o .: "branch"
                            <*> o .: "contents"
                            <*> o .: "signature"

-- { "protocol_data":
--     { "protocol": "Pt24m4xiPbLDhVgVfABUjirbmda3yohdN82Sp9FeuAXJ4eV9otd",
--       "priority": integer ∈ [0, 2^16-1],
--        "proof_of_work_nonce": /^[a-zA-Z0-9]+$/,
--       "seed_nonce_hash"?: $cycle_nonce,
--       "signature": $Signature },
--   "operations": [ [ $next_operation ... ] ... ] }

data SimulateBlockRequest =
  SimulateBlockRequest
    { protocolData :: ProtocolData
    , operations   :: [[NextOperation]]
    } deriving (Eq, Show, Generic)

instance ToJSON SimulateBlockRequest where
  toJSON (SimulateBlockRequest protocolData' operations') =
    object
      [ "protocol_data" .= protocolData'
      , "operations"    .= operations'
      ]

instance FromJSON SimulateBlockRequest where
  parseJSON = withObject "SimulateBlockRequest" $ \o ->
    SimulateBlockRequest <$> o .: "protocol_data"
                         <*> o .: "operations"

--  $next_operation:
--    { "protocol": "Pt24m4xiPbLDhVgVfABUjirbmda3yohdN82Sp9FeuAXJ4eV9otd",
--      "branch": $block_hash,
--      "contents": [ $operation.alpha.contents ... ],
--      "signature": $Signature }

data NextOperation =
  NextOperation
    { protocol  :: ProtocolHash
    , branch    :: BlockHash
    , contents  :: [OperationContents]
    , signature :: Signature
    } deriving (Eq, Show, Generic)

instance ToJSON NextOperation where
  toJSON (NextOperation protocol' branch' contents' signature') =
    object
      [ "protocol"  .= protocol'
      , "branch"    .= branch'
      , "contents"  .= contents'
      , "signature" .= signature'
      ]

instance FromJSON NextOperation where
  parseJSON = withObject "NextOperation" $ \o ->
    NextOperation <$> o .: "protocol"
                  <*> o .: "branch"
                  <*> o .: "contents"
                  <*> o .: "signature"

-- { "shell_header": $block_header.shell,
--   "operations":
--     [ { "applied":
--           [ { "hash": $Operation_hash,
--               "branch": $block_hash,
--               "data": /^[a-zA-Z0-9]+$/ } ... ],
--         "refused":
--           [ { "hash": $Operation_hash,
--               "branch": $block_hash,
--               "data": /^[a-zA-Z0-9]+$/,
--               "error": $error } ... ],
--         "branch_refused":
--           [ { "hash": $Operation_hash,
--               "branch": $block_hash,
--               "data": /^[a-zA-Z0-9]+$/,
--               "error": $error } ... ],
--         "branch_delayed":
--           [ { "hash": $Operation_hash,
--               "branch": $block_hash,
--               "data": /^[a-zA-Z0-9]+$/,
--               "error": $error } ... ] } ... ] }

data SimulateBlockResponse =
  SimulateBlockResponse
    { shellHeader :: BlockHeaderShell
    , operations  :: [BlockResponseOperations]
    } deriving (Eq, Show, Generic)

instance ToJSON SimulateBlockResponse
instance FromJSON SimulateBlockResponse

-- "operations":
--   [ { "applied":
--         [ { "hash": $Operation_hash,
--             "branch": $block_hash,
--             "data": /^[a-zA-Z0-9]+$/ } ... ],
--       "refused":
--         [ { "hash": $Operation_hash,
--             "branch": $block_hash,
--             "data": /^[a-zA-Z0-9]+$/,
--             "error": $error } ... ],
--       "branch_refused":
--         [ { "hash": $Operation_hash,
--             "branch": $block_hash,
--             "data": /^[a-zA-Z0-9]+$/,
--             "error": $error } ... ],
--       "branch_delayed":
--         [ { "hash": $Operation_hash,
--             "branch": $block_hash,
--             "data": /^[a-zA-Z0-9]+$/,
--             "error": $error } ... ] } ... ]

data BlockResponseOperations =
  BlockResponseOperations
    { applied       :: [BlockResponseOperation]
    , refused       :: [BlockResponseOperation]
    , branchRefused :: [BlockResponseOperation]
    , branchDelayed :: [BlockResponseOperation]
    } deriving (Eq, Show, Generic)

instance ToJSON BlockResponseOperations
instance FromJSON BlockResponseOperations

-- { "hash": $Operation_hash,
--   "branch": $block_hash,
--   "data": /^[a-zA-Z0-9]+$/ }

data BlockResponseOperation =
  BlockResponseOperation
    { hash   :: OperationHash
    , branch :: BlockHash
    , data_  :: Text
    } deriving (Eq, Show, Generic)

instance ToJSON BlockResponseOperation
instance FromJSON BlockResponseOperation

-- $operation.alpha.operation_with_metadata:
--    { "contents": [ $operation.alpha.operation_contents_and_result ... ],
--      "signature"?: $Signature }
--    || { "contents": [ $operation.alpha.contents ... ],
--         "signature"?: $Signature }


data OperationWithMetadata =
  OperationWithMetadata
  deriving (Eq, Show, Generic)

instance ToJSON OperationWithMetadata
instance FromJSON OperationWithMetadata

-- { "data": $micheline.michelson_v1.expression,
--   "type": $micheline.michelson_v1.expression,
--   "gas"?: $bignum }

data PackDataRequest =
  PackDataRequest
    { data_ :: Expression
    , type_ :: Expression
    , gas   :: Maybe Bignum
    } deriving (Eq, Show, Generic)

instance ToJSON PackDataRequest where
  toJSON (PackDataRequest data' type' gas') =
    object $
      [ "data" .= data'
      , "type" .= type'
      ] ++ catMaybes
      [ "gas"  .=? gas'
      ]

instance FromJSON PackDataRequest where
  parseJSON = withObject "PackDataRequest" $ \o ->
    PackDataRequest <$> o .:  "data"
                    <*> o .:  "type"
                    <*> o .:? "gas"

-- { "packed": /^[a-zA-Z0-9]+$/,
--   "gas": $bignum || "unaccounted" }

data PackDataResponse =
  PackDataResponse
    { packed :: Text
    , gas    :: Gas
    } deriving (Eq, Show, Generic)

instance ToJSON PackDataResponse where
  toJSON (PackDataResponse packed' gas') =
    object
      [ "packed" .= packed'
      , "gas"    .= gas'
      ]

instance FromJSON PackDataResponse where
  parseJSON = withObject "PackDataResponse" $ \o ->
    PackDataResponse <$> o .: "packed"
                     <*> o .: "gas"
  

-- { "script": $micheline.michelson_v1.expression,
--   "storage": $micheline.michelson_v1.expression,
--   "input": $micheline.michelson_v1.expression,
--   "amount": $mutez,
--   "source"?: $contract_id,
--   "payer"?: $contract_id,
--   "gas"?: $bignum }

data RunCodeRequest =
  RunCodeRequest
    { script  :: Expression
    , storage :: Expression
    , input   :: Expression
    , amount  :: Mutez
    , source  :: Maybe ContractId
    , payer   :: Maybe ContractId
    , gas     :: Maybe Bignum
    } deriving (Eq, Show, Generic)

instance ToJSON RunCodeRequest where
  toJSON (RunCodeRequest script' storage' input' amount' source' payer' gas') =
    object $
      [ "script"  .= script'
      , "storage" .= storage'
      , "input"   .= input'
      , "amount"  .= amount'
      ] ++ catMaybes
        [ "source"  .=? source'
        , "payer"   .=? payer'
        , "gas"     .=? gas'
        ]

instance FromJSON RunCodeRequest where
  parseJSON = withObject "RunCodeRequest" $ \o ->
    RunCodeRequest <$> o .:  "script"
                   <*> o .:  "storage"
                   <*> o .:  "input"
                   <*> o .:  "amount"
                   <*> o .:? "source"
                   <*> o .:? "payer"
                   <*> o .:? "gas"

--  { "storage": $micheline.michelson_v1.expression,
--    "operations": [ $operation.alpha.internal_operation ... ],
--    "big_map_diff"?: $contract.big_map_diff }

data RunCodeResponse =
  RunCodeResponse
    { storage    :: Expression
    , operations :: [Expression]
    , bigMapDiff :: Maybe BigMapDiff
    } deriving (Eq, Show, Generic)

instance ToJSON RunCodeResponse where
  toJSON (RunCodeResponse storage' operations' bigMapDiff') =
    object $ ["storage" .= storage', "operations" .= operations'] ++
    catMaybes ["big_map_diff" .=? bigMapDiff']

instance FromJSON RunCodeResponse where
  parseJSON = withObject "RunCodeResponse" $ \o ->
    RunCodeResponse <$> o .:  "storage"
                    <*> o .:  "operations"
                    <*> o .:? "big_map_diff"

--  { "branch": $block_hash,
--    "contents": [ $operation.alpha.contents ... ],
--    "signature": $Signature }

data RunOperationRequest =
  RunOperationRequest
    { branch    :: BlockHash
    , contents  :: [OperationContents]
    , signature :: Signature   
    } deriving (Eq, Show, Generic)

instance ToJSON RunOperationRequest where
  toJSON (RunOperationRequest branch' contents' signature') =
    object
      [ "branch"    .= branch'
      , "contents"  .= contents'
      , "signature" .= signature'
      ]

instance FromJSON RunOperationRequest where
  parseJSON = withObject "RunOperationRequest" $ \o ->
    RunOperationRequest <$> o .: "branch"
                        <*> o .: "contents"
                        <*> o .: "signature"

--  { "script": $micheline.michelson_v1.expression,
--    "storage": $micheline.michelson_v1.expression,
--    "input": $micheline.michelson_v1.expression,
--    "amount": $mutez,
--    "source"?: $contract_id,
--    "payer"?: $contract_id,
--    "gas"?: $bignum }

data RunTraceRequest =
  RunTraceRequest
    { script  :: Expression
    , storage :: Expression
    , input   :: Expression
    , amount  :: Mutez
    , source  :: Maybe ContractId
    , payer   :: Maybe ContractId
    , gas     :: Maybe Bignum
    } deriving (Eq, Show, Generic)

instance ToJSON RunTraceRequest where
  toJSON (RunTraceRequest script' storage' input' amount' source' payer' gas') =
    object $
      [ "script"  .= script'
      , "storage" .= storage'
      , "input"   .= input'
      , "amount"  .= amount'
      ] ++ catMaybes
        [ "source"  .=? source'
        , "payer"   .=? payer'
        , "gas"     .=? gas'
        ]

instance FromJSON RunTraceRequest where
  parseJSON = withObject "RunTraceRequest" $ \o ->
    RunTraceRequest <$> o .:  "script"
                    <*> o .:  "storage"
                    <*> o .:  "input"
                    <*> o .:  "amount"
                    <*> o .:? "source"
                    <*> o .:? "payer"
                    <*> o .:? "gas"

--  { "storage": $micheline.michelson_v1.expression,
--    "operations": [ $operation.alpha.internal_operation ... ],
--    "trace": $scripted.trace,
--    "big_map_diff"?: $contract.big_map_diff }

data RunTraceResponse =
  RunTraceResponse
    { storage    :: Expression
    , operations :: [InternalOperation]
    , trace      :: [Trace]
    , bigMapDiff :: Maybe BigMapDiff
    } deriving (Eq, Show, Generic)

instance ToJSON RunTraceResponse where
  toJSON (RunTraceResponse storage' operations' trace' bigMapDiff') =
    object $
      [ "storage"    .= storage'
      , "operations" .= operations'
      , "trace"      .= trace'
      ] ++ catMaybes ["big_map_diff" .=? bigMapDiff']

instance FromJSON RunTraceResponse where
  parseJSON = withObject "RunTraceResponse" $ \o ->
    RunTraceResponse <$> o .:  "storage"
                     <*> o .:  "operations"
                     <*> o .:  "trace"
                     <*> o .:? "big_map_diff"

-- $operation.alpha.internal_operation:
--   { "source": $contract_id,
--     "nonce": integer ∈ [0, 2^16-1],
--     "kind": "reveal",
--     "public_key": $Signature.Public_key }
--   || { "source": $contract_id,
--        "nonce": integer ∈ [0, 2^16-1],
--        "kind": "transaction",
--        "amount": $mutez,
--        "destination": $contract_id,
--        "parameters"?: $micheline.michelson_v1.expression }
--   || { "source": $contract_id,
--        "nonce": integer ∈ [0, 2^16-1],
--        "kind": "origination",
--        "manager_pubkey": $Signature.Public_key_hash,
--        "balance": $mutez,
--        "spendable"?: boolean,
--        "delegatable"?: boolean,
--        "delegate"?: $Signature.Public_key_hash,
--        "script"?: $scripted.contracts }
--   || { "source": $contract_id,
--        "nonce": integer ∈ [0, 2^16-1],
--        "kind": "delegation",
--        "delegate"?: $Signature.Public_key_hash }

data InternalOperation
  = InternalOperationReveal
    { source        :: ContractId
    , nonce         :: Word16
    , publicKey     :: PublicKey
    }
  | InternalOperationTransaction
    { source        :: ContractId
    , nonce         :: Word16
    , amount        :: Mutez
    , destination   :: ContractId
    , parameters    :: Maybe Expression
    }
  | InternalOperationOrigination
    { source        :: ContractId
    , nonce         :: Word16
    , managerPubKey :: PublicKeyHash
    , balance       :: Mutez
    , spendable     :: Maybe Bool
    , delegatable   :: Maybe Bool
    , delegate      :: Maybe PublicKeyHash
    , script        :: Maybe Contract
    }  
  | InternalOperationDelegation
    { source        :: ContractId
    , nonce         :: Word16
    , delegate      :: Maybe PublicKeyHash
    }
  deriving (Eq, Show, Generic)

instance ToJSON InternalOperation where
  toJSON (InternalOperationReveal source' nonce' publicKey') =
    object
      [ "kind"       .= ("reveal" :: Text)
      , "source"     .= source'
      , "nonce"      .= nonce'
      , "public_key" .= publicKey'
      ]
  toJSON (InternalOperationTransaction source' nonce' amount'
          destination' parameters') =
    object $
      [ "kind"        .= ("transaction" :: Text)
      , "source"      .= source'
      , "nonce"       .= nonce'
      , "amount"      .= amount'
      , "destination" .= destination'
      ] ++ catMaybes
      [ "parameters"  .=? parameters'
      ]
  toJSON (InternalOperationOrigination source' nonce' managerPubKey' balance'
          spendable' delegatable' delegate' script') =
    object $
      [ "kind"            .= ("origination" :: Text)
      , "source"          .= source'
      , "nonce"           .= nonce'
      , "manager_pub_key" .= managerPubKey'
      , "balance"         .= balance'
      ] ++ catMaybes
      [ "spendable"       .=? spendable'
      , "delegatable"     .=? delegatable'      
      , "delegate"        .=? delegate'
      , "script"          .=? script'
      ]
  toJSON (InternalOperationDelegation source' nonce' delegate') =
    object $
      [ "kind"     .= ("delegation" :: Text)
      , "source"   .= source'
      , "nonce"    .= nonce'
      ] ++ catMaybes
      [ "delegate" .=? delegate'
      ]

instance FromJSON InternalOperation where
  parseJSON = withObject "InternalOperation" $ \o ->
    case HM.lookup "kind" o of
      Just "reveal"      ->
        InternalOperationReveal <$> o .: "source"
                                <*> o .: "nonce"
                                <*> o .: "public_key"
      Just "transaction" ->
        InternalOperationTransaction <$> o .:  "source"
                                     <*> o .:  "nonce"
                                     <*> o .:  "amount"
                                     <*> o .:  "destination"
                                     <*> o .:? "parameters"
      Just "origination" ->
        InternalOperationOrigination <$> o .:  "source"
                                     <*> o .:  "nonce"
                                     <*> o .:  "manager_pub_key"
                                     <*> o .:  "balance"
                                     <*> o .:? "spendable"
                                     <*> o .:? "delegatable"
                                     <*> o .:? "delegate"
                                     <*> o .:? "script"
      Just "delegation"  ->
        InternalOperationDelegation <$> o .:  "source"
                                    <*> o .:  "nonce"
                                    <*> o .:? "delegate"
      _ -> fail "InternalOperationResult expected a key 'kind'."

-- $scripted.trace:
--   [ { "location": $micheline.location,
--       "gas": $bignum || "unaccounted",
--       "stack":
--         [ { "item": $micheline.michelson_v1.expression,
--             "annot"?: $unistring } ... ] } ... ]

data Trace =
  Trace
    { location :: Location
    , gas      :: Gas
    , stack    :: [StackExpression]
    } deriving (Eq, Show, Generic)

instance ToJSON Trace where
  toJSON (Trace location' gas' stack') =
    object
      [ "location" .= location'
      , "gas"      .= gas'
      , "stack"    .= stack'
      ]

instance FromJSON Trace where
  parseJSON = withObject "Trace" $ \o ->
    Trace <$> o .: "location"
          <*> o .: "gas"
          <*> o .: "stack"

-- "stack":
--   [ { "item": $micheline.michelson_v1.expression,
--       "annot"?: $unistring } ... ] } ... ]

data StackExpression =
  StackExpression
    { item  :: Expression
    , annot :: Maybe Unistring
    } deriving (Eq, Show, Generic)

instance ToJSON StackExpression where
  toJSON (StackExpression item' annot') =
    object $ ["item" .= item'] ++ catMaybes ["annot" .=? annot']

instance FromJSON StackExpression where
  parseJSON = withObject "StackExpression" $ \o ->
    StackExpression <$> o .:  "item"
                    <*> o .:? "annot"

--   { "program": $micheline.michelson_v1.expression,
--     "gas"?: $bignum }

data TypecheckCodeRequest =
  TypecheckCodeRequest
    { program :: Expression
    , gas     :: Maybe Bignum
    } deriving (Eq, Show, Generic)

instance ToJSON TypecheckCodeRequest where
  toJSON (TypecheckCodeRequest program' gas') =
    object $ ["program" .=  program'] ++ catMaybes ["gas" .=? gas']

instance FromJSON TypecheckCodeRequest where
  parseJSON = withObject "TypecheckCodeRequest" $ \o ->
    TypecheckCodeRequest <$> o .:  "program"
                         <*> o .:? "gas"

-- { "type_map":
--    [ { "location": $micheline.location,
--        "stack_before":
--          [ [ $micheline.michelson_v1.expression, [ $unistring ... ] ] ... ],
--        "stack_after":
--          [ [ $micheline.michelson_v1.expression, [ $unistring ... ] ] ... ] } ... ],
--  "gas": $bignum || "unaccounted" }

data TypecheckCodeResponse =
  TypecheckCodeResponse
    { typeMap :: [TypeMap]
    , gas     :: Gas
    } deriving (Eq, Show, Generic)

instance ToJSON TypecheckCodeResponse where
  toJSON (TypecheckCodeResponse typeMap' gas') =
    object
      [ "type_map" .= typeMap'
      , "gas"      .= gas'
      ]

instance FromJSON TypecheckCodeResponse where
  parseJSON = withObject "TypecheckCodeResponse" $ \o ->
    TypecheckCodeResponse <$> o .: "type_map"
                          <*> o .: "gas"

-- "type_map":
--    [ { "location": $micheline.location,
--        "stack_before":
--          [ [ $micheline.michelson_v1.expression, [ $unistring ... ] ] ... ],
--        "stack_after":
--          [ [ $micheline.michelson_v1.expression, [ $unistring ... ] ] ... ] } ... ]

data TypeMap =
  TypeMap
    { location    :: Location
    , stackBefore :: (Expression, [Unistring])
    , stackAfter  :: (Expression, [Unistring])
    } deriving (Eq, Show, Generic)

instance ToJSON TypeMap where
  toJSON (TypeMap location' stackBefore' stackAfter') =
    object
      [ "location"     .= location'
      , "stack_before" .= stackBefore'
      , "stack_after"  .= stackAfter'
      ]

instance FromJSON TypeMap where
  parseJSON = withObject "TypeMap" $ \o ->
    TypeMap <$> o .: "location"
            <*> o .: "stack_before"
            <*> o .: "stack_after"

--  { "data": $micheline.michelson_v1.expression,
--    "type": $micheline.michelson_v1.expression,
--    "gas"?: $bignum }

data TypecheckDataRequest =
  TypecheckDataRequest
    { data_ :: Expression
    , type_ :: Expression
    , gas   :: Maybe Bignum
    } deriving (Eq, Show, Generic)

instance ToJSON TypecheckDataRequest
instance FromJSON TypecheckDataRequest

-- { "gas": $bignum || "unaccounted" }

data Gas
  = Gas Bignum
  | GasUnaccounted
  deriving (Eq, Show, Generic)

instance ToJSON Gas where
  toJSON (Gas bignum')  = toJSON bignum'
  toJSON GasUnaccounted = "unaccounted"

instance FromJSON Gas where
  parseJSON json' =
    case json' of
      (String "unaccounted") -> pure GasUnaccounted
      _                      -> Gas <$> parseJSON json'

-- fix this
type Pkh = Text
