{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE OverloadedStrings     #-}

module Tezos.Client.Types.PeerLog where

import Data.Aeson
  (FromJSON(parseJSON), ToJSON(toJSON), (.=), (.:), (.:?), object, withObject,
   withText)
import Data.Maybe (catMaybes)
import Data.Text (Text)
import Data.Word (Word16)
import GHC.Generics (Generic)
import Tezos.Client.Types.Core
import Tezos.Client.Extension.Aeson ((.=?))

-- [ { "kind":
--       "rejecting_request"
--       | "incoming_request"
--       | "disconnection"
--       | "external_disconnection"
--       | "connection_established"
--       | "request_rejected",
--     "timestamp": $timestamp,
--     "addr": $unistring,
--     "port"?: integer ∈ [0, 2^16-1] } ... ]

data PeerLog =
  PeerLog
    { kind      :: PeerLogKind
    , timestamp :: Timestamp
    , addr      :: Text
    , port      :: Maybe Word16
    } deriving (Eq, Show, Generic)

instance ToJSON PeerLog where
  toJSON (PeerLog kind' timestamp' addr' port') =
    object $ 
      [ "kind"      .= kind'
      , "timestamp" .= timestamp'
      , "addr"      .= addr'
      ] ++ catMaybes
      [ "port"      .=? port'
      ]

instance FromJSON PeerLog where
  parseJSON = withObject "PeerLog" $ \o ->
    PeerLog <$> o .:  "kind"
            <*> o .:  "timestamp"
            <*> o .:  "addr"
            <*> o .:? "port"

-- "kind":
--   "rejecting_request"
--   | "incoming_request"
--   | "disconnection"
--   | "external_disconnection"
--   | "connection_established"
--   | "request_rejected"

data PeerLogKind
  = PLRejectingRequest
  | PLIncomingRequest
  | PLDisconnection
  | PLExternalDisconnection
  | PLConnectionEstablished
  | PLRequestRejected
  deriving (Eq, Show, Generic)

instance ToJSON PeerLogKind where
  toJSON PLRejectingRequest      = "rejecting_request"
  toJSON PLIncomingRequest       = "incoming_request"
  toJSON PLDisconnection         = "disconnection"
  toJSON PLExternalDisconnection ="external_disconnection"
  toJSON PLConnectionEstablished = "connection_established"
  toJSON PLRequestRejected       = "request_rejected"

instance FromJSON PeerLogKind where
  parseJSON = withText "PeerLogKind" $ \s ->
    case s of
      "rejecting_request"      -> pure PLRejectingRequest
      "incoming_request"       -> pure PLIncomingRequest
      "disconnection"          -> pure PLDisconnection
      "external_disconnection" -> pure PLExternalDisconnection
      "connection_established" -> pure PLConnectionEstablished
      "request_rejected"       -> pure PLRequestRejected
      _ -> fail $ "expected PeerLogKind, encountered " ++ (show s)

