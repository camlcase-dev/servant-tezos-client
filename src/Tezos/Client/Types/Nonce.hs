{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings     #-}

module Tezos.Client.Types.Nonce where

import Data.Aeson
  (FromJSON(parseJSON), ToJSON(toJSON), (.=), (.:), object, withObject,
   Value(Object))
import Data.Text (Text)
import GHC.Generics (Generic)
import qualified Data.HashMap.Lazy as HM

-- { "nonce": /^[a-zA-Z0-9]+$/ } || { "hash": $cycle_nonce } || {  }

data Nonce
  = NNonce Text
  | NHash Text
  | NNull
  deriving (Eq, Show, Generic)

instance ToJSON Nonce where
  toJSON (NNonce text')       = object [ "nonce" .= text' ]
  toJSON (NHash  cycleNonce') = object [ "hash"  .= cycleNonce' ]
  toJSON NNull                = Object HM.empty
    
instance FromJSON Nonce where
  parseJSON = withObject "Nonce" $ \o ->
    case HM.member "nonce" o of
      True -> NNonce <$> o .: "nonce"
      _ ->
        case HM.member "hash" o of
          True -> NHash <$> o .: "hash"
          _ -> fail "Nonce"
