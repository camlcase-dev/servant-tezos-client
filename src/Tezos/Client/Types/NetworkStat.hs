{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings     #-}

module Tezos.Client.Types.NetworkStat where

import Data.Aeson
  (FromJSON(parseJSON), ToJSON(toJSON), (.=), (.:), object, withObject)
import Data.Int (Int32)
import Data.Text (Text)
import GHC.Generics (Generic)

data NetworkStat =
  NetworkStat
    { totalSent      :: Text -- Int64 -- encoded as string
    , totalRecv      :: Text -- Int64 -- encoded as string
    , currentInflow  :: Int32
    , currentOutflow :: Int32
    } deriving (Eq, Show, Generic)

instance ToJSON NetworkStat where
  toJSON (NetworkStat totalSent' totalRecv' currentInflow' currentOutflow') =
    object
      [ "total_sent"      .= totalSent'
      , "total_recv"      .= totalRecv'
      , "current_inflow"  .= currentInflow'
      , "current_outflow" .= currentOutflow'
      ]

instance FromJSON NetworkStat where
  parseJSON = withObject "NetworkStat" $ \o ->
    NetworkStat <$> o .: "total_sent"
                <*> o .: "total_recv"
                <*> o .: "current_inflow"
                <*> o .: "current_outflow"
