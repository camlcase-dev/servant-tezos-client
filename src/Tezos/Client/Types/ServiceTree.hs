{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings     #-}

module Tezos.Client.Types.ServiceTree where

import Data.Aeson
  (FromJSON, ToJSON(toJSON), Value, (.=), object)
import Data.Int (Int64)
import Data.Maybe (catMaybes)
import Data.Text (Text)
import GHC.Generics (Generic)
import Tezos.Client.Types.Core (Unistring)
import Tezos.Client.Extension.Aeson ((.=?))

{-
 $service_tree
  $layout:
    { "kind": "Zero_width" }
    || { "size": "Int32" | "Int16" | "Uint16" | "Int64" | "Int8" | "Uint8",
         "kind": "Int" }
    || { "kind": "Bool" }
    || { "min": integer ∈ [-2^30-2, 2^30+2],
         "max": integer ∈ [-2^30-2, 2^30+2],
         "kind": "RangedInt" }
    || { "min": number,
         "max": number,
         "kind": "RangedFloat" }
    || { "kind": "Float" }
    || { "kind": "Bytes" }
    || { "kind": "String" }
    || { "size": "Int16" | "Uint16" | "Int8" | "Uint8",
         "reference": $unistring,
         "kind": "Enum" }
    || { "layout": $layout,
         "kind": "Seq",
         "max_length"?: integer ∈ [-2^30-2, 2^30+2] }
    || { "name": $unistring,
         "kind": "Ref" }
    || { "kind": "Padding" }
  $schema.field:
    { "name": $unistring,
      "layout": $layout,
      "data_kind": $schema.kind,
      "kind": "named" }
    || { "layout": $layout,
         "kind": "anon",
         "data_kind": $schema.kind }
    || { "kind": "dyn",
         "name"?: $unistring,
         "num_fields": integer ∈ [-2^30-2, 2^30+2],
         "size": "Uint30" | "Uint16" | "Uint8" }
    || { "kind": "option_indicator",
         "name": $unistring }
  $schema.kind:
    { "size": integer ∈ [-2^30-2, 2^30+2],
      "kind": "Float" }
    || { "kind": "Dynamic" }
    || { "kind": "Variable" }
  $service_tree:
    { "static":
        { "get_service"?:
            { "meth": "PATCH" | "GET" | "POST" | "PUT" | "DELETE",
              "path":
                [ $unistring
                  || { "id": "single",
                       "name": $unistring,
                       "descr"?: $unistring }
                  || { "id": "multiple",
                       "name": $unistring,
                       "descr"?: $unistring } ... ],
              "description"?: $unistring,
              "query":
                [ { "name": $unistring,
                    "description"?: $unistring,
                    "kind":
                      { "single":
                          { "id": "single",
                            "name": $unistring,
                            "descr"?: $unistring } }
                      || { "optional":
                             { "id": "single",
                               "name": $unistring,
                               "descr"?: $unistring } }
                      || { "flag": {  } }
                      || { "multi":
                             { "id": "single",
                               "name": $unistring,
                               "descr"?: $unistring } } } ... ],
              "input"?:
                { "json_schema": any,
                  "binary_schema":
                    { "toplevel":
                        { "fields": [ $schema.field ... ] }
                        || { "tag_size": "Uint16" | "Uint8",
                             "kind": $schema.kind,
                             "cases": [ $union case ... ] }
                        || { "size": "Int16" | "Uint16" | "Int8" | "Uint8",
                             "cases":
                               [ [ integer ∈ [-2^30-2, 2^30+2],
                                   $unistring ] ... ] },
                      "fields":
                        [ { "description":
                              { "title": $unistring,
                                "description"?: $unistring },
                            "encoding":
                              { "fields": [ $schema.field ... ] }
                              || { "tag_size": "Uint16" | "Uint8",
                                   "kind": $schema.kind,
                                   "cases": [ $union case ... ] }
                              || { "size":
                                     "Int16" | "Uint16" | "Int8" | "Uint8",
                                   "cases":
                                     [ [ integer ∈ [-2^30-2, 2^30+2],
                                         $unistring ] ... ] } } ... ] } },
              "output":
                { "json_schema": any,
                  "binary_schema":
                    { "toplevel":
                        { "fields": [ $schema.field ... ] }
                        || { "tag_size": "Uint16" | "Uint8",
                             "kind": $schema.kind,
                             "cases": [ $union case ... ] }
                        || { "size": "Int16" | "Uint16" | "Int8" | "Uint8",
                             "cases":
                               [ [ integer ∈ [-2^30-2, 2^30+2],
                                   $unistring ] ... ] },
                      "fields":
                        [ { "description":
                              { "title": $unistring,
                                "description"?: $unistring },
                            "encoding":
                              { "fields": [ $schema.field ... ] }
                              || { "tag_size": "Uint16" | "Uint8",
                                   "kind": $schema.kind,
                                   "cases": [ $union case ... ] }
                              || { "size":
                                     "Int16" | "Uint16" | "Int8" | "Uint8",
                                   "cases":
                                     [ [ integer ∈ [-2^30-2, 2^30+2],
                                         $unistring ] ... ] } } ... ] } },
              "error":
                { "json_schema": any,
                  "binary_schema":
                    { "toplevel":
                        { "fields": [ $schema.field ... ] }
                        || { "tag_size": "Uint16" | "Uint8",
                             "kind": $schema.kind,
                             "cases": [ $union case ... ] }
                        || { "size": "Int16" | "Uint16" | "Int8" | "Uint8",
                             "cases":
                               [ [ integer ∈ [-2^30-2, 2^30+2],
                                   $unistring ] ... ] },
                      "fields":
                        [ { "description":
                              { "title": $unistring,
                                "description"?: $unistring },
                            "encoding":
                              { "fields": [ $schema.field ... ] }
                              || { "tag_size": "Uint16" | "Uint8",
                                   "kind": $schema.kind,
                                   "cases": [ $union case ... ] }
                              || { "size":
                                     "Int16" | "Uint16" | "Int8" | "Uint8",
                                   "cases":
                                     [ [ integer ∈ [-2^30-2, 2^30+2],
                                         $unistring ] ... ] } } ... ] } } },
          "post_service"?:
            { "meth": "PATCH" | "GET" | "POST" | "PUT" | "DELETE",
              "path":
                [ $unistring
                  || { "id": "single",
                       "name": $unistring,
                       "descr"?: $unistring }
                  || { "id": "multiple",
                       "name": $unistring,
                       "descr"?: $unistring } ... ],
              "description"?: $unistring,
              "query":
                [ { "name": $unistring,
                    "description"?: $unistring,
                    "kind":
                      { "single":
                          { "id": "single",
                            "name": $unistring,
                            "descr"?: $unistring } }
                      || { "optional":
                             { "id": "single",
                               "name": $unistring,
                               "descr"?: $unistring } }
                      || { "flag": {  } }
                      || { "multi":
                             { "id": "single",
                               "name": $unistring,
                               "descr"?: $unistring } } } ... ],
              "input"?:
                { "json_schema": any,
                  "binary_schema":
                    { "toplevel":
                        { "fields": [ $schema.field ... ] }
                        || { "tag_size": "Uint16" | "Uint8",
                             "kind": $schema.kind,
                             "cases": [ $union case ... ] }
                        || { "size": "Int16" | "Uint16" | "Int8" | "Uint8",
                             "cases":
                               [ [ integer ∈ [-2^30-2, 2^30+2],
                                   $unistring ] ... ] },
                      "fields":
                        [ { "description":
                              { "title": $unistring,
                                "description"?: $unistring },
                            "encoding":
                              { "fields": [ $schema.field ... ] }
                              || { "tag_size": "Uint16" | "Uint8",
                                   "kind": $schema.kind,
                                   "cases": [ $union case ... ] }
                              || { "size":
                                     "Int16" | "Uint16" | "Int8" | "Uint8",
                                   "cases":
                                     [ [ integer ∈ [-2^30-2, 2^30+2],
                                         $unistring ] ... ] } } ... ] } },
              "output":
                { "json_schema": any,
                  "binary_schema":
                    { "toplevel":
                        { "fields": [ $schema.field ... ] }
                        || { "tag_size": "Uint16" | "Uint8",
                             "kind": $schema.kind,
                             "cases": [ $union case ... ] }
                        || { "size": "Int16" | "Uint16" | "Int8" | "Uint8",
                             "cases":
                               [ [ integer ∈ [-2^30-2, 2^30+2],
                                   $unistring ] ... ] },
                      "fields":
                        [ { "description":
                              { "title": $unistring,
                                "description"?: $unistring },
                            "encoding":
                              { "fields": [ $schema.field ... ] }
                              || { "tag_size": "Uint16" | "Uint8",
                                   "kind": $schema.kind,
                                   "cases": [ $union case ... ] }
                              || { "size":
                                     "Int16" | "Uint16" | "Int8" | "Uint8",
                                   "cases":
                                     [ [ integer ∈ [-2^30-2, 2^30+2],
                                         $unistring ] ... ] } } ... ] } },
              "error":
                { "json_schema": any,
                  "binary_schema":
                    { "toplevel":
                        { "fields": [ $schema.field ... ] }
                        || { "tag_size": "Uint16" | "Uint8",
                             "kind": $schema.kind,
                             "cases": [ $union case ... ] }
                        || { "size": "Int16" | "Uint16" | "Int8" | "Uint8",
                             "cases":
                               [ [ integer ∈ [-2^30-2, 2^30+2],
                                   $unistring ] ... ] },
                      "fields":
                        [ { "description":
                              { "title": $unistring,
                                "description"?: $unistring },
                            "encoding":
                              { "fields": [ $schema.field ... ] }
                              || { "tag_size": "Uint16" | "Uint8",
                                   "kind": $schema.kind,
                                   "cases": [ $union case ... ] }
                              || { "size":
                                     "Int16" | "Uint16" | "Int8" | "Uint8",
                                   "cases":
                                     [ [ integer ∈ [-2^30-2, 2^30+2],
                                         $unistring ] ... ] } } ... ] } } },
          "delete_service"?:
            { "meth": "PATCH" | "GET" | "POST" | "PUT" | "DELETE",
              "path":
                [ $unistring
                  || { "id": "single",
                       "name": $unistring,
                       "descr"?: $unistring }
                  || { "id": "multiple",
                       "name": $unistring,
                       "descr"?: $unistring } ... ],
              "description"?: $unistring,
              "query":
                [ { "name": $unistring,
                    "description"?: $unistring,
                    "kind":
                      { "single":
                          { "id": "single",
                            "name": $unistring,
                            "descr"?: $unistring } }
                      || { "optional":
                             { "id": "single",
                               "name": $unistring,
                               "descr"?: $unistring } }
                      || { "flag": {  } }
                      || { "multi":
                             { "id": "single",
                               "name": $unistring,
                               "descr"?: $unistring } } } ... ],
              "input"?:
                { "json_schema": any,
                  "binary_schema":
                    { "toplevel":
                        { "fields": [ $schema.field ... ] }
                        || { "tag_size": "Uint16" | "Uint8",
                             "kind": $schema.kind,
                             "cases": [ $union case ... ] }
                        || { "size": "Int16" | "Uint16" | "Int8" | "Uint8",
                             "cases":
                               [ [ integer ∈ [-2^30-2, 2^30+2],
                                   $unistring ] ... ] },
                      "fields":
                        [ { "description":
                              { "title": $unistring,
                                "description"?: $unistring },
                            "encoding":
                              { "fields": [ $schema.field ... ] }
                              || { "tag_size": "Uint16" | "Uint8",
                                   "kind": $schema.kind,
                                   "cases": [ $union case ... ] }
                              || { "size":
                                     "Int16" | "Uint16" | "Int8" | "Uint8",
                                   "cases":
                                     [ [ integer ∈ [-2^30-2, 2^30+2],
                                         $unistring ] ... ] } } ... ] } },
              "output":
                { "json_schema": any,
                  "binary_schema":
                    { "toplevel":
                        { "fields": [ $schema.field ... ] }
                        || { "tag_size": "Uint16" | "Uint8",
                             "kind": $schema.kind,
                             "cases": [ $union case ... ] }
                        || { "size": "Int16" | "Uint16" | "Int8" | "Uint8",
                             "cases":
                               [ [ integer ∈ [-2^30-2, 2^30+2],
                                   $unistring ] ... ] },
                      "fields":
                        [ { "description":
                              { "title": $unistring,
                                "description"?: $unistring },
                            "encoding":
                              { "fields": [ $schema.field ... ] }
                              || { "tag_size": "Uint16" | "Uint8",
                                   "kind": $schema.kind,
                                   "cases": [ $union case ... ] }
                              || { "size":
                                     "Int16" | "Uint16" | "Int8" | "Uint8",
                                   "cases":
                                     [ [ integer ∈ [-2^30-2, 2^30+2],
                                         $unistring ] ... ] } } ... ] } },
              "error":
                { "json_schema": any,
                  "binary_schema":
                    { "toplevel":
                        { "fields": [ $schema.field ... ] }
                        || { "tag_size": "Uint16" | "Uint8",
                             "kind": $schema.kind,
                             "cases": [ $union case ... ] }
                        || { "size": "Int16" | "Uint16" | "Int8" | "Uint8",
                             "cases":
                               [ [ integer ∈ [-2^30-2, 2^30+2],
                                   $unistring ] ... ] },
                      "fields":
                        [ { "description":
                              { "title": $unistring,
                                "description"?: $unistring },
                            "encoding":
                              { "fields": [ $schema.field ... ] }
                              || { "tag_size": "Uint16" | "Uint8",
                                   "kind": $schema.kind,
                                   "cases": [ $union case ... ] }
                              || { "size":
                                     "Int16" | "Uint16" | "Int8" | "Uint8",
                                   "cases":
                                     [ [ integer ∈ [-2^30-2, 2^30+2],
                                         $unistring ] ... ] } } ... ] } } },
          "put_service"?:
            { "meth": "PATCH" | "GET" | "POST" | "PUT" | "DELETE",
              "path":
                [ $unistring
                  || { "id": "single",
                       "name": $unistring,
                       "descr"?: $unistring }
                  || { "id": "multiple",
                       "name": $unistring,
                       "descr"?: $unistring } ... ],
              "description"?: $unistring,
              "query":
                [ { "name": $unistring,
                    "description"?: $unistring,
                    "kind":
                      { "single":
                          { "id": "single",
                            "name": $unistring,
                            "descr"?: $unistring } }
                      || { "optional":
                             { "id": "single",
                               "name": $unistring,
                               "descr"?: $unistring } }
                      || { "flag": {  } }
                      || { "multi":
                             { "id": "single",
                               "name": $unistring,
                               "descr"?: $unistring } } } ... ],
              "input"?:
                { "json_schema": any,
                  "binary_schema":
                    { "toplevel":
                        { "fields": [ $schema.field ... ] }
                        || { "tag_size": "Uint16" | "Uint8",
                             "kind": $schema.kind,
                             "cases": [ $union case ... ] }
                        || { "size": "Int16" | "Uint16" | "Int8" | "Uint8",
                             "cases":
                               [ [ integer ∈ [-2^30-2, 2^30+2],
                                   $unistring ] ... ] },
                      "fields":
                        [ { "description":
                              { "title": $unistring,
                                "description"?: $unistring },
                            "encoding":
                              { "fields": [ $schema.field ... ] }
                              || { "tag_size": "Uint16" | "Uint8",
                                   "kind": $schema.kind,
                                   "cases": [ $union case ... ] }
                              || { "size":
                                     "Int16" | "Uint16" | "Int8" | "Uint8",
                                   "cases":
                                     [ [ integer ∈ [-2^30-2, 2^30+2],
                                         $unistring ] ... ] } } ... ] } },
              "output":
                { "json_schema": any,
                  "binary_schema":
                    { "toplevel":
                        { "fields": [ $schema.field ... ] }
                        || { "tag_size": "Uint16" | "Uint8",
                             "kind": $schema.kind,
                             "cases": [ $union case ... ] }
                        || { "size": "Int16" | "Uint16" | "Int8" | "Uint8",
                             "cases":
                               [ [ integer ∈ [-2^30-2, 2^30+2],
                                   $unistring ] ... ] },
                      "fields":
                        [ { "description":
                              { "title": $unistring,
                                "description"?: $unistring },
                            "encoding":
                              { "fields": [ $schema.field ... ] }
                              || { "tag_size": "Uint16" | "Uint8",
                                   "kind": $schema.kind,
                                   "cases": [ $union case ... ] }
                              || { "size":
                                     "Int16" | "Uint16" | "Int8" | "Uint8",
                                   "cases":
                                     [ [ integer ∈ [-2^30-2, 2^30+2],
                                         $unistring ] ... ] } } ... ] } },
              "error":
                { "json_schema": any,
                  "binary_schema":
                    { "toplevel":
                        { "fields": [ $schema.field ... ] }
                        || { "tag_size": "Uint16" | "Uint8",
                             "kind": $schema.kind,
                             "cases": [ $union case ... ] }
                        || { "size": "Int16" | "Uint16" | "Int8" | "Uint8",
                             "cases":
                               [ [ integer ∈ [-2^30-2, 2^30+2],
                                   $unistring ] ... ] },
                      "fields":
                        [ { "description":
                              { "title": $unistring,
                                "description"?: $unistring },
                            "encoding":
                              { "fields": [ $schema.field ... ] }
                              || { "tag_size": "Uint16" | "Uint8",
                                   "kind": $schema.kind,
                                   "cases": [ $union case ... ] }
                              || { "size":
                                     "Int16" | "Uint16" | "Int8" | "Uint8",
                                   "cases":
                                     [ [ integer ∈ [-2^30-2, 2^30+2],
                                         $unistring ] ... ] } } ... ] } } },
          "patch_service"?:
            { "meth": "PATCH" | "GET" | "POST" | "PUT" | "DELETE",
              "path":
                [ $unistring
                  || { "id": "single",
                       "name": $unistring,
                       "descr"?: $unistring }
                  || { "id": "multiple",
                       "name": $unistring,
                       "descr"?: $unistring } ... ],
              "description"?: $unistring,
              "query":
                [ { "name": $unistring,
                    "description"?: $unistring,
                    "kind":
                      { "single":
                          { "id": "single",
                            "name": $unistring,
                            "descr"?: $unistring } }
                      || { "optional":
                             { "id": "single",
                               "name": $unistring,
                               "descr"?: $unistring } }
                      || { "flag": {  } }
                      || { "multi":
                             { "id": "single",
                               "name": $unistring,
                               "descr"?: $unistring } } } ... ],
              "input"?:
                { "json_schema": any,
                  "binary_schema":
                    { "toplevel":
                        { "fields": [ $schema.field ... ] }
                        || { "tag_size": "Uint16" | "Uint8",
                             "kind": $schema.kind,
                             "cases": [ $union case ... ] }
                        || { "size": "Int16" | "Uint16" | "Int8" | "Uint8",
                             "cases":
                               [ [ integer ∈ [-2^30-2, 2^30+2],
                                   $unistring ] ... ] },
                      "fields":
                        [ { "description":
                              { "title": $unistring,
                                "description"?: $unistring },
                            "encoding":
                              { "fields": [ $schema.field ... ] }
                              || { "tag_size": "Uint16" | "Uint8",
                                   "kind": $schema.kind,
                                   "cases": [ $union case ... ] }
                              || { "size":
                                     "Int16" | "Uint16" | "Int8" | "Uint8",
                                   "cases":
                                     [ [ integer ∈ [-2^30-2, 2^30+2],
                                         $unistring ] ... ] } } ... ] } },
              "output":
                { "json_schema": any,
                  "binary_schema":
                    { "toplevel":
                        { "fields": [ $schema.field ... ] }
                        || { "tag_size": "Uint16" | "Uint8",
                             "kind": $schema.kind,
                             "cases": [ $union case ... ] }
                        || { "size": "Int16" | "Uint16" | "Int8" | "Uint8",
                             "cases":
                               [ [ integer ∈ [-2^30-2, 2^30+2],
                                   $unistring ] ... ] },
                      "fields":
                        [ { "description":
                              { "title": $unistring,
                                "description"?: $unistring },
                            "encoding":
                              { "fields": [ $schema.field ... ] }
                              || { "tag_size": "Uint16" | "Uint8",
                                   "kind": $schema.kind,
                                   "cases": [ $union case ... ] }
                              || { "size":
                                     "Int16" | "Uint16" | "Int8" | "Uint8",
                                   "cases":
                                     [ [ integer ∈ [-2^30-2, 2^30+2],
                                         $unistring ] ... ] } } ... ] } },
              "error":
                { "json_schema": any,
                  "binary_schema":
                    { "toplevel":
                        { "fields": [ $schema.field ... ] }
                        || { "tag_size": "Uint16" | "Uint8",
                             "kind": $schema.kind,
                             "cases": [ $union case ... ] }
                        || { "size": "Int16" | "Uint16" | "Int8" | "Uint8",
                             "cases":
                               [ [ integer ∈ [-2^30-2, 2^30+2],
                                   $unistring ] ... ] },
                      "fields":
                        [ { "description":
                              { "title": $unistring,
                                "description"?: $unistring },
                            "encoding":
                              { "fields": [ $schema.field ... ] }
                              || { "tag_size": "Uint16" | "Uint8",
                                   "kind": $schema.kind,
                                   "cases": [ $union case ... ] }
                              || { "size":
                                     "Int16" | "Uint16" | "Int8" | "Uint8",
                                   "cases":
                                     [ [ integer ∈ [-2^30-2, 2^30+2],
                                         $unistring ] ... ] } } ... ] } } },
          "subdirs"?:
            { "suffixes":
                [ { "name": $unistring,
                    "tree": $service_tree } ... ] }
            || { "dynamic_dispatch":
                   { "arg":
                       { "id": "single",
                         "name": $unistring,
                         "descr"?: $unistring },
                     "tree": $service_tree } } } }
    || { "dynamic": $unistring || null }
  $union case:
    { "tag": integer ∈ [-2^30-2, 2^30+2],
      "fields": [ $schema.field ... ],
      "name"?: $unistring }
  $unistring:
    /* Universal string representation
       Either a plain UTF8 string, or a sequence of bytes for strings that
       contain invalid byte sequences. */
    string || { "invalid_utf8_string": [ integer ∈ [0, 255] ... ] }
-}

data ServiceTree
  = ServiceTreeStatic
    { getService    :: Maybe Service
    , postService   :: Maybe Service
    , deleteService :: Maybe Service
    , patchService  :: Maybe Service
    , putService    :: Maybe Service
    }
  | ServiceTreeDynamic Unistring
  deriving (Eq, Show, Generic)

instance ToJSON ServiceTree where
  toJSON (ServiceTreeStatic getService' postService' deleteService' patchService' putService') =
    let innerObject =
          object $ catMaybes
            [ "get_service"    .=? getService'
            , "post_service"   .=? postService'
            , "delete_service" .=? deleteService'
            , "patch_service"  .=? patchService'
            , "put_service"    .=? putService'            
            ]
    in
    object
      ["static" .= innerObject]
  toJSON (ServiceTreeDynamic string') = object ["dynamic" .= string']

instance FromJSON ServiceTree

data Service =
  Service
    { meth :: ServiceMethod
    , path :: [ServicePath]
    , description :: Maybe Text
    , query :: [ServiceQuery]
    , input :: Maybe ServiceInput
    , output :: ServiceOutput
    , error :: ServiceOutput
    } deriving (Eq, Show, Generic)

instance ToJSON Service
instance FromJSON Service

data UnionCase =
  UnionCase
    { tag    :: Int64
    , fields :: [SchemaField]
    , name   :: Maybe Text
    } deriving (Eq, Show, Generic)

instance ToJSON UnionCase where
  toJSON (UnionCase tag' fields' name') =
    object $
      [ "tag"    .= tag'
      , "fields" .= fields'
      ] ++ catMaybes
      [ "name" .=? name'
      ]

instance FromJSON UnionCase
  
data ServiceMethod
  = Patch
  | Get
  | Post
  | Put
  | Delete
  deriving (Eq, Show, Generic)

instance ToJSON ServiceMethod
instance FromJSON ServiceMethod

data ServicePath
  = ServicePathString Text
  | ServicePathSingle { name :: Text, descr :: Maybe Text}
  | ServicePathMultiple { name :: Text, descr :: Maybe Text}
  deriving (Eq, Show, Generic)

instance ToJSON ServicePath
instance FromJSON ServicePath

data ServiceQuery =
  ServiceQuery
    { name        :: Text
    , description :: Maybe Text
    , kind        :: [ServiceInputKind]
    } deriving (Eq, Show, Generic)

instance ToJSON ServiceQuery
instance FromJSON ServiceQuery

data ServiceInputKind
  = ServiceInputKindSingle {name :: Text, descr :: Maybe Text}
  | ServiceInputKindOptional {name :: Text, descr :: Maybe Text}
  | ServiceInputKindFlag
  | ServiceInputKindMulti {name :: Text, descr :: Maybe Text}
  deriving (Eq, Show, Generic)

instance ToJSON ServiceInputKind
instance FromJSON ServiceInputKind

data ServiceInput =
  ServiceInput
    { name :: Text
    , description :: Maybe Text
    , kind :: ServiceInputKind
    } deriving (Eq, Show, Generic)

instance ToJSON ServiceInput
instance FromJSON ServiceInput

data BinarySchemaEncoding
  = BinarySchemaToplevelFields [SchemaField]
  | BinarySchemaToplevelKind Text SchemaKind [UnionCase] --"Uint16" | "Uint8",
  | BinarySchemaToplevelCases Text [[(Int64, Text)]] -- "Int16" | "Uint16" | "Int8" | "Uint8",
  deriving (Eq, Show, Generic)

instance ToJSON BinarySchemaEncoding
instance FromJSON BinarySchemaEncoding

data BinarySchemaFieldDescription =
  BinarySchemaFieldDescription
    { title :: Text
    , description :: Maybe Text
    } deriving (Eq, Show, Generic)

instance ToJSON BinarySchemaFieldDescription
instance FromJSON BinarySchemaFieldDescription

data BinarySchemaField =
  BinarySchemaField
    { description :: BinarySchemaFieldDescription
    , encoding :: [BinarySchemaEncoding]
    } deriving (Eq, Show, Generic)

instance ToJSON BinarySchemaField
instance FromJSON BinarySchemaField

data BinarySchema =
  BinarySchema
    { toplevel :: BinarySchemaEncoding
    , fields   :: [BinarySchemaField]
    } deriving (Eq, Show, Generic)

instance ToJSON BinarySchema
instance FromJSON BinarySchema

data ServiceOutput =
  ServiceOutput
    { jsonSchema   :: Value
    , binarySchema :: BinarySchema
    } deriving (Eq, Show, Generic)

instance ToJSON ServiceOutput
instance FromJSON ServiceOutput

data SchemaKind
  = SchemaKindFloat Int64
  | SchemaKindDynamic
  | SchemaKindVariable
  deriving (Eq, Show, Generic)

instance ToJSON SchemaKind where
  toJSON (SchemaKindFloat i) = object ["kind" .= ("Float" :: Text), "size" .= i]
  toJSON SchemaKindDynamic   = object ["kind" .= ("Dynamic" :: Text)]
  toJSON SchemaKindVariable  = object ["kind" .= ("Variable" :: Text)]

instance FromJSON SchemaKind

data SchemaField
  = SchemaFieldNamed
    { name :: Text
    , layout :: Layout
    , dataKind :: SchemaKind
    }
  | SchemaFieldAnon
    { layout :: Layout
    , dataKind :: SchemaKind
    }
  | SchemaFieldDyn
    { mname :: Maybe Text
    , numFields :: Int64
    , layout :: Layout
    , size :: IntSize
    }
  | SchemaFieldOptionIndictor
    { name :: Text
    }
  deriving (Eq, Show, Generic)

instance ToJSON SchemaField where
  toJSON (SchemaFieldNamed name' layout' dataKind') =
    object
      [ "name"     .= name'
      , "layout"   .= layout'
      , "dataKind" .= dataKind'
      , "kind"     .= ("named" :: Text)
      ]
  toJSON (SchemaFieldAnon layout' dataKind') =
    object
      [ "layout"   .= layout'
      , "dataKind" .= dataKind'
      , "kind"     .= ("anon" :: Text)
      ]
  toJSON (SchemaFieldDyn mname' numFields' layout' size') =
    object
      [ "name"       .= mname'
      , "num_fields" .= numFields'
      , "layout"     .= layout'
      , "size"       .= size'
      , "kind"       .= ("dyn" :: Text)
      ]
  toJSON (SchemaFieldOptionIndictor name') =
    object
      [ "name" .= name'
      , "kind" .= ("option_indicator" :: Text)
      ]

instance FromJSON SchemaField

data Layout
  = LZeroWidth
  | LInt IntSize
  | LBool
  | LRangedInt Int64 Int64
  | LRangedFloat Double Double
  | LFloat
  | LBytes
  | LString
  | LEnum EnumSize Text
  | LSeq Layout
  | LRef Text
  | LPadding
  deriving (Eq, Show, Generic)

instance ToJSON Layout where
  toJSON LZeroWidth   = object ["kind" .= ("Zero_width" :: Text)]
  toJSON (LInt s)     = object ["kind" .= ("Int" :: Text), "size" .= s]
  toJSON LBool        = object ["kind" .= ("Bool" :: Text)]
  toJSON (LRangedInt mn mx)   = object ["kind" .= ("RangedInt" :: Text), "min" .= mn, "max" .= mx]
  toJSON (LRangedFloat mn mx) = object ["kind" .= ("RangedFloat" :: Text), "min" .= mn, "max" .= mx]
  toJSON LFloat       = object ["kind" .= ("Float" :: Text)]
  toJSON LBytes       = object ["kind" .= ("Bytes" :: Text)]
  toJSON LString      = object ["kind" .= ("String" :: Text)]
  toJSON (LEnum s r)  = object ["kind" .= ("Enum" :: Text), "size" .= s, "reference" .= r]
  toJSON (LSeq l)     = object ["kind" .= ("Seq" :: Text), "layout" .= l]
  toJSON (LRef n)     = object ["kind" .= ("Ref" :: Text), "name" .= n]
  toJSON LPadding     = object ["kind" .= ("Padding" :: Text)]

instance FromJSON Layout


data IntSize = IInt32 | IInt16 | IUint16 | IInt64 | IInt8 | IUint8
  deriving (Eq, Show, Generic)

instance ToJSON IntSize where
  toJSON IInt32  = "Int32"
  toJSON IInt16  = "Int16"
  toJSON IUint16 = "Uint16"
  toJSON IInt64  = "Int64"
  toJSON IInt8   = "Int8"

  toJSON IUint8  = "Uint8"

instance FromJSON IntSize

data EnumSize = EInt16 | EUint16 | EInt8 | EUint8
  deriving (Eq, Show, Generic)

instance ToJSON EnumSize where
  toJSON EInt16  = "Int16"
  toJSON EUint16 = "Uint16"
  toJSON EInt8   = "Int8"
  toJSON EUint8  = "Uint8"

instance FromJSON EnumSize
