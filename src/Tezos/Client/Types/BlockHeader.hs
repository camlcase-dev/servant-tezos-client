{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings     #-}

module Tezos.Client.Types.BlockHeader where

import Data.Aeson
  (FromJSON(parseJSON), ToJSON(toJSON), (.=), (.:), (.:?), object, withObject)
import Data.Int (Int64)
import Data.Maybe (catMaybes)
import Data.Text (Text)
import Data.Word (Word8, Word16)
import GHC.Generics (Generic)
import Tezos.Client.Types.Core
import Tezos.Client.Types.Crypto
import Tezos.Client.Extension.Aeson ((.=?))

-- $block_header:
--   { "protocol": "Pt24m4xiPbLDhVgVfABUjirbmda3yohdN82Sp9FeuAXJ4eV9otd",
--     "chain_id": $Chain_id,
--     "hash": $block_hash,
--     "level": integer ∈ [-2^31-2, 2^31+2],
--     "proto": integer ∈ [0, 255],
--     "predecessor": $block_hash,
--     "timestamp": $timestamp,
--     "validation_pass": integer ∈ [0, 255],
--     "operations_hash": $Operation_list_list_hash,
--     "fitness": $fitness,
--     "context": $Context_hash,
--     "priority": integer ∈ [0, 2^16-1],
--     "proof_of_work_nonce": /^[a-zA-Z0-9]+$/,
--     "seed_nonce_hash"?: $cycle_nonce,
--     "signature": $Signature }

data BlockHeader =
  BlockHeader
    { protocol         :: Unistring
    , chainId          :: ChainId
    , hash             :: BlockHash
    , level            :: Int64
    , proto            :: Word8
    , predecessor      :: BlockHash
    , timestamp        :: Timestamp
    , validationPass   :: Word8
    , operationsHash   :: Unistring
    , fitness          :: Fitness
    , context          :: ContextHash
    , priority         :: Word16
    , proofOfWorkNonce :: Text
    , seedNonceHash    :: Maybe CycleNonce
    , signature        :: Signature
    } deriving (Eq, Show, Generic)

instance ToJSON BlockHeader where
  toJSON (BlockHeader protocol' chainId' hash' level' proto' predecessor'
          timestamp' validationPass' operationsHash' fitness' context' priority'
          proofOfWorkNonce' seedNonceHash' signature') =
    object $
      [ "protocol"            .= protocol'
      , "chain_id"            .= chainId'
      , "hash"                .= hash'
      , "level"               .= level'
      , "proto"               .= proto'
      , "predecessor"         .= predecessor'
      , "timestamp"           .= timestamp'
      , "validation_pass"     .= validationPass'
      , "operations_hash"     .= operationsHash'
      , "fitness"             .= fitness'
      , "context"             .= context'
      , "priority"            .= priority'
      , "proof_of_work_nonce" .= proofOfWorkNonce'
      , "signature"           .= signature'
      ] ++ catMaybes ["seed_nonce_hash" .=? seedNonceHash']

instance FromJSON BlockHeader where
  parseJSON = withObject "BlockHeader" $ \o ->
    BlockHeader <$> o .:  "protocol"
                <*> o .:  "chain_id"
                <*> o .:  "hash"
                <*> o .:  "level"
                <*> o .:  "proto"
                <*> o .:  "predecessor"
                <*> o .:  "timestamp"
                <*> o .:  "validation_pass"
                <*> o .:  "operations_hash"
                <*> o .:  "fitness"
                <*> o .:  "context"
                <*> o .:  "priority"
                <*> o .:  "proof_of_work_nonce"
                <*> o .:? "seed_nonce_hash"
                <*> o .:  "signature"
