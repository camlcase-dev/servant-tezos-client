{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings     #-}

module Tezos.Client.Types.PointLog where

import Data.Aeson
  (FromJSON(parseJSON), ToJSON(toJSON), (.=), (.:), object,  withObject)
import Data.Aeson.Types (Parser)
import Data.Text (Text)
import GHC.Generics (Generic)
import Tezos.Client.Types.Core
import Tezos.Client.Types.Crypto

data PointLog =
  PointLog
    { kind :: PointLogKind
    , timestamp :: Timestamp
    } deriving (Eq, Show, Generic)

instance ToJSON PointLog where
  toJSON (PointLog kind' timestamp') =
    object
      [ "kind" .= kind'
      , "timestamp" .= timestamp'
      ]

instance FromJSON PointLog where
  parseJSON = withObject "PointLog" $ \o ->
    PointLog <$> o .: "kind"
             <*> o .: "timestamp"

data PointLogKind
  = PKOutgoingRequest
  | PKAcceptingRequest PublicKeyHash
  | PKRejectingRequest PublicKeyHash
  | PKRequestRejected  (Maybe PublicKeyHash)
  deriving (Eq, Show, Generic)

instance ToJSON PointLogKind where
  toJSON PKOutgoingRequest =
    object ["event_kind" .= ("outgoing_request" :: Text)]
  toJSON (PKAcceptingRequest p2pPeerId) =
    object
      [ "event_kind" .= ("accepting_request" :: Text)
      , "p2p_peer_id" .= p2pPeerId
      ]
  toJSON (PKRejectingRequest p2pPeerId) =
    object
      [ "event_kind" .= ("rejecting_request" :: Text)
      , "p2p_peer_id" .= p2pPeerId
      ]
  toJSON (PKRequestRejected p2pPeerId) =
    object
      [ "event_kind" .= ("request_rejected" :: Text)
      , "p2p_peer_id" .= p2pPeerId
      ]

instance FromJSON PointLogKind where
  parseJSON = withObject "PointLogKind" $ \o -> do
    let match s =
          case s of
            "outgoing_request"  -> pure PKOutgoingRequest
            "accepting_request" -> PKAcceptingRequest <$> o .: "p2p_peer_id"
            "rejecting_request" -> PKRejectingRequest <$> o .: "p2p_peer_id"
            "request_rejected"  -> PKRequestRejected  <$> o .: "p2p_peer_id"
            _ -> fail $ "expected P2PEvent, encountered " ++ (show s)
    eventKind <- (o .: "event_kind") :: Parser Text
    match eventKind
