{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings     #-}

module Tezos.Client.Types.ContractStatus where

import Data.Aeson
  (FromJSON(parseJSON), ToJSON(toJSON), (.=), (.:), (.:?), object, withObject)
import Data.Maybe (catMaybes)
import Data.Word (Word64)
import GHC.Generics (Generic)
import Tezos.Client.Types.Core
import Tezos.Client.Types.Crypto
import Tezos.Client.Types.Micheline
import Tezos.Client.Extension.Aeson ((.=?))

-- { "manager": $Signature.Public_key_hash,
--   "balance": $mutez,
--   "spendable": boolean,
--   "delegate": { "setable": boolean,
--                 "value"?: $Signature.Public_key_hash },
--   "script"?: $scripted.contracts,
--   "counter": $positive_bignum }

data ContractStatus =
  ContractStatus
    { manager   :: PublicKeyHash
    , balance   :: Mutez
    , spendable :: Bool
    , delegate  :: Delegate
    , script    :: Maybe Contract
    , counter   :: Word64
    } deriving (Eq, Show, Generic)

instance ToJSON ContractStatus where
  toJSON (ContractStatus manager' balance' spendable' delegate' script'
          counter') =
    object $
      [ "manager"   .= manager'
      , "balance"   .= balance'
      , "spendable" .= spendable'
      , "delegate"  .= delegate'
      , "counter"   .= counter'
      ] ++ catMaybes ["script" .=? script']

instance FromJSON ContractStatus where
  parseJSON = withObject "ContractStatus" $ \o ->
    ContractStatus <$> o .:  "manager"
                   <*> o .:  "balance"
                   <*> o .:  "spendable"
                   <*> o .:  "delegate"
                   <*> o .:? "script"
                   <*> o .:  "counter"

--   "delegate":
--     { "setable": boolean,
--       "value"?: $Signature.Public_key_hash }

data Delegate =
  Delegate
    { setable :: Bool
    , value   :: Maybe PublicKeyHash
    } deriving (Eq, Show, Generic)

instance ToJSON Delegate where
  toJSON (Delegate setable' value') =
    object $
      [ "setable" .= setable'
      ] ++ catMaybes ["value" .=? value']
      
instance FromJSON Delegate where
  parseJSON = withObject "Delegate" $ \o ->
    Delegate <$> o .:  "setable"
             <*> o .:? "value"
