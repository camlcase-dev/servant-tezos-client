{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE OverloadedStrings     #-}

module Tezos.Client.Types.Peer where

import Data.Aeson
  (FromJSON(parseJSON), ToJSON(toJSON), (.=), (.:), (.:?), object, withObject,
   withText)
import Data.Int (Int64)
import Data.Maybe (catMaybes)
import Data.Word (Word16)
import GHC.Generics (Generic)
import Tezos.Client.Types.Core
import Tezos.Client.Types.Connection
import Tezos.Client.Extension.Aeson ((.=?))

-- [ [ $Crypto_box.Public_key_hash,
--     { "score": number,
--       "trusted": boolean,
--       "conn_metadata"?:
--         { "disable_mempool": boolean,
--           "private_node": boolean },
--       "peer_metadata":
--         { "responses":
--             { "sent":
--                 { "branch": $bignum,
--                   "head": $bignum,
--                   "block_header": $bignum,
--                   "operations": $bignum,
--                   "protocols": $bignum,
--                   "operation_hashes_for_block": $bignum,
--                   "operations_for_block": $bignum,
--                   "other": $bignum },
--               "failed":
--                 { "branch": $bignum,
--                   "head": $bignum,
--                   "block_header": $bignum,
--                   "operations": $bignum,
--                   "protocols": $bignum,
--                   "operation_hashes_for_block": $bignum,
--                   "operations_for_block": $bignum,
--                   "other": $bignum },
--               "received":
--                 { "branch": $bignum,
--                   "head": $bignum,
--                   "block_header": $bignum,
--                   "operations": $bignum,
--                   "protocols": $bignum,
--                   "operation_hashes_for_block": $bignum,
--                   "operations_for_block": $bignum,
--                   "other": $bignum },
--               "unexpected": $bignum,
--               "outdated": $bignum },
--           "requests":
--             { "sent":
--                 { "branch": $bignum,
--                   "head": $bignum,
--                   "block_header": $bignum,
--                   "operations": $bignum,
--                   "protocols": $bignum,
--                   "operation_hashes_for_block": $bignum,
--                   "operations_for_block": $bignum,
--                   "other": $bignum },
--               "received":
--                 { "branch": $bignum,
--                   "head": $bignum,
--                   "block_header": $bignum,
--                   "operations": $bignum,
--                   "protocols": $bignum,
--                   "operation_hashes_for_block": $bignum,
--                   "operations_for_block": $bignum,
--                   "other": $bignum },
--               "failed":
--                 { "branch": $bignum,
--                   "head": $bignum,
--                   "block_header": $bignum,
--                   "operations": $bignum,
--                   "protocols": $bignum,
--                   "operation_hashes_for_block": $bignum,
--                   "operations_for_block": $bignum,
--                   "other": $bignum },
--               "scheduled":
--                 { "branch": $bignum,
--                   "head": $bignum,
--                   "block_header": $bignum,
--                   "operations": $bignum,
--                   "protocols": $bignum,
--                   "operation_hashes_for_block": $bignum,
--                   "operations_for_block": $bignum,
--                   "other": $bignum } },
--           "valid_blocks": $bignum,
--           "old_heads": $bignum,
--           "prevalidator_results":
--             { "cannot_download": $bignum,
--               "cannot_parse": $bignum,
--               "refused_by_prefilter": $bignum,
--               "refused_by_postfilter": $bignum,
--               "applied": $bignum,
--               "branch_delayed": $bignum,
--               "branch_refused": $bignum,
--               "refused": $bignum,
--               "duplicate": $bignum,
--               "outdated": $bignum },
--           "unactivated_chains": $bignum,
--           "inactive_chains": $bignum,
--           "future_blocks_advertised": $bignum,
--           "unadvertised":
--             { "block": $bignum,
--               "operations": $bignum,
--               "protocol": $bignum },
--           "advertisements":
--             { "sent": { "head": $bignum,
--                         "branch": $bignum },
--               "received": { "head": $bignum,
--                             "branch": $bignum } } },
--       "state": "running" | "accepted" | "disconnected",
--       "reachable_at"?:
--         { "addr": $unistring,
--           "port"?: integer ∈ [0, 2^16-1] },
--       "stat":
--         { "total_sent": $int64,
--           "total_recv": $int64,
--           "current_inflow": integer ∈ [-2^30-2, 2^30+2],
--           "current_outflow": integer ∈ [-2^30-2, 2^30+2] },
--       "last_failed_connection"?:
--         [ { "addr": $unistring,
--             "port"?: integer ∈ [0, 2^16-1] },
--           $timestamp ],
--       "last_rejected_connection"?:
--         [ { "addr": $unistring,
--             "port"?: integer ∈ [0, 2^16-1] },
--           $timestamp ],
--       "last_established_connection"?:
--         [ { "addr": $unistring,
--             "port"?: integer ∈ [0, 2^16-1] },
--           $timestamp ],
--       "last_disconnection"?:
--         [ { "addr": $unistring,
--             "port"?: integer ∈ [0, 2^16-1] },
--           $timestamp ],
--       "last_seen"?:
--         [ { "addr": $unistring,
--             "port"?: integer ∈ [0, 2^16-1] },
--           $timestamp ],
--       "last_miss"?:
--         [ { "addr": $unistring,
--             "port"?: integer ∈ [0, 2^16-1] },
--           $timestamp ] } ] ... ]

data Peer =
  Peer
    { score                     :: Integer
    , trusted                   :: Bool
    , connMetadata              :: Maybe P2PMetadata
    , peerMetadata              :: PeerMetadata
    , peerState                 :: PeerState
    , reachableAt               :: AddrPort
    , peerStat                  :: PeerStat
    , lastFailedConnection      :: Maybe [(AddrPort, Timestamp)]
    , lastRejectedConnection    :: Maybe [(AddrPort, Timestamp)]
    , lastEstablishedConnection :: Maybe [(AddrPort, Timestamp)]
    , lastDisconnection         :: Maybe [(AddrPort, Timestamp)]
    , lastSeen                  :: Maybe [(AddrPort, Timestamp)]
    , lastMiss                  :: Maybe [(AddrPort, Timestamp)]
    } deriving (Eq, Show, Generic)

instance ToJSON Peer where
  toJSON (Peer score' trusted' connMetadata' peerMetadata' peerState'
          reachableAt' peerStat' lastFailedConnection'
          lastRejectedConnection' lastEstablishedConnection'
          lastDisconnection' lastSeen' lastMiss') =
    object $
      [ "score"                       .= score'
      , "trusted"                     .= trusted'
      , "peer_metadata"               .= peerMetadata'
      , "peer_state"                  .= peerState'
      , "reachable_at"                .= reachableAt'
      , "peer_stat"                   .= peerStat'
      ] ++ catMaybes
      [ "conn_metadata"               .=? connMetadata'
      , "last_failed_connection"      .=? lastFailedConnection'
      , "last_rejected_connection"    .=? lastRejectedConnection'
      , "last_established_connection" .=? lastEstablishedConnection'
      , "last_disconnection"          .=? lastDisconnection'
      , "last_seen"                   .=? lastSeen'
      , "last_miss"                   .=? lastMiss'      
      ]

instance FromJSON Peer where
  parseJSON = withObject "Peer" $ \o ->
    Peer <$> o .:  "score"
         <*> o .:  "trusted"
         <*> o .:? "conn_metadata"
         <*> o .:  "peer_metadata"
         <*> o .:  "peer_state"
         <*> o .:  "reachable_at"
         <*> o .:  "peer_stat"
         <*> o .:? "last_failed_connection"
         <*> o .:? "last_rejected_connection"
         <*> o .:? "last_established_connection"
         <*> o .:? "last_disconnection"
         <*> o .:? "last_seen"
         <*> o .:? "last_miss"


data PeerMetadata =
  PeerMetadata
    { responses              :: PeerResponse
    , requests               :: PeerRequest
    , oldHeads               :: Bignum
    , prevalidatorResults    :: PrevalidatorResults
    , unactivatedChains      :: Bignum
    , inactiveChains         :: Bignum
    , futureBlocksAdvertised :: Bignum
    , unadvertised           :: Unadvertised
    , advertisements         :: Advertisements
    } deriving (Eq, Show, Generic)

instance ToJSON PeerMetadata where
  toJSON (PeerMetadata responses' requests' oldHeads' prevalidatorResults'
          unactivatedChains' inactiveChains' futureBlocksAdvertised'
          unadvertised' advertisements') =
    object
      [ "responses"                .= responses'
      , "requests"                 .= requests'
      , "old_heads"                .= oldHeads'
      , "prevalidator_results"     .= prevalidatorResults'
      , "unactivated_chains"       .= unactivatedChains'
      , "inactive_chains"          .= inactiveChains'
      , "future_blocks_advertised" .= futureBlocksAdvertised'
      , "unadvertised"             .= unadvertised'
      , "advertisements"           .= advertisements'
      ]

instance FromJSON PeerMetadata where
  parseJSON = withObject "PeerMetadata" $ \o ->
    PeerMetadata <$> o .: "responses"
                 <*> o .: "requests"
                 <*> o .: "old_heads"
                 <*> o .: "prevalidator_results"
                 <*> o .: "unactivated_chains"
                 <*> o .: "inactive_chains"
                 <*> o .: "future_blocks_advertised"
                 <*> o .: "unadvertised"
                 <*> o .: "advertisements"

data PeerResponse =
  PeerResponse
    { sent       :: BlockData
    , failed     :: BlockData
    , received   :: BlockData
    , unexpected :: BlockData
    , outdated   :: BlockData
    } deriving (Eq, Show, Generic)

instance ToJSON PeerResponse where
  toJSON (PeerResponse sent' failed' received' unexpected' outdated') =
    object
      [ "sent"       .= sent'
      , "failed"     .= failed'
      , "received"   .= received'
      , "unexpected" .= unexpected'
      , "outdated"   .= outdated'
      ]

instance FromJSON PeerResponse where
  parseJSON = withObject "PeerResponse" $ \o ->
    PeerResponse <$> o .: "sent"
                 <*> o .: "failed"
                 <*> o .: "received"
                 <*> o .: "unexpected"
                 <*> o .: "outdated"

type PeerRequest = PeerResponse

-- { "branch": $bignum,
--   "head": $bignum,
--   "block_header": $bignum,
--   "operations": $bignum,
--   "protocols": $bignum,
--   "operation_hashes_for_block": $bignum,
--   "operations_for_block": $bignum,
--   "other": $bignum }

data BlockData =
  BlockData
    { branch                  :: Bignum
    , head_                   :: Bignum
    , blockHeader             :: Bignum
    , operations              :: Bignum
    , protocols               :: Bignum
    , operationHashesForBlock :: Bignum
    , operationsForBlock      :: Bignum
    , other                   :: Bignum
    } deriving (Eq, Show, Generic)

instance ToJSON BlockData where
  toJSON (BlockData branch' blockHead' blockHeader' operations' protocols' operationHashesForBlock' operationsForBlock' other') =
    object
      [ "branch"                     .= branch'
      , "head"                       .= blockHead'
      , "block_header"               .= blockHeader'
      , "operations"                 .= operations'
      , "protocols"                  .= protocols'
      , "operation_hashes_for_block" .= operationHashesForBlock'
      , "operations_for_block"       .= operationsForBlock'
      , "other"                      .= other'
      ]

instance FromJSON BlockData where
  parseJSON = withObject "BlockData" $ \o ->
    BlockData <$> o .: "branch"
              <*> o .: "head"
              <*> o .: "block_header"
              <*> o .: "operations"
              <*> o .: "protocols"
              <*> o .: "operation_hashes_for_block"
              <*> o .: "operations_for_block"
              <*> o .: "other"

data AddrPort =
  AddrPort
    { apAddr :: Unistring
    , apPort :: Maybe Word16
    } deriving (Eq, Show, Generic)

instance ToJSON AddrPort
instance FromJSON AddrPort

data Unadvertised =
  Unadvertised
    { block :: Integer
    , operations :: Integer
    , protocol :: Integer
    } deriving (Eq, Show, Generic)

instance ToJSON Unadvertised where
  toJSON (Unadvertised block' operations' protocol') =
    object
      [ "block" .= block'
      , "operations" .= operations'
      , "protocol" .= protocol'
      ]

instance FromJSON Unadvertised where
  parseJSON = withObject "Unadvertised" $ \o ->
    Unadvertised <$> o .: "block"
                 <*> o .: "operations"
                 <*> o .: "protocol"

data PrevalidatorResults =
  PrevalidatorResults
    { cannotDownload :: Integer
    , cannotParse :: Integer
    , refusedByPrefilter :: Integer
    , refusedByPostfilter :: Integer
    , applied :: Integer
    , branchDelayed :: Integer
    , branchRefused :: Integer
    , refused :: Integer
    , duplicate :: Integer
    , outdated :: Integer
    } deriving (Eq, Show, Generic)

instance ToJSON PrevalidatorResults where
  toJSON (PrevalidatorResults cannotDownload' cannotParse' refusedByPrefilter' refusedByPostfilter' applied' branchDelayed' branchRefused' refused' duplicate' outdated') =
    object
      [ "cannot_download" .= cannotDownload'
      , "cannot_parse" .= cannotParse'
      , "refused_by_prefilter" .= refusedByPrefilter'
      , "refused_by_postfilter" .= refusedByPostfilter'
      , "applied" .= applied'
      , "branch_delayed" .= branchDelayed'
      , "branch_refused" .= branchRefused'
      , "refused" .= refused'
      , "duplicate" .= duplicate'
      , "outdated" .= outdated'
      ]

instance FromJSON PrevalidatorResults where
  parseJSON = withObject "PrevalidatorResults" $ \o ->
    PrevalidatorResults <$> o .: "cannot_download"
                        <*> o .: "cannot_parse"
                        <*> o .: "refused_by_prefilter"
                        <*> o .: "refused_by_postfilter"
                        <*> o .: "applied"
                        <*> o .: "branch_delayed"
                        <*> o .: "branch_refused"
                        <*> o .: "refused"
                        <*> o .: "duplicate"
                        <*> o .: "outdated"

data PeerStat =
  PeerStat
    { totalSent      :: Int64
    , totalRecv      :: Int64
    , currentInflow  :: Int64
    , currentOutflow :: Int64
    } deriving (Eq, Show, Generic)

instance ToJSON PeerStat
instance FromJSON PeerStat


data PeerState
  = PSRunning
  | PSAccepted
  | PSDisconnected
  deriving (Eq, Show, Generic)

instance ToJSON PeerState where
  toJSON = \case
    PSRunning      -> "running"
    PSAccepted     -> "accepted"
    PSDisconnected -> "disconnected"

instance FromJSON PeerState where
  parseJSON = withText "PeerState" $ \t ->
    case t of
      "running"      -> pure PSRunning
      "accepted"     -> pure PSAccepted
      "disconnected" -> pure PSDisconnected
      _              -> fail $ "expected PeerState, encountered " ++ (show t)

data Advertisements =
  Advertisements
    { sent :: HeadBranch
    , received :: HeadBranch
    } deriving (Eq, Show, Generic)

instance ToJSON Advertisements where
  toJSON (Advertisements sent' received') =
    object
      [ "sent" .= sent'
      , "received" .= received'
      ]

instance FromJSON Advertisements where
  parseJSON = withObject "Advertisements" $ \o ->
    Advertisements <$> o .: "sent"
                   <*> o .: "received"

data HeadBranch =
  HeadBranch
    { branchHead :: Integer
    , branch :: Integer
    } deriving (Eq, Show, Generic)

instance ToJSON HeadBranch where
  toJSON (HeadBranch branchHead' branch') =
    object
      [ "head" .= branchHead'
      , "branch" .= branch'
      ]

instance FromJSON HeadBranch where
  parseJSON = withObject "HeadBranch" $ \o ->
    HeadBranch <$> o .: "head"
               <*> o .: "branch"
