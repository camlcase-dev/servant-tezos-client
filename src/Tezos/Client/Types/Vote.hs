{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings     #-}

module Tezos.Client.Types.Vote where

import Data.Aeson
  (FromJSON(parseJSON), ToJSON(toJSON), (.=), (.:), object, withObject, withText)
import Data.Int (Int64)
import GHC.Generics (Generic)
import Tezos.Client.Types.Core
import Tezos.Client.Types.Crypto

-- [ { "pkh": $Signature.Public_key_hash,
--     "ballot": "nay" | "yay" | "pass" } ... ]

data Vote =
  Vote
    { pkh     :: PublicKeyHash
    , vballot :: Ballot
    }  deriving (Eq, Show, Generic)

instance ToJSON Vote where
  toJSON (Vote pkh' ballot') =
    object
      [ "pkh"    .= pkh'
      , "ballot" .= ballot'
      ]

instance FromJSON Vote where
  parseJSON = withObject "Vote" $ \o ->
    Vote <$> o .: "pkh"
         <*> o .: "ballot"

-- { "yay": integer ∈ [-2^31-2, 2^31+2],
--   "nay": integer ∈ [-2^31-2, 2^31+2],
--   "pass": integer ∈ [-2^31-2, 2^31+2] }

data BallotCount =
  BallotCount
    { yay  :: Int64
    , nay  :: Int64
    , pass :: Int64
    } deriving (Eq, Show, Generic)

instance ToJSON BallotCount where
  toJSON (BallotCount yay' nay' pass') =
    object
      [ "yay"  .= yay'
      , "nay"  .= nay'
      , "pass" .= pass'
      ] 

instance FromJSON BallotCount where
  parseJSON = withObject "BallotCount" $ \o ->
    BallotCount <$> o .: "yay"
                <*> o .: "nay"
                <*> o .: "pass"

-- "voting_period_kind":
--   "proposal" || "testing_vote" || "testing" || "promotion_vote" || "adoption"

data VotingPeriodKind
  = Proposal
  | TestingVote
  | Testing
  | PromotionVote
  | Adoption
  deriving (Eq, Show, Generic)

instance ToJSON VotingPeriodKind where
  toJSON Proposal      = "proposal"
  toJSON TestingVote   = "testing_vote"
  toJSON Testing       = "testing"
  toJSON PromotionVote = "promotion_vote"
  toJSON Adoption      = "adoption"

instance FromJSON VotingPeriodKind where
  parseJSON = withText "VotingPeriodKind" $ \t ->
    case t of
      "proposal"       -> pure Proposal
      "testing_vote"   -> pure TestingVote
      "testing"        -> pure Testing
      "promotion_vote" -> pure PromotionVote
      "adoption"       -> pure Adoption
      _ -> fail $ "Unknown VotingPeriodKind: " <> show t

-- [ { "pkh": $Signature.Public_key_hash,
--     "rolls": integer ∈ [-2^31-2, 2^31+2] } ... ]

data DelegateWeight =
  DelegateWeight
    { pkh :: PublicKeyHash
    , rolls :: Int64
    } deriving (Eq, Show, Generic)

instance ToJSON DelegateWeight where
  toJSON (DelegateWeight pkh' rolls') =
    object
      [ "pkh"   .= pkh'
      , "rolls" .= rolls'
      ]

instance FromJSON DelegateWeight where
  parseJSON = withObject "DelegateWeight" $ \o ->
    DelegateWeight <$> o .: "pkh"
                   <*> o .: "rolls"
