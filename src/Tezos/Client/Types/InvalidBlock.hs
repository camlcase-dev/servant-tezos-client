{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings     #-}

module Tezos.Client.Types.InvalidBlock where

import Data.Aeson
  (ToJSON(toJSON), FromJSON(parseJSON), (.=), (.:), withObject, object)
import Data.Int (Int64)
import GHC.Generics (Generic)
import Tezos.Client.Types.Core

-- [ { "block": $block_hash,
--      "level": integer ∈ [-2^31-2, 2^31+2],
--      "errors": $error } ... ]

data InvalidBlock =
  InvalidBlock
    { block  :: BlockHash
    , level  :: Int64
    , errors :: Error
    } deriving (Eq, Show, Generic)

instance ToJSON InvalidBlock where
  toJSON (InvalidBlock block' level' errors') =
    object
      [ "block"  .= block'
      , "level"  .= level'
      , "errors" .= errors'
      ]

instance FromJSON InvalidBlock where
  parseJSON = withObject "InvalidBlock" $ \o ->
    InvalidBlock <$> o .: "block"
                 <*> o .: "level"
                 <*> o .: "errors"
