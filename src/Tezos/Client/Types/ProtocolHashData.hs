{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings     #-}

module Tezos.Client.Types.ProtocolHashData where

import Data.Aeson
  (FromJSON(parseJSON), ToJSON(toJSON), (.=), (.:), (.:?), object, withObject)
import Data.Int (Int32)
import Data.Maybe (catMaybes)
import Data.Text (Text)
import GHC.Generics (Generic)
import Tezos.Client.Extension.Aeson ((.=?))

-- "expected_env_version": integer ∈ [-2^15, 2^15-1],
--   "components":
--     [ { "name": $unistring,
--         "interface"?: /^[a-zA-Z0-9]+$/,
--         "implementation": /^[a-zA-Z0-9]+$/ } ... ] }

data ProtocolHashData =
  ProtocolHashData
    { expectedEnvVersion :: Int32
    , components         :: [ProtocolComponent]
    } deriving (Eq, Show, Generic)

instance ToJSON ProtocolHashData where
  toJSON (ProtocolHashData expectedEnvVersion' components') =
    object
      [ "expected_env_version" .= expectedEnvVersion'
      , "components"           .= components'
      ]
    
instance FromJSON ProtocolHashData where
  parseJSON = withObject "ProtocolData" $ \o ->
    ProtocolHashData <$> o .: "expected_env_version"
                     <*> o .: "components"

-- { "name": $unistring,
--   "interface"?: /^[a-zA-Z0-9]+$/,
--   "implementation": /^[a-zA-Z0-9]+$/ }

data ProtocolComponent =
  ProtocolComponent
    { name           :: Text
    , interface      :: Maybe Text
    , implementation :: Text
    } deriving (Eq, Show, Generic)

instance ToJSON ProtocolComponent where
  toJSON (ProtocolComponent name' interface' implementation') =
    object $
      [ "name"           .= name'
      , "implementation" .= implementation'
      ] ++ catMaybes
      [ "interface"      .=? interface'
      ]

instance FromJSON ProtocolComponent where
  parseJSON = withObject "ProtocolComponent" $ \o ->
    ProtocolComponent <$> o .:  "name"
                      <*> o .:? "interface"
                      <*> o .:  "implementation"

