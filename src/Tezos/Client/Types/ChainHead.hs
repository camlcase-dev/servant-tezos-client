{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings     #-}

module Tezos.Client.Types.ChainHead where

import Data.Aeson
  (FromJSON(parseJSON), ToJSON(toJSON), (.=), (.:), object, withObject)
import Data.Int (Int64)
import Data.Text (Text)
import Data.Word (Word16)
import GHC.Generics (Generic)
import Tezos.Client.Types.Core

data ChainHead =
  ChainHead
    { hash           :: BlockHash
    , level          :: Int64
    , proto          :: Word16
    , predecessor    :: BlockHash
    , timestamp      :: Timestamp
    , validationPass :: Word16
    , operationsHash :: [[InternalByteString]]
    , fitness        :: Fitness
    , context        :: ContextHash
    , protocolData   :: Text
    } deriving (Eq, Show, Generic)

instance ToJSON ChainHead where
  toJSON (ChainHead hash' level' proto' predecessor' timestamp' validationPass' operationsHash' fitness' context' protocolData') =
    object
      [ "hash"            .= hash'
      , "level"           .= level'
      , "proto"           .= proto'
      , "predecessor"     .= predecessor'
      , "timestamp"       .= timestamp'
      , "validation_pass" .= validationPass'
      , "operations_hash" .= operationsHash'
      , "fitness"         .= fitness'
      , "context"         .= context'
      , "protocol_data"   .= protocolData'
      ]

instance FromJSON ChainHead where
  parseJSON = withObject "ChainHead" $ \o ->
    ChainHead <$> o .: "hash"
              <*> o .: "level"
              <*> o .: "proto"
              <*> o .: "predecessor"
              <*> o .: "timestamp"
              <*> o .: "validation_pass"
              <*> o .: "operations_hash"
              <*> o .: "fitness"
              <*> o .: "context"
              <*> o .: "protocol_data"
