{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings     #-}

module Tezos.Client.Types.ChainValidator where

import Data.Aeson
  (FromJSON(parseJSON), ToJSON(toJSON), (.=), (.:), (.:?), object, withObject,
   withText)
import Data.Maybe (catMaybes)
import Data.Text (Text)
import GHC.Generics (Generic)
import Tezos.Client.Types.BlockValidator
import Tezos.Client.Types.Core
import Tezos.Client.Extension.Aeson ((.=?))

-- [ { "chain_id": $Chain_id,
--     "status":
--       { "phase": "launching",
--         "since": $timestamp }
--       || { "phase": "running",
--            "since": $timestamp }
--       || { "phase": "closing",
--            "birth": $timestamp,
--            "since": $timestamp }
--       || { "phase": "closed",
--            "birth": $timestamp,
--            "since": $timestamp }
--       || { "phase": "crashed",
--            "birth": $timestamp,
--            "since": $timestamp,
--            "errors": $error } } ... ]

data ChainValidatorMini =
  ChainValidatorMini
    { chainId :: ChainId
    , status  :: BlockValidatorStatus
    } deriving (Eq, Show, Generic)

instance ToJSON ChainValidatorMini where
  toJSON (ChainValidatorMini chainId' status') =
    object
      [ "chain_id" .= chainId'
      , "status"  .= status'
      ]

instance FromJSON ChainValidatorMini where
  parseJSON = withObject "ChainValidatorMini" $ \o ->
    ChainValidatorMini <$> o .: "chain_id"
                       <*> o .: "status"

-- { "status":
--     { "phase": "launching",
--       "since": $timestamp }
--     || { "phase": "running",
--          "since": $timestamp }
--     || { "phase": "closing",
--          "birth": $timestamp,
--          "since": $timestamp }
--     || { "phase": "closed",
--          "birth": $timestamp,
--          "since": $timestamp }
--     || { "phase": "crashed",
--          "birth": $timestamp,
--          "since": $timestamp,
--          "errors": $error },
--   "pending_requests":
--     [ { "pushed": $timestamp,
--         "request": $block_hash } ... ],
--   "backlog":
--     [ { "level":
--           "info" | "debug" | "error" | "fatal" | "warning" | "notice",
--         "events":
--           [ { "request": $block_hash,
--               "status":
--                 { "pushed": $timestamp,
--                   "treated": $timestamp,
--                   "completed": $timestamp },
--               "outcome": "branch" | "ignored" | "increment",
--               "fitness": $fitness }
--             || $error ... ] } ... ],
--   "current_request"?:
--     { "pushed": $timestamp,
--       "treated": $timestamp,
--       "request": $block_hash } }

data ChainValidator =
  ChainValidator
    { status          :: BlockValidatorStatus
    , pendingRequests :: [ChainValidatorPendingRequest]
    , backlog         :: [ChainValidatorBacklog]
    , currentRequest  :: Maybe CurrentRequest
    } deriving (Eq, Show, Generic)

instance ToJSON ChainValidator where
  toJSON (ChainValidator status' pendingRequests' backlog' currentRequest') =
    object $
      [ "status"           .= status' 
      , "pending_requests" .= pendingRequests'
      , "backlog"          .= backlog'
      ] ++ catMaybes ["current_request" .=? currentRequest']

instance FromJSON ChainValidator where
  parseJSON = withObject "ChainValidator" $ \o ->
    ChainValidator <$> o .:  "status"
                   <*> o .:  "pending_requests"
                   <*> o .:  "backlog"
                   <*> o .:? "current_request"

-- "pending_requests":
--   [ { "pushed": $timestamp,
--       "request": $block_hash } ... ]

data ChainValidatorPendingRequest =
  ChainValidatorPendingRequest
    { pushed  :: Timestamp
    , request :: BlockHash
    } deriving (Eq, Show, Generic)

instance ToJSON ChainValidatorPendingRequest where
  toJSON (ChainValidatorPendingRequest pushed' request') =
    object
      [ "pushed"  .= pushed'
      , "request" .= request'
      ]
  
instance FromJSON ChainValidatorPendingRequest where
  parseJSON = withObject "ChainValidatorPendingRequest" $ \o ->
    ChainValidatorPendingRequest <$> o .: "pushed"
                                 <*> o .: "request"

--   "backlog":
--     [ { "level":
--           "info" | "debug" | "error" | "fatal" | "warning" | "notice",
--         "events":
--           [ { "request": $block_hash,
--               "status":
--                 { "pushed": $timestamp,
--                   "treated": $timestamp,
--                   "completed": $timestamp },
--               "outcome": "branch" | "ignored" | "increment",
--               "fitness": $fitness }
--             || $error ... ] } ... ],

data ChainValidatorBacklogEventStatus =
  ChainValidatorBacklogEventStatus
    { pushed    :: Timestamp
    , treated   :: Timestamp
    , completed :: Timestamp
    } deriving (Eq, Show, Generic)

instance ToJSON ChainValidatorBacklogEventStatus where
  toJSON (ChainValidatorBacklogEventStatus pushed' treated' completed') =
    object
      [ "pushed"    .= pushed'
      , "treated"   .= treated'
      , "completed" .= completed'
      ]

instance FromJSON ChainValidatorBacklogEventStatus where
  parseJSON = withObject "ChainValidatorBacklogEventStatus" $ \o ->
    ChainValidatorBacklogEventStatus <$> o .: "pushed"
                                     <*> o .: "treated"
                                     <*> o .: "completed"

-- "outcome": "branch" | "ignored" | "increment"

data ChainValidatorBacklogEventOutcome
  = CVBEOBranch
  | CVBEOIgnored
  | CVBEOIncrement
  deriving (Eq, Show, Generic)

instance ToJSON ChainValidatorBacklogEventOutcome where
  toJSON CVBEOBranch    = "branch"
  toJSON CVBEOIgnored   = "ignored"
  toJSON CVBEOIncrement = "increment"

instance FromJSON ChainValidatorBacklogEventOutcome where
  parseJSON = withText "ChainValidatorBacklogEventOutcome" $ \s ->
    case s of
      "branch"    -> pure CVBEOBranch
      "ignored"   -> pure CVBEOIgnored
      "increment" -> pure CVBEOIncrement
      _ -> fail $ "ChainValidatorBacklogEventOutcome " <> show s

-- "events":
--   [ { "request": $block_hash,
--       "status":
--         { "pushed": $timestamp,
--           "treated": $timestamp,
--           "completed": $timestamp },
--       "outcome": "branch" | "ignored" | "increment",
--       "fitness": $fitness }
--     || $error ... ] } ... ]


data ChainValidatorBacklogEvent =
  ChainValidatorBacklogEvent
    { request :: Text
    , status  :: ChainValidatorBacklogEventStatus
    , outcome :: ChainValidatorBacklogEventOutcome
    , fitness :: [Text] -- Fitness
    } deriving (Eq, Show, Generic)

instance ToJSON ChainValidatorBacklogEvent where
  toJSON (ChainValidatorBacklogEvent request' status' outcome' fitness') =
    object
      [ "request" .= request'
      , "status"  .= status'
      , "outcome" .= outcome'
      , "fitness" .= fitness'
      ]

instance FromJSON ChainValidatorBacklogEvent where
  parseJSON = withObject "ChainValidatorBacklogEvent" $ \o ->
    ChainValidatorBacklogEvent <$> o .: "request"
                               <*> o .: "status"
                               <*> o .: "outcome"
                               <*> o .: "fitness"

-- "level":
--   "info" | "debug" | "error" | "fatal" | "warning" | "notice"

data ChainValidatorBacklogLevel
  = CVBLInfo
  | CVBLDebug
  | CVBLError
  | CVBLFatal
  | CVBLWarning
  | CVBLNotice
  deriving (Eq, Show, Generic)

instance ToJSON ChainValidatorBacklogLevel where
  toJSON CVBLInfo    = "info"
  toJSON CVBLDebug   = "debug"
  toJSON CVBLError   = "error"
  toJSON CVBLFatal   = "fatal"
  toJSON CVBLWarning = "warning"
  toJSON CVBLNotice  = "notice"

instance FromJSON ChainValidatorBacklogLevel where
  parseJSON = withText "ChainValidatorBacklogLevel" $ \s ->
    case s of
      "info"    -> pure CVBLInfo
      "debug"   -> pure CVBLDebug
      "error"   -> pure CVBLError
      "fatal"   -> pure CVBLFatal
      "warning" -> pure CVBLWarning
      "notice"  -> pure CVBLNotice
      _         -> fail "ChainValidatorBacklogLevel"

-- "backlog":
--   [ { "level":
--         "info" | "debug" | "error" | "fatal" | "warning" | "notice",
--       "events":
--         [ { "request": $block_hash,
--             "status":
--               { "pushed": $timestamp,
--                 "treated": $timestamp,
--                 "completed": $timestamp },
--             "outcome": "branch" | "ignored" | "increment",
--             "fitness": $fitness }
--           || $error ... ] } ... ]

data ChainValidatorBacklog =
  ChainValidatorBacklog
    { level  :: ChainValidatorBacklogLevel
    , events :: [ChainValidatorBacklogEvent]
    } deriving (Eq, Show, Generic)

instance ToJSON ChainValidatorBacklog where
  toJSON (ChainValidatorBacklog level' events') =
    object
      [ "level"  .= level'
      , "events" .= events'
      ]

instance FromJSON ChainValidatorBacklog where
  parseJSON = withObject "ChainValidatorBacklog" $ \o ->
    ChainValidatorBacklog <$> o .: "level"
                          <*> o .: "events"
