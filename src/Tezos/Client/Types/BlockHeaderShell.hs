{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings     #-}

module Tezos.Client.Types.BlockHeaderShell where

import Data.Aeson
  (FromJSON(parseJSON), ToJSON(toJSON), (.=), (.:), object, withObject)
import Data.Int (Int64)
import Data.Text (Text)
import Data.Word (Word8)
import GHC.Generics (Generic)
import Tezos.Client.Types.Core

-- $block_header.shell:
--   { "level": integer ∈ [-2^31-2, 2^31+2],
--     "proto": integer ∈ [0, 255],
--     "predecessor": $block_hash,
--     "timestamp": $timestamp,
--     "validation_pass": integer ∈ [0, 255],
--     "operations_hash": $Operation_list_list_hash,
--     "fitness": $fitness,
--     "context": $Context_hash }

data BlockHeaderShell =
  BlockHeaderShell
    { level          :: Int64
    , proto          :: Word8
    , predecessor    :: BlockHash
    , timestamp      :: Timestamp
    , validationPass :: Word8
    , operationsHash :: [[Text]]
    , fitness        :: Fitness
    , context        :: ContextHash
    } deriving (Eq, Show, Generic)

instance ToJSON BlockHeaderShell where
  toJSON (BlockHeaderShell level' proto' predecessor' timestamp' validationPass'
          operationsHash' fitness' context') =
    object
      [ "level"           .= level'
      , "proto"           .= proto'
      , "predecessor"     .= predecessor'
      , "timestamp"       .= timestamp'
      , "validation_pass" .= validationPass'
      , "operations_hash" .= operationsHash'
      , "fitness"         .= fitness'
      , "context"         .= context'
      ]

instance FromJSON BlockHeaderShell where
  parseJSON = withObject "BlockHeaderShell" $ \o ->
    BlockHeaderShell <$> o .: "level"
                     <*> o .: "proto"
                     <*> o .: "predecessor"
                     <*> o .: "timestamp"
                     <*> o .: "validation_pass"
                     <*> o .: "operations_hash"
                     <*> o .: "fitness"
                     <*> o .: "context"
