{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings     #-}

module Tezos.Client.Types.BakingRights where

import Data.Aeson
import Data.Int (Int64)
import Data.Maybe (catMaybes)
import Data.Word (Word16)
import GHC.Generics (Generic)
import Tezos.Client.Types.Core
import Tezos.Client.Types.Crypto
import Tezos.Client.Extension.Aeson ((.=?))

-- [ { "level": integer ∈ [-2^31-2, 2^31+2],
--      "delegate": $Signature.Public_key_hash,
--      "priority": integer ∈ [0, 2^16-1],
--      "estimated_time"?: $timestamp } ... ]

data BakingRights =
  BakingRights
    { level         :: Int64
    , delegate      :: PublicKeyHash
    , priority      :: Word16
    , estimatedTime :: Maybe Timestamp
    } deriving (Eq, Show, Generic)

instance ToJSON BakingRights where
  toJSON (BakingRights level' delegate' priority' estimatedTime') =
    object $
      [ "level"          .= level'
      , "delegate"       .= delegate'
      , "priority"       .= priority'
      ] ++ catMaybes ["estimated_time" .=? estimatedTime']

instance FromJSON BakingRights where
  parseJSON = withObject "BakingRights" $ \o ->
    BakingRights <$> o .:  "level"
                 <*> o .:  "delegate"
                 <*> o .:  "priority"
                 <*> o .:? "estimated_time"
