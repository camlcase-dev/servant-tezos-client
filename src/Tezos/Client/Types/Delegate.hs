{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings     #-}

module Tezos.Client.Types.Delegate where

import Data.Aeson
  (ToJSON(toJSON), FromJSON(parseJSON), (.=), (.:), withObject, object)
import Data.Int (Int64)
import GHC.Generics (Generic)
import Tezos.Client.Types.Core

--  { "balance": $mutez,
--    "frozen_balance": $mutez,
--    "frozen_balance_by_cycle":
--      [ { "cycle": integer ∈ [-2^31-2, 2^31+2],
--          "deposit": $mutez,
--          "fees": $mutez,
--          "rewards": $mutez } ... ],
--    "staking_balance": $mutez,
--    "delegated_contracts": [ $Contract_hash ... ],
--    "delegated_balance": $mutez,
--    "deactivated": boolean,
--    "grace_period": integer ∈ [-2^31-2, 2^31+2] }

data DelegateInfo =
  DelegateInfo
    { balance              :: Mutez
    , frozenBalance        :: Mutez
    , frozenBalanceByCycle :: [Balance]
    , stakingBalance       :: Mutez
    , delegatedContracts   :: [ContractHash]
    , delegatedBalance     :: Mutez
    , deactivated          :: Bool
    , gracePeriod          :: Int64
    } deriving (Eq, Show, Generic)

instance ToJSON DelegateInfo where
  toJSON (DelegateInfo balance' frozenBalance' frozenBalanceByCycle'
          stakingBalance' delegatedContracts' delegatedBalance' deactivated'
          gracePeriod') =
    object
      [ "balance"                 .= balance'
      , "frozen_balance"          .= frozenBalance'
      , "frozen_balance_by_cycle" .= frozenBalanceByCycle'
      , "staking_balance"         .= stakingBalance'
      , "delegated_contracts"     .= delegatedContracts'
      , "delegated_balance"       .= delegatedBalance'
      , "deactivated"             .= deactivated'
      , "grace_period"            .= gracePeriod'
      ]

instance FromJSON DelegateInfo where
  parseJSON = withObject "DelegateInfo" $ \o ->
    DelegateInfo <$> o .: "balance"
                 <*> o .: "frozen_balance"
                 <*> o .: "frozen_balance_by_cycle"
                 <*> o .: "staking_balance"
                 <*> o .: "delegated_contracts"
                 <*> o .: "delegated_balance"
                 <*> o .: "deactivated"
                 <*> o .: "grace_period"

-- { "cycle": integer ∈ [-2^31-2, 2^31+2],
--   "deposit": $mutez,
--   "fees": $mutez,
--   "rewards": $mutez }

data Balance =
  Balance
    { cycle    :: Int64
    , deposit  :: Mutez
    , fees     :: Mutez
    , rewards  :: Mutez
    } deriving (Eq, Show, Generic)

instance ToJSON Balance where
  toJSON (Balance cycle' deposit' fees' rewards') =
    object
      [ "cycle"   .= cycle'
      , "deposit" .= deposit'
      , "fees"    .= fees'
      , "rewards" .= rewards'
      ]

instance FromJSON Balance where
  parseJSON = withObject "Balance" $ \o ->
    Balance <$> o .: "cycle"
            <*> o .: "deposit"
            <*> o .: "fees"
            <*> o .: "rewards"
