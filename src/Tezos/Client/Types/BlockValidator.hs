{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings     #-}

module Tezos.Client.Types.BlockValidator where

import Data.Aeson
  (FromJSON(parseJSON), ToJSON(toJSON), Value, (.=), (.:), (.:?), object,
   withObject)
import Data.Aeson.Types (Parser)
import Data.Maybe (catMaybes)
import Data.Text (Text)
import GHC.Generics (Generic)
import Tezos.Client.Types.Core
import Tezos.Client.Types.Crypto
import qualified Data.HashMap.Lazy as HM
import Tezos.Client.Extension.Aeson ((.=?))

-- { "status":
--     { "phase": "launching",
--       "since": $timestamp }
--     || { "phase": "running",
--          "since": $timestamp }
--     || { "phase": "closing",
--          "birth": $timestamp,
--          "since": $timestamp }
--     || { "phase": "closed",
--          "birth": $timestamp,
--          "since": $timestamp }
--     || { "phase": "crashed",
--          "birth": $timestamp,
--          "since": $timestamp,
--          "errors": $error },
--   "pending_requests":
--     [ { "pushed": $timestamp,
--         "request":
--           { "block": $block_hash,
--             "chain_id": $Chain_id,
--            "peer"?: $Crypto_box.Public_key_hash } } ... ],
--   "backlog":
--     [ { "level":
--           "info" | "debug" | "error" | "fatal" | "warning" | "notice",
--         "events":
--           [ { "message": $unistring }
--             || { "successful_validation":
--                    { "block": $block_hash,
--                      "chain_id": $Chain_id,
--                      "peer"?: $Crypto_box.Public_key_hash },
--                  "status":
--                    { "pushed": $timestamp,
--                      "treated": $timestamp,
--                      "completed": $timestamp } }
--             || { "failed_validation":
--                    { "block": $block_hash,
--                      "chain_id": $Chain_id,
--                      "peer"?: $Crypto_box.Public_key_hash },
--                  "status":
--                    { "pushed": $timestamp,
--                      "treated": $timestamp,
--                      "completed": $timestamp },
--                  "errors"?: $error } ... ] } ... ],
--   "current_request"?:
--     { "pushed": $timestamp,
--       "treated": $timestamp,
--       "request":
--         { "block": $block_hash,
--           "chain_id": $Chain_id,
--           "peer"?: $Crypto_box.Public_key_hash } } }

data BlockValidator =
  BlockValidator
    { status          :: BlockValidatorStatus
    , pendingRequests :: [BlockValidatorPendingRequest]
    , backlog         :: [Backlog]
    , currentRequests :: Maybe CurrentRequest
    } deriving (Eq, Show, Generic)

instance ToJSON BlockValidator where
  toJSON (BlockValidator status' pendingRequests' backlog' currentRequests') =
    object $
      [ "status"           .= status'
      , "pending_requests" .= pendingRequests'
      , "backlog"          .= backlog'
      ] ++ catMaybes ["current_requests" .=? currentRequests']

instance FromJSON BlockValidator where
  parseJSON = withObject "BlockValidator" $ \o ->
    BlockValidator <$> o .:  "status"
                   <*> o .:  "pending_requests"
                   <*> o .:  "backlog"
                   <*> o .:? "current_requests"

-- { "phase": "launching",
--   "since": $timestamp }
-- || { "phase": "running",
--      "since": $timestamp }
-- || { "phase": "closing",
--      "birth": $timestamp,
--      "since": $timestamp }
-- || { "phase": "closed",
--      "birth": $timestamp,
--      "since": $timestamp }
-- || { "phase": "crashed",
--      "birth": $timestamp,
--      "since": $timestamp,
--      "errors": $error }

data BlockValidatorStatus
  = BVSLaunching
    { since :: Timestamp }
  | BVSRunning
    { since :: Timestamp }
  | BVSClosing
    { birth :: Timestamp
    , since :: Timestamp
    }
  | BVSClosed
    { birth :: Timestamp
    , since :: Timestamp
    }
  | BVSCrashed
    { birth :: Timestamp
    , since :: Timestamp
    , error :: Error
    }
  deriving (Eq, Show, Generic)

instance ToJSON BlockValidatorStatus where
  toJSON (BVSLaunching since') =
    object
      [ "phase" .= ("launching" :: Text)
      , "since" .= since'
      ]
  toJSON (BVSRunning since') =
    object
      [ "phase" .= ("running" :: Text)
      , "since" .= since'
      ]
  toJSON (BVSClosing birth' since') =
    object
      [ "phase" .= ("closing" :: Text)
      , "birth" .= birth'
      , "since" .= since'
      ]
  toJSON (BVSClosed birth' since') =
    object
      [ "phase" .= ("closed" :: Text)
      , "birth" .= birth'
      , "since" .= since'
      ]
  toJSON (BVSCrashed birth' since' error') =
    object
      [ "phase" .= ("crashed" :: Text)
      , "birth" .= birth'
      , "since" .= since'
      , "error" .= error'
      ]

instance FromJSON BlockValidatorStatus where
  parseJSON = withObject "BlockValidatorStatus" $ \o -> do
    let match s =
          case s of
            "launching" -> BVSLaunching <$> o .: "since"
            "running"   -> BVSRunning   <$> o .: "since"
            "closing"   -> BVSClosing   <$> o .: "birth" <*> o .: "since"
            "closed"    -> BVSClosed    <$> o .: "birth" <*> o .: "since"
            "crashed"   -> BVSCrashed   <$> o .: "birth" <*> o .: "since" <*> o .: "error"
            _ -> fail $ "expected BlockValidatorStatus, encountered " ++ (show s)
    phase <- (o .: "phase") :: Parser Text
    match phase

-- [ { "level":
--       "info" | "debug" | "error" | "fatal" | "warning" | "notice",
--     "events":
--       [ { "message": $unistring }
--         || { "successful_validation":
--                { "block": $block_hash,
--                  "chain_id": $Chain_id,
--                  "peer"?: $Crypto_box.Public_key_hash },
--              "status":
--                { "pushed": $timestamp,
--                  "treated": $timestamp,
--                  "completed": $timestamp } }
--         || { "failed_validation":
--                { "block": $block_hash,
--                  "chain_id": $Chain_id,
--                  "peer"?: $Crypto_box.Public_key_hash },
--              "status":
--                { "pushed": $timestamp,
--                  "treated": $timestamp,
--                  "completed": $timestamp },
--              "errors"?: $error } ... ] } ... ],

data Backlog =
  Backlog
    { level  :: Text
    , events :: [BacklogEvent]
    } deriving (Eq, Show, Generic)

instance ToJSON Backlog where
  toJSON (Backlog level' events') =
    object
      [ "level"  .= level'
      , "events" .= events'
      ]

instance FromJSON Backlog where
  parseJSON = withObject "Backlog" $ \o ->
    Backlog <$> o .: "level"
            <*> o .: "events"

-- [ { "message": $unistring }
--   || { "successful_validation":
--          { "block": $block_hash,
--            "chain_id": $Chain_id,
--            "peer"?: $Crypto_box.Public_key_hash },
--        "status":
--          { "pushed": $timestamp,
--            "treated": $timestamp,
--            "completed": $timestamp } }
--   || { "failed_validation":
--          { "block": $block_hash,
--            "chain_id": $Chain_id,
--            "peer"?: $Crypto_box.Public_key_hash },
--        "status":
--          { "pushed": $timestamp,
--            "treated": $timestamp,
--            "completed": $timestamp },
--        "errors"?: $error } ... ]


data BacklogEvent
  = BEMessage
    { message :: Text }
  | BESuccessfulValidation
    { blockValidatorRequest :: BlockValidatorRequest
    , status :: BacklogStatus
    }
  | BEFailedValidation
    { blockValidatorRequest :: BlockValidatorRequest
    , status :: BacklogStatus
    , errors :: Maybe Value
    }
  deriving (Eq, Show, Generic)

instance ToJSON BacklogEvent where
  toJSON (BEMessage message') =
    object [ "message" .= message' ]

  toJSON (BESuccessfulValidation blockValidatorRequest' status') =
    object ["successful_validation" .= blockValidatorRequest', "status" .= status']

  toJSON (BEFailedValidation blockValidatorRequest' status' errors') =
    object $
      [ "failed_validation" .= blockValidatorRequest'
      , "status" .= status'
      ] ++ catMaybes ["errors" .=? errors']
 
instance FromJSON BacklogEvent where
  parseJSON = withObject "BlockValidatorStatus" $ \o -> do
    let hasMessage             = HM.member "message" o
        hasSuccesfulValidation = HM.member "successful_validation" o
        hasFailedValidation    = HM.member "failed_validation" o
        parseBacklogEvent
          | hasMessage             = BEMessage <$> o .: "message"
          | hasSuccesfulValidation = BESuccessfulValidation <$> o .: "successful_validation" <*> o .: "status"
          | hasFailedValidation    = BEFailedValidation <$> o .: "failed_validation" <*> o .: "status" <*> o .:? "errors"
          | otherwise = fail $ "expected BlockValidatorStatus, encountered " ++ (show o)
    parseBacklogEvent


-- "pending_requests":
--   [ { "pushed": $timestamp,
--       "request":
--         { "block": $block_hash,
--           "chain_id": $Chain_id,
--           "peer"?: $Crypto_box.Public_key_hash } } ... ],

data BlockValidatorPendingRequest =
  BlockValidatorPendingRequest
    { pushed  :: Timestamp
    , request :: BlockValidatorRequest
    } deriving (Eq, Show, Generic)

instance ToJSON BlockValidatorPendingRequest where
  toJSON (BlockValidatorPendingRequest pushed' request') =
    object
      [ "pushed"  .= pushed'
      , "request" .= request'
      ]

instance FromJSON BlockValidatorPendingRequest where
  parseJSON = withObject "BlockValidatorPendingRequest" $ \o ->
    BlockValidatorPendingRequest <$> o .: "pushed"
                                 <*> o .: "request"

-- "current_request"?:
--   { "pushed": $timestamp,
--     "treated": $timestamp,
--     "request":
--       { "block": $block_hash,
--         "chain_id": $Chain_id,
--         "peer"?: $Crypto_box.Public_key_hash } } }

data CurrentRequest =
  CurrentRequest
    { pushed  :: Timestamp
    , treated :: Timestamp
    , request :: BlockValidatorRequest
    } deriving (Eq, Show, Generic)

instance ToJSON CurrentRequest where
  toJSON (CurrentRequest pushed' treated' request') =
    object
      [ "pushed"  .= pushed'
      , "treated" .= treated'
      , "request" .= request'
      ]

instance FromJSON CurrentRequest where
  parseJSON = withObject "CurrentRequest" $ \o ->
    CurrentRequest <$> o .: "pushed"
                   <*> o .: "treated"
                   <*> o .: "request"

-- "status":
--   { "pushed": $timestamp,
--     "treated": $timestamp,
--     "completed": $timestamp }

data BacklogStatus =
  BacklogStatus
    { pushed    :: Timestamp
    , treated   :: Timestamp
    , completed :: Timestamp
    } deriving (Eq, Show, Generic)

instance ToJSON BacklogStatus where
  toJSON (BacklogStatus pushed' treated' completed') =
    object
      [ "pushed"    .= pushed'
      , "treated"   .= treated'
      , "completed" .= completed'
      ]

instance FromJSON BacklogStatus where
  parseJSON = withObject "BacklogStatus" $ \o ->
    BacklogStatus <$> o .: "pushed"
                  <*> o .: "treated"
                  <*> o .: "completed"

-- "request":
--   { "block": $block_hash,
--     "chain_id": $Chain_id,
--     "peer"?: $Crypto_box.Public_key_hash }

data BlockValidatorRequest =
  BlockValidatorRequest
    { block   :: BlockHash
    , chainId :: ChainId
    , peer    :: Maybe PublicKeyHash
    } deriving (Eq, Show, Generic)

instance ToJSON BlockValidatorRequest where
  toJSON (BlockValidatorRequest block' chainId' peer') =
    object $
      [ "block"    .= block'
      , "chain_id" .= chainId'
      ] ++ catMaybes ["peer" .=? peer']
  
instance FromJSON BlockValidatorRequest where
  parseJSON = withObject "BlockValidatorRequest" $ \o ->
    BlockValidatorRequest <$> o .:  "block"
                          <*> o .:  "chain_id"
                          <*> o .:? "peer"
