{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings     #-}

module Tezos.Client.Types.Block where

import Data.Aeson
  (FromJSON(parseJSON), ToJSON(toJSON), Value(Array, Object), (.=),
   (.:), (.:?), object, withObject)
import Data.Int (Int64)
import Data.Maybe (catMaybes)
import Data.Text (Text)
import qualified Data.Vector as V
import Data.Word (Word8, Word16)
import GHC.Generics (Generic)
import Tezos.Client.Types.Core
import Tezos.Client.Types.Contract
import Tezos.Client.Types.Endorsement
import Tezos.Client.Types.Crypto
import Tezos.Client.Types.Vote
import Tezos.Client.Types.Micheline
import qualified Data.HashMap.Lazy as HM
import Tezos.Client.Extension.Aeson ((.=?), combineValues)

-- { "protocol": "ProtoALphaALphaALphaALphaALphaALphaALphaALphaDdp3zK",
--   "chain_id": $Chain_id,
--   "hash": $block_hash,
--   "header": $raw_block_header,
--   "metadata"?: $block_header_metadata,
--   "operations": [ [ $operation ... ] ... ] }

data Block =
  Block
    { protocol   :: ProtocolHash
    , chainId    :: ChainId
    , hash       :: BlockHash
    , header     :: RawBlockHeader
    , metadata   :: BlockHeaderMetadata
    , operations :: [[Operation]]
    } deriving (Eq, Show, Generic)

instance ToJSON Block where
  toJSON (Block protocol' chainId' hash' header' metadata' operations') =
    object
      [ "protocol"   .= protocol'
      , "chain_id"   .= chainId'
      , "hash"       .= hash'
      , "header"     .= header'
      , "metadata"   .= metadata'
      , "operations" .= operations'
      ]

instance FromJSON Block where
  parseJSON = withObject "Block" $ \o ->
    Block <$> o .: "protocol"
          <*> o .: "chain_id"
          <*> o .: "hash"
          <*> o .: "header"
          <*> o .: "metadata"
          <*> o .: "operations"

-- $block_header.alpha.full_header:
--   { "level": integer ∈ [-2^31-2, 2^31+2],
--     "proto": integer ∈ [0, 255],
--     "predecessor": $block_hash,
--     "timestamp": $timestamp,
--     "validation_pass": integer ∈ [0, 255],
--     "operations_hash": $Operation_list_list_hash,
--     "fitness": $fitness,
--     "context": $Context_hash,
--     "priority": integer ∈ [0, 2^16-1],
--     "proof_of_work_nonce": /^[a-zA-Z0-9]+$/,
--     "seed_nonce_hash"?: $cycle_nonce,
--     "signature": $Signature }

data FullHeader =
  FullHeader
    { level            :: Int64
    , proto            :: Word8
    , predecessor      :: BlockHash
    , timestamp        :: Timestamp
    , validationPass   :: Word8
    , operationsHash   :: Unistring
    , fitness          :: Fitness
    , context          :: ContextHash
    , priority         :: Word16
    , proofOfWorkNonce :: Text
    , seedNonceHash    :: Maybe CycleNonce
    , signature        :: Signature
    } deriving (Eq, Show, Generic)

instance ToJSON FullHeader where
  toJSON (FullHeader level' proto' predecessor' timestamp' validationPass'
          operationsHash' fitness' context' priority' proofOfWorkNonce'
          seedNonceHash' signature') =
    object $
      [ "level"               .= level'
      , "proto"               .= proto'
      , "predecessor"         .= predecessor'
      , "timestamp"           .= timestamp'
      , "validation_pass"     .= validationPass'
      , "operations_hash"     .= operationsHash'
      , "fitness"             .= fitness'
      , "context"             .= context'
      , "priority"            .= priority'
      , "proof_of_work_nonce" .= proofOfWorkNonce'
      , "signature"           .= signature'
      ] ++ catMaybes ["seed_nonce_hash" .=? seedNonceHash']

instance FromJSON FullHeader where
  parseJSON = withObject "FullHeader" $ \o ->
    FullHeader <$> o .:  "level"
               <*> o .:  "proto"
               <*> o .:  "predecessor"
               <*> o .:  "timestamp"
               <*> o .:  "validation_pass"
               <*> o .:  "operations_hash"
               <*> o .:  "fitness"
               <*> o .:  "context"
               <*> o .:  "priority"
               <*> o .:  "proof_of_work_nonce"
               <*> o .:? "seed_nonce_hash"
               <*> o .:  "signature"

-- $block_header_metadata:
-- { "protocol": "ProtoALphaALphaALphaALphaALphaALphaALphaALphaDdp3zK",
--   "next_protocol": "ProtoALphaALphaALphaALphaALphaALphaALphaALphaDdp3zK",
--   "test_chain_status": $test_chain_status,
--   "max_operations_ttl": integer ∈ [-2^30-2, 2^30+2],
--   "max_operation_data_length": integer ∈ [-2^30-2, 2^30+2],
--   "max_block_header_length": integer ∈ [-2^30-2, 2^30+2],
--   "max_operation_list_length":
--     [ { "max_size": integer ∈ [-2^30-2, 2^30+2],
--         "max_op"?: integer ∈ [-2^30-2, 2^30+2] } ... ],
--   "baker": $Signature.Public_key_hash,
--   "level":
--     { "level":
--         integer ∈ [-2^31-2, 2^31+2]
--         /* The level of the block relative to genesis. This is also the
--            Shell's notion of level */,
--       "level_position":
--         integer ∈ [-2^31-2, 2^31+2]
--         /* The level of the block relative to the block that starts
--            protocol alpha. This is specific to the protocol alpha. Other
--            protocols might or might not include a similar notion. */,
--       "cycle":
--         integer ∈ [-2^31-2, 2^31+2]
--         /* The current cycle's number. Note that cycles are a
--            protocol-specific notion. As a result, the cycle number starts
--            at 0 with the first block of protocol alpha. */,
--       "cycle_position":
--         integer ∈ [-2^31-2, 2^31+2]
--         /* The current level of the block relative to the first block of
--            the current cycle. */,
--       "voting_period":
--         integer ∈ [-2^31-2, 2^31+2]
--         /* The current voting period's index. Note that cycles are a
--            protocol-specific notion. As a result, the voting period index
--            starts at 0 with the first block of protocol alpha. */,
--       "voting_period_position":
--         integer ∈ [-2^31-2, 2^31+2]
--         /* The current level of the block relative to the first block of
--            the current voting period. */,
--       "expected_commitment":
--         boolean
--         /* Tells wether the baker of this block has to commit a seed
--            nonce hash. */ },
--    "voting_period_kind":
--      "proposal" || "testing_vote" || "testing" || "promotion_vote",
--    "nonce_hash": $cycle_nonce /* Some */ || null /* None */,
--    "consumed_gas": $positive_bignum,
--    "deactivated": [ $Signature.Public_key_hash ... ],
--    "balance_updates": $alpha.operation_metadata.alpha.balance_updates }

data BlockHeaderMetadata =
  BlockHeaderMetadata
    { protocol                 :: ProtocolHash
    , nextProtocol             :: ProtocolHash
    , testChainStatus          :: TestChainStatus
    , maxOperationsTtl         :: Int
    , maxOperationDataLength   :: Int
    , maxBlockHeaderLength     :: Int
    , maxOperationListLength   :: [MaxOperationLength]
    , baker                    :: PublicKeyHash
    , level                    :: BlockHeaderLevel
    , votingPeriodKind         :: VotingPeriodKind
    , nonceHash                :: Maybe CycleNonce
    , consumedGas              :: PositiveBignum
    , deactivated              :: [PublicKeyHash]
    , balanceUpdates           :: [BalanceUpdate]
    } deriving (Eq, Show, Generic)

instance ToJSON BlockHeaderMetadata where
  toJSON (BlockHeaderMetadata protocol' nextProtocol' testChainStatus'
          maxOperationsTtl' maxOperationsDataLength' maxBlockHeaderLength'
          maxOperationListLength' baker' level' votingPeriodKind' nonceHash'
          consumedGas' deactivated' balanceUpdates') =
    object
      [ "protocol"                   .= protocol'
      , "next_protocol"              .= nextProtocol'
      , "test_chain_status"          .= testChainStatus'
      , "max_operations_ttl"         .= maxOperationsTtl'
      , "max_operation_data_length" .= maxOperationsDataLength'
      , "max_block_header_length"    .= maxBlockHeaderLength'
      , "max_operation_list_length"  .= maxOperationListLength'
      , "baker"                      .= baker'
      , "level"                      .= level'
      , "voting_period_kind"         .= votingPeriodKind'
      , "nonce_hash"                 .= nonceHash'
      , "consumed_gas"               .= consumedGas'
      , "deactivated"                .= deactivated'
      , "balance_updates"            .= balanceUpdates'
      ]

instance FromJSON BlockHeaderMetadata where
  parseJSON = withObject "BlockHeaderMetadata" $ \o ->
    BlockHeaderMetadata <$> o .: "protocol"
                        <*> o .: "next_protocol"
                        <*> o .: "test_chain_status"
                        <*> o .: "max_operations_ttl"
                        <*> o .: "max_operation_data_length"
                        <*> o .: "max_block_header_length"
                        <*> o .: "max_operation_list_length"
                        <*> o .: "baker"
                        <*> o .: "level"
                        <*> o .: "voting_period_kind"
                        <*> o .: "nonce_hash"
                        <*> o .: "consumed_gas"
                        <*> o .: "deactivated"
                        <*> o .: "balance_updates"
                        
-- $test_chain_status:
--   { "status": "not_running" }
--   || { "status": "forking",
--        "protocol": $Protocol_hash,
--        "expiration": $timestamp }
--   || { "status": "running",
--        "chain_id": $Chain_id,
--        "genesis": $block_hash,
--        "protocol": $Protocol_hash,
--        "expiration": $timestamp }

data TestChainStatus
  = TCSNotRunning
  | TCSForking { protocol :: ProtocolHash, expiration :: Timestamp }
  | TCSRunning { protocol :: ProtocolHash, expiration :: Timestamp, chainId :: ChainId, genesis :: BlockHash }
  deriving (Eq, Show, Generic)

instance ToJSON TestChainStatus where
  toJSON TCSNotRunning = object ["status" .= ("not_running" :: Text)]
  toJSON (TCSForking protocol' expiration') =
    object
      [ "status"     .= ("forking" :: Text)
      , "protocol"   .= protocol'
      , "expiration" .= expiration'
      ]
  toJSON (TCSRunning protocol' expiration' chainId' genesis') =
    object
      [ "status"     .= ("running" :: Text)
      , "protocol"   .= protocol'
      , "expiration" .= expiration'
      , "chain_id"   .= chainId'
      , "genesis"    .= genesis'
      ]
  
instance FromJSON TestChainStatus where
  parseJSON = withObject "TestChainStatus" $ \o ->
    case HM.lookup "status" o of
      Just "not_running" -> pure TCSNotRunning
      Just "forking"     ->
        TCSForking <$> o .: "protocol"
                   <*> o .: "expiration"
      Just "running"     ->
        TCSRunning <$> o .: "protocol"
                   <*> o .: "expiration"
                   <*> o .: "chain_id"
                   <*> o .: "genesis"
      _ -> fail "TestChainStatus expected object with key 'status'."
    

-- "max_operation_list_length":
--   [ { "max_size": integer ∈ [-2^30-2, 2^30+2],
--       "max_op"?: integer ∈ [-2^30-2, 2^30+2] } ... ],

data MaxOperationLength =
  MaxOperationLength
    { maxSize :: Int64
    , maxOp   :: Maybe Int64
    } deriving (Eq, Show, Generic)

instance ToJSON MaxOperationLength where
  toJSON (MaxOperationLength maxSize' maxOp') =
    object $
      [ "max_size" .= maxSize'
      ] ++ catMaybes ["max_op" .=? maxOp']

instance FromJSON MaxOperationLength where
  parseJSON = withObject "MaxOperationLength" $ \o ->
    MaxOperationLength <$> o .:  "max_size"
                       <*> o .:? "max_op"

-- "level":
--   { "level": integer ∈ [-2^31-2, 2^31+2],
--     "level_position": integer ∈ [-2^31-2, 2^31+2],
--     "cycle": integer ∈ [-2^31-2, 2^31+2],
--     "cycle_position": integer ∈ [-2^31-2, 2^31+2],
--     "voting_period": integer ∈ [-2^31-2, 2^31+2],
--     "voting_period_position": integer ∈ [-2^31-2, 2^31+2],
--     "expected_commitment": boolean },

data BlockHeaderLevel =
  BlockHeaderLevel
    { level                :: Int64
    , levelPosition        :: Int64
    , cycle                :: Int64
    , cyclePosition        :: Int64
    , votingPeriod         :: Int64
    , votingPeriodPosition :: Int64
    , expectedCommitment   :: Bool
    } deriving (Eq, Show, Generic)

instance ToJSON BlockHeaderLevel where
  toJSON (BlockHeaderLevel level' levelPosition' cycle' cyclePosition'
          votingPeriod' votingPeriodPosition' expectedCommitment') =
    object
      [ "level"                  .= level'
      , "level_position"         .= levelPosition'
      , "cycle"                  .= cycle'
      , "cycle_position"         .= cyclePosition'
      , "voting_period"          .= votingPeriod'
      , "voting_period_position" .= votingPeriodPosition'
      , "expected_commitment"    .= expectedCommitment'
      ]

instance FromJSON BlockHeaderLevel where
  parseJSON = withObject "BlockHeaderLevel" $ \o ->
    BlockHeaderLevel <$> o .: "level"
                     <*> o .: "level_position"
                     <*> o .: "cycle"
                     <*> o .: "cycle_position"
                     <*> o .: "voting_period"
                     <*> o .: "voting_period_position"
                     <*> o .: "expected_commitment"

-- $alpha.operation_metadata.alpha.balance_updates:
--   [ { "kind": "contract",
--       "contract": $contract_id,
--       "change": $int64 }
--     || { "kind": "freezer",
--          "category": "rewards",
--          "delegate": $Signature.Public_key_hash,
--          "cycle": integer ∈ [-2^31-2, 2^31+2],
--          "change": $int64 }
--     || { "kind": "freezer",
--          "category": "fees",
--          "delegate": $Signature.Public_key_hash,
--          "cycle": integer ∈ [-2^31-2, 2^31+2],
--          "change": $int64 }
--     || { "kind": "freezer",
--          "category": "deposits",
--          "delegate": $Signature.Public_key_hash,
--          "cycle": integer ∈ [-2^31-2, 2^31+2],
--          "change": $int64 } ... ]

data BalanceUpdate
  = BalanceUpdateContract { contract :: ContractId, change :: Int64AsString }
  | FreezerRewards        { delegate :: PublicKeyHash, cycle :: Int64, change :: Int64AsString }
  | FreezerFees           { delegate :: PublicKeyHash, cycle :: Int64, change :: Int64AsString }
  | FreezerDeposits       { delegate :: PublicKeyHash, cycle :: Int64, change :: Int64AsString }
  deriving (Eq, Show, Generic)

instance ToJSON BalanceUpdate where
  toJSON (BalanceUpdateContract contract' change') =
    object
      [ "kind"     .= ("contract" :: Text)
      , "contract" .= contract'
      , "change"   .= change'
      ]
  toJSON (FreezerRewards delegate' cycle' change') =
    object
      [ "kind"     .= ("freezer" :: Text)
      , "category" .= ("rewards"  :: Text)
      , "delegate" .= delegate'
      , "cycle"    .= cycle'
      , "change"   .= change'
      ]
  toJSON (FreezerFees delegate' cycle' change') =
    object
      [ "kind"     .= ("freezer" :: Text)
      , "category" .= ("fees"     :: Text)
      , "delegate" .= delegate'
      , "cycle"    .= cycle'
      , "change"   .= change'
      ]
  toJSON (FreezerDeposits delegate' cycle' change') =
    object
      [ "kind"     .= ("freezer" :: Text)
      , "category" .= ("deposits" :: Text)
      , "delegate" .= delegate'
      , "cycle"    .= cycle'
      , "change"   .= change'
      ]

instance FromJSON BalanceUpdate where
  parseJSON = withObject "BalanceUpdate" $ \o ->
    case HM.lookup "kind" o of
      Just "contract" ->
        BalanceUpdateContract <$> o .: "contract"
                              <*> o .: "change"
      Just "freezer" ->
        case HM.lookup "category" o of
          Just "rewards"  ->
            FreezerRewards <$> o .: "delegate"
                           <*> o .: "cycle"
                           <*> o .: "change"
          Just "fees"     ->
            FreezerFees <$> o .: "delegate"
                        <*> o .: "cycle"
                        <*> o .: "change"
          Just "deposits" ->
            FreezerDeposits <$> o .: "delegate"
                            <*> o .: "cycle"
                            <*> o .: "change"
          _ -> fail "Expected key 'category' in with 'kind' of 'freezer'."
      _ -> fail "Expected key 'kind'."


newtype BalanceUpdates
  = BalanceUpdates { balanceUpdates :: [BalanceUpdate] }
  deriving (Eq, Show, Generic)

instance ToJSON BalanceUpdates where
  toJSON (BalanceUpdates balanceUpdates') =
    object
      [ "balance_updates" .= balanceUpdates'
      ]

instance FromJSON BalanceUpdates where
  parseJSON = withObject "BalanceUpdates" $ \o ->
    BalanceUpdates <$> o .: "balance_updates"
           
-- $operation:
--   { "protocol": "ProtoALphaALphaALphaALphaALphaALphaALphaALphaDdp3zK",
--     "chain_id": $Chain_id,
--     "hash": $Operation_hash,
--     "branch": $block_hash,
--     "contents":
--       [ $alpha.operation.alpha.operation_contents_and_result ... ],
--     "signature"?: $Signature }
--   || { "protocol": "ProtoALphaALphaALphaALphaALphaALphaALphaALphaDdp3zK",
--        "chain_id": $Chain_id,
--        "hash": $Operation_hash,
--        "branch": $block_hash,
--        "contents": [ $alpha.operation.alpha.contents ... ],
--        "signature"?: $Signature }
--   || { "protocol": "ProtoALphaALphaALphaALphaALphaALphaALphaALphaDdp3zK",
--        "chain_id": $Chain_id,
--        "hash": $Operation_hash,
--        "branch": $block_hash,
--        "contents": [ $alpha.operation.alpha.contents ... ],
--        "signature": $Signature }

data Operation
 = OperationAndResult
   { protocol          :: ProtocolHash
   , chainId           :: ChainId
   , hash              :: OperationHash
   , branch            :: BlockHash
   , contentsAndResult :: [OperationContentsAndResult]
   , signature         :: Maybe Signature
   }
 | Operation
   { protocol          :: ProtocolHash
   , chainId           :: ChainId
   , hash              :: OperationHash
   , branch            :: BlockHash
   , contents          :: [OperationContents]
   , signature         :: Maybe Signature
   } 
 deriving (Eq, Show, Generic)

instance ToJSON Operation where
  toJSON (OperationAndResult protocol' chainId' hash' branch' contentsAndResult' signature') =
    object $
      [ "protocol"  .= protocol'
      , "chain_id"  .= chainId'
      , "hash"      .= hash'
      , "branch"    .= branch'
      , "contents"  .= contentsAndResult'
      ] ++ catMaybes ["signature" .=? signature']
  toJSON (Operation protocol' chainId' hash' branch' contents' signature') =
    object $
      [ "protocol"  .= protocol'
      , "chain_id"  .= chainId'
      , "hash"      .= hash'
      , "branch"    .= branch'
      , "contents"  .= contents'
      ] ++ catMaybes ["signature" .=? signature']

instance FromJSON Operation where
  parseJSON = withObject "Operation" $ \o -> do
    case HM.lookup "contents" o of
      Just (Array contents') ->
        if V.length contents' == 0
          then
            Operation <$> o .:  "protocol"
                      <*> o .:  "chain_id"
                      <*> o .:  "hash"
                      <*> o .:  "branch"
                      <*> o .:  "contents"
                      <*> o .:? "signature"
          else
            case V.head contents' of
              Object content' ->
                if HM.member "metadata" content'
                  then
                    OperationAndResult <$> o .:  "protocol"
                                       <*> o .:  "chain_id"
                                       <*> o .:  "hash"
                                       <*> o .:  "branch"
                                       <*> o .:  "contents"
                                       <*> o .:? "signature"
            
                  else
                    Operation <$> o .:  "protocol"
                              <*> o .:  "chain_id"
                              <*> o .:  "hash"
                              <*> o .:  "branch"
                              <*> o .:  "contents"
                              <*> o .:? "signature"
              _ -> fail "parseJSON Operation expected a the objects of key 'contents' to be operations."
      _ -> fail "parseJSON Operation expected a key 'contents' of an array."
    
-- $alpha.operation.alpha.contents:
--   { /* Endorsement */
--     "kind": "endorsement",
--     "level": integer ∈ [-2^31-2, 2^31+2] }
--   || { /* Seed_nonce_revelation */
--        "kind": "seed_nonce_revelation",
--        "level": integer ∈ [-2^31-2, 2^31+2],
--        "nonce": /^[a-zA-Z0-9]+$/ }
--   || { /* Double_endorsement_evidence */
--        "kind": "double_endorsement_evidence",
--        "op1": $alpha.inlined.endorsement,
--        "op2": $alpha.inlined.endorsement }
--   || { /* Double_baking_evidence */
--        "kind": "double_baking_evidence",
--        "bh1": $alpha.block_header.alpha.full_header,
--        "bh2": $alpha.block_header.alpha.full_header }
--   || { /* Activate_account */
--        "kind": "activate_account",
--        "pkh": $Ed25519.Public_key_hash,
--        "secret": /^[a-zA-Z0-9]+$/ }
--   || { /* Proposals */
--        "kind": "proposals",
--        "source": $Signature.Public_key_hash,
--        "period": integer ∈ [-2^31-2, 2^31+2],
--        "proposals": [ $Protocol_hash ... ] }
--   || { /* Ballot */
--        "kind": "ballot",
--        "source": $Signature.Public_key_hash,
--        "period": integer ∈ [-2^31-2, 2^31+2],
--        "proposal": $Protocol_hash,
--        "ballot": "nay" | "yay" | "pass" }
--   || { /* Reveal */
--        "kind": "reveal",
--        "source": $Signature.Public_key_hash,
--        "fee": $alpha.mutez,
--        "counter": $positive_bignum,
--        "gas_limit": $positive_bignum,
--        "storage_limit": $positive_bignum,
--        "public_key": $Signature.Public_key }
--   || { /* Transaction */
--        "kind": "transaction",
--        "source": $Signature.Public_key_hash,
--        "fee": $alpha.mutez,
--        "counter": $positive_bignum,
--        "gas_limit": $positive_bignum,
--        "storage_limit": $positive_bignum,
--        "amount": $alpha.mutez,
--        "destination": $alpha.contract_id,
--        "parameters"?:
--          { "entrypoint": $alpha.entrypoint,
--            "value": $micheline.alpha.michelson_v1.expression } }
--   || { /* Origination */
--        "kind": "origination",
--        "source": $Signature.Public_key_hash,
--        "fee": $alpha.mutez,
--        "counter": $positive_bignum,
--        "gas_limit": $positive_bignum,
--        "storage_limit": $positive_bignum,
--        "balance": $alpha.mutez,
--        "delegate"?: $Signature.Public_key_hash,
--        "script": $alpha.scripted.contracts }
--   || { /* Delegation */
--        "kind": "delegation",
--        "source": $Signature.Public_key_hash,
--        "fee": $alpha.mutez,
--        "counter": $positive_bignum,
--        "gas_limit": $positive_bignum,
--        "storage_limit": $positive_bignum,
--        "delegate"?: $Signature.Public_key_hash }


data OperationSeedNonceRevelation =
  OperationSeedNonceRevelation
    { level :: Int64
    , nonce :: Text
    } deriving (Eq, Show, Generic)

instance ToJSON OperationSeedNonceRevelation where
  toJSON (OperationSeedNonceRevelation level' nonce') =
    object
      [ "kind"  .= ("seed_nonce_revelation" :: Text)
      , "level" .= level'
      , "nonce" .= nonce'
      ]
    
instance FromJSON OperationSeedNonceRevelation where
  parseJSON = withObject "OperationSeedNonceRevelation" $ \o ->
    OperationSeedNonceRevelation <$> o .: "level" <*> o .: "nonce"

data OperationDoubleEndorsementEvidence =
  OperationDoubleEndorsementEvidence
    { op1 :: Endorsement
    , op2 :: Endorsement
    } deriving (Eq, Show, Generic)

instance ToJSON OperationDoubleEndorsementEvidence where
  toJSON (OperationDoubleEndorsementEvidence op1' op2') =      
    object ["op1" .= op1', "op2" .= op2']    

instance FromJSON OperationDoubleEndorsementEvidence where
  parseJSON = withObject "OperationDoubleEndorsementEvidence" $ \o ->
    OperationDoubleEndorsementEvidence <$> o .: "op1" <*> o .: "op2"

data OperationDoubleBakingEvidence =
  OperationDoubleBakingEvidence
    { bh1 :: FullHeader
    , bh2 :: FullHeader
    } deriving (Eq, Show, Generic)

instance ToJSON OperationDoubleBakingEvidence where
  toJSON (OperationDoubleBakingEvidence bh1' bh2') =
    object ["bh1" .= bh1', "bh2" .= bh2']

instance FromJSON OperationDoubleBakingEvidence where
  parseJSON = withObject "OperationDoubleBakingEvidence" $ \o ->
    OperationDoubleBakingEvidence <$> o .: "bh1" <*> o .: "bh2"

data OperationActivateAccount
 = OperationActivateAccount
    { pkh    :: PublicKeyHash
    , secret :: Text
    } deriving (Eq, Show, Generic)

instance ToJSON OperationActivateAccount where
  toJSON (OperationActivateAccount pkh' secret') =
    object
      [ "pkh"    .= pkh'
      , "secret" .= secret'
      ]

instance FromJSON OperationActivateAccount where
  parseJSON = withObject "OperationActivateAccount" $ \o ->
    OperationActivateAccount <$> o .: "pkh" <*> o .: "secret"

data OperationProposals =
  OperationProposals
    { sourcePkhs :: PublicKeyHash
    , period     :: Int64
    , proposals  :: [ProtocolHash]
    } deriving (Eq, Show, Generic)

instance ToJSON OperationProposals where
  toJSON (OperationProposals sourcePkhs' period' proposals') =
    object
      [ "source"    .= sourcePkhs'
      , "period"    .= period'
      , "proposals" .= proposals'
      ]

instance FromJSON OperationProposals where
  parseJSON = withObject "OperationProposals" $ \o ->
    OperationProposals
    <$> o .: "source"
    <*> o .: "period"
    <*> o .: "proposals"

data OperationBallot =
  OperationBallot
    { source        :: ContractId
    , period        :: Int64
    , proposal      :: ProtocolHash
    , ballot        :: Ballot
    } deriving (Eq, Show, Generic)

instance ToJSON OperationBallot where
  toJSON (OperationBallot source' period' proposal' ballot') =
    object      
      [ "source"    .= source'
      , "period"    .= period'
      , "proposal"  .= proposal'
      , "ballot"    .= ballot'
      ]

instance FromJSON OperationBallot where
  parseJSON = withObject "OperationBallot" $ \o ->
    OperationBallot
    <$> o .: "source"
    <*> o .: "period"
    <*> o .: "proposal"
    <*> o .: "ballot"

data OperationReveal =
  OperationReveal
    { source        :: ContractId
    , fee           :: Mutez
    , counter       :: PositiveBignum
    , gasLimit      :: PositiveBignum
    , storageLimit  :: PositiveBignum
    , publicKey     :: PublicKey
    } deriving (Eq, Show, Generic)

instance ToJSON OperationReveal where
  toJSON (OperationReveal source' fee' counter' gasLimit'
          storageLimit' publicKey') =
    object
      [ "source"        .= source'
      , "fee"           .= fee'
      , "counter"       .= counter'
      , "gas_limit"     .= gasLimit'
      , "storage_limit" .= storageLimit'
      , "public_key"    .= publicKey'
      ]
    
instance FromJSON OperationReveal where
  parseJSON = withObject "OperationReveal" $ \o ->
    OperationReveal
    <$> o .: "source"
    <*> o .: "fee"
    <*> o .: "counter"
    <*> o .: "gas_limit"
    <*> o .: "storage_limit"
    <*> o .: "public_key"    

data OperationTransaction =
  OperationTransaction
    { source              :: ContractId
    , fee                 :: Mutez
    , counter             :: PositiveBignum
    , gasLimit            :: PositiveBignum
    , storageLimit        :: PositiveBignum
    , amount              :: Mutez
    , destination         :: ContractId
    , parameters          :: Maybe Parameters
    } deriving (Eq, Show, Generic)

instance ToJSON OperationTransaction where
  toJSON (OperationTransaction source' fee' counter' gasLimit'
          storageLimit' amount' destination' parameters') =
    object $
      [ "source"        .= source'
      , "fee"           .= fee'
      , "counter"       .= counter'
      , "gas_limit"     .= gasLimit'
      , "storage_limit" .= storageLimit'
      , "amount"        .= amount'
      , "destination"   .= destination'
      ] ++ catMaybes ["parameters" .=? parameters']

instance FromJSON OperationTransaction where
  parseJSON = withObject "OperationTransaction" $ \o ->
    OperationTransaction <$> o .:  "source"
                         <*> o .:  "fee"
                         <*> o .:  "counter"
                         <*> o .:  "gas_limit"
                         <*> o .:  "storage_limit"
                         <*> o .:  "amount"
                         <*> o .:  "destination"
                         <*> o .:? "parameters"

data OperationOrigination =
  OperationOrigination
    { source        :: ContractId
    , fee           :: Mutez
    , counter       :: PositiveBignum
    , gasLimit      :: PositiveBignum
    , storageLimit  :: PositiveBignum
    , balance       :: Mutez
    , delegate      :: Maybe PublicKeyHash
    , script        :: Contract
    } deriving (Eq, Show, Generic)

instance ToJSON OperationOrigination where
  toJSON (OperationOrigination source' fee' counter' gasLimit'
          storageLimit' balance' delegate' script') =
    object $
      [ "source"         .= source'
      , "fee"            .= fee'
      , "counter"        .= counter'
      , "gas_limit"      .= gasLimit'
      , "storage_limit"  .= storageLimit'
      , "balance"        .= balance'
      , "script"         .= script'      
      ] ++ catMaybes
      [ "delegate"      .=? delegate'
      ]

instance FromJSON OperationOrigination where
  parseJSON = withObject "OperationOrigination" $ \o -> OperationOrigination
    <$> o .:  "source"
    <*> o .:  "fee"
    <*> o .:  "counter"
    <*> o .:  "gas_limit"
    <*> o .:  "storage_limit"
    <*> o .:  "balance"
    <*> o .:? "delegate"
    <*> o .:  "script"

data OperationDelegation =
  OperationDelegation
    { source       :: ContractId
    , fee          :: Mutez
    , counter      :: PositiveBignum
    , gasLimit     :: PositiveBignum
    , storageLimit :: PositiveBignum
    , delegate     :: Maybe PublicKeyHash
    } deriving (Eq, Show, Generic)

instance ToJSON OperationDelegation where
  toJSON (OperationDelegation source' fee' counter' gasLimit' storageLimit'
          delegate') =
    object $
      [ "source"         .= source'
      , "fee"            .= fee'
      , "counter"        .= counter'
      , "gas_limit"      .= gasLimit'
      , "storage_limit"  .= storageLimit'
      ] ++ catMaybes 
      [ "delegate"       .=? delegate'
      ]

instance FromJSON OperationDelegation where
  parseJSON = withObject "OperationDelegation" $ \o ->
    OperationDelegation
    <$> o .:  "source"
    <*> o .:  "fee"
    <*> o .:  "counter"
    <*> o .:  "gas_limit"
    <*> o .:  "storage_limit"
    <*> o .:? "delegate"

data OperationContents
  = OperationContentsEndorsement {level :: Int64}  
  | OperationContentsSeedNonceRevelation OperationSeedNonceRevelation
  | OperationContentsDoubleEndorsementEvidence OperationDoubleEndorsementEvidence
  | OperationContentsDoubleBakingEvidence OperationDoubleBakingEvidence
  | OperationContentsActivateAccount OperationActivateAccount 
  | OperationContentsProposals OperationProposals
  | OperationContentsBallot OperationBallot
  | OperationContentsReveal OperationReveal
  | OperationContentsTransaction OperationTransaction
  | OperationContentsOrigination OperationOrigination
  | OperationContentsDelegation OperationDelegation
  deriving (Eq, Show, Generic)

instance ToJSON OperationContents where
  toJSON (OperationContentsEndorsement level') =
    object
      [ "kind"  .= ("endorsement" :: Text)
      , "level" .= level'
      ]

  toJSON (OperationContentsSeedNonceRevelation revelation') =
    combineValues
    (object ["kind" .= ("seed_nonce_revelation" :: Text)])
    (toJSON revelation')
    
  toJSON (OperationContentsDoubleEndorsementEvidence evidence') =
    combineValues
    (object ["kind" .= ("double_endorsement_evidence" :: Text)])
    (toJSON evidence')

  toJSON (OperationContentsDoubleBakingEvidence doubleBaking') =
    combineValues
    (object ["kind" .= ("double_baking_evidence" :: Text)])
    (toJSON doubleBaking')

  toJSON (OperationContentsActivateAccount activateAccount') =
    combineValues
    (object ["kind" .= ("activate_account" :: Text)])
    (toJSON activateAccount')

  toJSON (OperationContentsProposals proposals') =
    combineValues (object ["kind" .= ("proposals" :: Text)]) (toJSON proposals')

  toJSON (OperationContentsBallot ballot') =
    combineValues (object ["kind" .= ("ballot" :: Text)]) (toJSON ballot')

  toJSON (OperationContentsReveal reveal') =
    combineValues (object ["kind" .= ("reveal" :: Text)])
    (toJSON reveal')

  toJSON (OperationContentsTransaction transaction') =
    combineValues (object ["kind" .= ("transaction" :: Text)])
    (toJSON transaction')
    
  toJSON (OperationContentsOrigination origination') =
    combineValues (object [ "kind" .= ("origination" :: Text)]) (toJSON origination')

  toJSON (OperationContentsDelegation delegation') =
    combineValues (object ["kind" .= ("delegation" :: Text)]) (toJSON delegation')

instance FromJSON OperationContents where
  parseJSON = withObject "OperationContents" $ \o ->
    case HM.lookup "kind" o of
      Just "endorsement" ->
        OperationContentsEndorsement <$> o .: "level"

      Just "seed_nonce_revelation" ->
        OperationContentsSeedNonceRevelation  <$> parseJSON (Object o)

      Just "double_endorsement_evidence" ->
        OperationContentsDoubleEndorsementEvidence <$> parseJSON (Object o)

      Just "double_baking_evidence" ->
        OperationContentsDoubleBakingEvidence <$> parseJSON (Object o)

      Just "activate_account" ->
        OperationContentsActivateAccount <$> parseJSON (Object o)

      Just "proposals" ->
        OperationContentsProposals <$> parseJSON (Object o)

      Just "ballot" ->
        OperationContentsBallot <$> parseJSON (Object o)

      Just "reveal" ->
        OperationContentsReveal <$> parseJSON (Object o) 

      Just "transaction" ->
        OperationContentsTransaction <$> parseJSON (Object o)

      Just "origination" ->
        OperationContentsOrigination <$> parseJSON (Object o)

      Just "delegation" ->
        OperationContentsDelegation <$> parseJSON (Object o)

      _ -> fail "OperationContents expected a key 'kind'."


-- $alpha.operation.alpha.operation_contents_and_result:
--   { /* Endorsement */
--     "kind": "endorsement",
--     "level": integer ∈ [-2^31-2, 2^31+2],
--     "metadata":
--       { "balance_updates": $alpha.operation_metadata.alpha.balance_updates,
--         "delegate": $Signature.Public_key_hash,
--         "slots": [ integer ∈ [0, 255] ... ] } }
--   || { /* Seed_nonce_revelation */
--        "kind": "seed_nonce_revelation",
--        "level": integer ∈ [-2^31-2, 2^31+2],
--        "nonce": /^[a-zA-Z0-9]+$/,
--        "metadata":
--          { "balance_updates":
--              $alpha.operation_metadata.alpha.balance_updates } }
--   || { /* Double_endorsement_evidence */
--        "kind": "double_endorsement_evidence",
--        "op1": $alpha.inlined.endorsement,
--        "op2": $alpha.inlined.endorsement,
--        "metadata":
--          { "balance_updates":
--              $alpha.operation_metadata.alpha.balance_updates } }
--   || { /* Double_baking_evidence */
--        "kind": "double_baking_evidence",
--        "bh1": $alpha.block_header.alpha.full_header,
--        "bh2": $alpha.block_header.alpha.full_header,
--        "metadata":
--          { "balance_updates":
--              $alpha.operation_metadata.alpha.balance_updates } }
--   || { /* Activate_account */
--        "kind": "activate_account",
--        "pkh": $Ed25519.Public_key_hash,
--        "secret": /^[a-zA-Z0-9]+$/,
--        "metadata":
--          { "balance_updates":
--              $alpha.operation_metadata.alpha.balance_updates } }
--   || { /* Proposals */
--        "kind": "proposals",
--        "source": $Signature.Public_key_hash,
--        "period": integer ∈ [-2^31-2, 2^31+2],
--        "proposals": [ $Protocol_hash ... ],
--        "metadata": {  } }
--   || { /* Ballot */
--        "kind": "ballot",
--        "source": $Signature.Public_key_hash,
--        "period": integer ∈ [-2^31-2, 2^31+2],
--        "proposal": $Protocol_hash,
--        "ballot": "nay" | "yay" | "pass",
--        "metadata": {  } }
--   || { /* Reveal */
--        "kind": "reveal",
--        "source": $Signature.Public_key_hash,
--        "fee": $alpha.mutez,
--        "counter": $positive_bignum,
--        "gas_limit": $positive_bignum,
--        "storage_limit": $positive_bignum,
--        "public_key": $Signature.Public_key,
--        "metadata":
--          { "balance_updates":
--              $alpha.operation_metadata.alpha.balance_updates,
--            "operation_result":
--              $alpha.operation.alpha.operation_result.reveal,
--            "internal_operation_results"?:
--              [ $alpha.operation.alpha.internal_operation_result ... ] } }
--   || { /* Transaction */
--        "kind": "transaction",
--        "source": $Signature.Public_key_hash,
--        "fee": $alpha.mutez,
--        "counter": $positive_bignum,
--        "gas_limit": $positive_bignum,
--        "storage_limit": $positive_bignum,
--        "amount": $alpha.mutez,
--        "destination": $alpha.contract_id,
--        "parameters"?:
--          { "entrypoint": $alpha.entrypoint,
--            "value": $micheline.alpha.michelson_v1.expression },
--        "metadata":
--          { "balance_updates":
--              $alpha.operation_metadata.alpha.balance_updates,
--            "operation_result":
--              $alpha.operation.alpha.operation_result.transaction,
--            "internal_operation_results"?:
--              [ $alpha.operation.alpha.internal_operation_result ... ] } }
--   || { /* Origination */
--        "kind": "origination",
--        "source": $Signature.Public_key_hash,
--        "fee": $alpha.mutez,
--        "counter": $positive_bignum,
--        "gas_limit": $positive_bignum,
--        "storage_limit": $positive_bignum,
--        "balance": $alpha.mutez,
--        "delegate"?: $Signature.Public_key_hash,
--        "script": $alpha.scripted.contracts,
--        "metadata":
--          { "balance_updates":
--              $alpha.operation_metadata.alpha.balance_updates,
--            "operation_result":
--              $alpha.operation.alpha.operation_result.origination,
--            "internal_operation_results"?:
--              [ $alpha.operation.alpha.internal_operation_result ... ] } }
--   || { /* Delegation */
--        "kind": "delegation",
--        "source": $Signature.Public_key_hash,
--        "fee": $alpha.mutez,
--        "counter": $positive_bignum,
--        "gas_limit": $positive_bignum,
--        "storage_limit": $positive_bignum,
--        "delegate"?: $Signature.Public_key_hash,
--        "metadata":
--          { "balance_updates":
--              $alpha.operation_metadata.alpha.balance_updates,
--            "operation_result":
--              $alpha.operation.alpha.operation_result.delegation,
--            "internal_operation_results"?:
--              [ $alpha.operation.alpha.internal_operation_result ... ] } }

data OperationContentsAndResult
  = OperationContentsAndResultEndorsement
    { level    :: Int64
    , metadataEndorsement :: OperationEndorsementMetadata
    }  
  | OperationContentsAndResultEndorsementWithSlot
    { metadataEndorsement :: OperationEndorsementMetadata
    , slot     :: Int64
    }  
  | OperationContentsAndResultSeedNonceRevelation OperationSeedNonceRevelation BalanceUpdates
  | OperationContentsAndResultDoubleEndorsementEvidence OperationDoubleEndorsementEvidence BalanceUpdates
  | OperationContentsAndResultDoubleBakingEvidence OperationDoubleBakingEvidence BalanceUpdates
  | OperationContentsAndResultActivateAccount OperationActivateAccount BalanceUpdates
  | OperationContentsAndResultProposals OperationProposals
  | OperationContentsAndResultBallot OperationBallot
  | OperationContentsAndResultReveal OperationReveal (InternalOperationResultsMetadata Reveal)
  | OperationContentsAndResultTransaction OperationTransaction (InternalOperationResultsMetadata Transaction)
  | OperationContentsAndResultOrigination OperationOrigination (InternalOperationResultsMetadata Origination)
  | OperationContentsAndResultDelegation OperationDelegation (InternalOperationResultsMetadata Delegation)
  deriving (Eq, Show, Generic)

instance ToJSON OperationContentsAndResult where
  toJSON (OperationContentsAndResultEndorsement level' metadataEndorsement') =
    object
      [ "kind"     .= ("endorsement" :: Text)
      , "level"    .= level'
      , "metadata" .= metadataEndorsement'
      ]

  toJSON (OperationContentsAndResultEndorsementWithSlot slot' metadataEndorsement') =
    object
      [ "kind"     .= ("endorsement_with_slot" :: Text)
      , "slot"     .= slot'
      , "metadata" .= metadataEndorsement'
      ]

  toJSON (OperationContentsAndResultSeedNonceRevelation seedNonceRevelation' metadata') =
    combineValues
    (object ["kind" .= ("seed_nonce_revelation" :: Text), "metadata" .= metadata'])
    (toJSON seedNonceRevelation')

  toJSON (OperationContentsAndResultDoubleEndorsementEvidence doubleEndorsement' metadata') =
    combineValues
    (object ["kind" .= ("double_endorsement_evidence" :: Text),"metadata" .= metadata'])
    (toJSON doubleEndorsement')

  toJSON (OperationContentsAndResultDoubleBakingEvidence doubleBaking' metadata') =
    combineValues
    (object ["kind" .= ("double_baking_evidence" :: Text), "metadata" .= metadata'])
    (toJSON doubleBaking')

  toJSON (OperationContentsAndResultActivateAccount activateAccount' metadata') =
    combineValues
    (object ["kind" .= ("activate_account" :: Text), "metadata" .= metadata'])
    (toJSON activateAccount')

  toJSON (OperationContentsAndResultProposals proposals') =
    combineValues
    (object ["kind" .= ("proposals" :: Text), "metadata" .= (Object HM.empty)])
    (toJSON proposals')    

  toJSON (OperationContentsAndResultBallot ballot') =
    combineValues
    (object ["kind" .= ("ballot" :: Text), "metadata"  .= (Object HM.empty)])
    (toJSON ballot')

  toJSON (OperationContentsAndResultReveal reveal' metadata') =
    combineValues
    (object ["kind" .= ("reveal" :: Text), "metadata" .= metadata'])
    (toJSON reveal')

  toJSON (OperationContentsAndResultTransaction transaction' metadata') =
    combineValues
    (object ["kind" .= ("transaction" :: Text), "metadata" .= metadata'])
    (toJSON transaction')
    
  toJSON (OperationContentsAndResultOrigination origination' metadata') =
    combineValues
    (object ["kind" .= ("origination" :: Text), "metadata" .= metadata'])
    (toJSON origination')

  toJSON (OperationContentsAndResultDelegation delegation' metadata') =
    combineValues
    (object [ "kind" .= ("delegation" :: Text), "metadata" .= metadata'])
    (toJSON delegation')

instance FromJSON OperationContentsAndResult where
  parseJSON = withObject "OperationContentsAndResult" $ \o ->
    case HM.lookup "kind" o of
      Just "endorsement" ->
        OperationContentsAndResultEndorsement
        <$> o .: "level"
        <*> o .: "metadata"

      Just "endorsement_with_slot" ->
        OperationContentsAndResultEndorsementWithSlot
        <$> o .: "metadata"
        <*> o .: "slot"

      Just "seed_nonce_revelation" ->
        OperationContentsAndResultSeedNonceRevelation
        <$> parseJSON (Object o)
        <*> o .: "metadata"

      Just "double_endorsement_evidence" ->
        OperationContentsAndResultDoubleEndorsementEvidence
        <$> parseJSON (Object o)
        <*> o .: "metadata"

      Just "double_baking_evidence" ->
        OperationContentsAndResultDoubleBakingEvidence
        <$> parseJSON (Object o)
        <*> o .: "metadata"

      Just "activate_account" ->
        OperationContentsAndResultActivateAccount
        <$> parseJSON (Object o)
        <*> o .: "metadata"

      Just "proposals" ->
        OperationContentsAndResultProposals
        <$> parseJSON (Object o)

      Just "ballot" ->
        OperationContentsAndResultBallot
        <$> parseJSON (Object o)

      Just "reveal" ->
        OperationContentsAndResultReveal
        <$> parseJSON (Object o)
        <*> o .: "metadata"

      Just "transaction" ->
        OperationContentsAndResultTransaction
        <$> parseJSON (Object o)
        <*> o .: "metadata"

      Just "origination" ->
        OperationContentsAndResultOrigination
        <$> parseJSON (Object o)
        <*> o .: "metadata"

      Just "delegation" ->
        OperationContentsAndResultDelegation
        <$> parseJSON (Object o)
        <*> o .: "metadata"

      Just unknown -> fail $ "OperationContentsAndResult does not know how to treat 'kind':"<> show unknown <> " in object: " <> show (HM.toList o)
      Nothing -> fail $ "OperationContentsAndResult expected a key 'kind', found keys: " <> show (HM.keys o)

-- { "balance_updates": $operation_metadata.alpha.balance_updates,
--   "delegate": $Signature.Public_key_hash,
--   "slots": [ integer ∈ [0, 255] ... ] } }

data OperationEndorsementMetadata =
  OperationEndorsementMetadata
    { balanceUpdates :: [BalanceUpdate]
    , delegate       :: PublicKeyHash
    , slots          :: [Word8]
    } deriving (Eq, Show, Generic)

instance ToJSON OperationEndorsementMetadata where
  toJSON (OperationEndorsementMetadata balanceUpdates' delegate' slots') =
    object
      [ "balance_updates" .= balanceUpdates'
      , "delegate"        .= delegate'
      , "slots"           .= slots'
      ]

instance FromJSON OperationEndorsementMetadata where
  parseJSON = withObject "OperationEndorsementMetadata" $ \o ->
    OperationEndorsementMetadata <$> o .: "balance_updates"
                                 <*> o .: "delegate"
                                 <*> o .: "slots"

-- { "balance_updates": $operation_metadata.alpha.balance_updates,
--   "operation_result": $operation.alpha.operation_result.reveal,
--   "internal_operation_results"?:
--     [ $operation.alpha.internal_operation_result ... ] } }

data InternalOperationResultsMetadata a =
  InternalOperationResultsMetadata
    { balanceUpdates           :: [BalanceUpdate]
    , operationResult          :: a
    , internalOperationResults :: Maybe [InternalOperationResult]
    } deriving (Eq, Show, Generic)

instance (ToJSON a) => ToJSON (InternalOperationResultsMetadata a) where
  toJSON (InternalOperationResultsMetadata balanceUpdates' operationResult'
          internalOperationResults') =
    object $
      [ "balance_updates"           .= balanceUpdates'
      , "operation_result"          .= operationResult'
      ] ++ catMaybes 
      [ "internal_operation_results" .=? internalOperationResults'
      ]
  
instance (FromJSON a) => FromJSON (InternalOperationResultsMetadata a) where
  parseJSON = withObject "InternalOperationResultsMetadata" $ \o ->
    InternalOperationResultsMetadata <$> o .:  "balance_updates"
                                     <*> o .:  "operation_result"
                                     <*> o .:? "internal_operation_results"

-- $raw_block_header:
--   { "level": integer ∈ [-2^31-2, 2^31+2],
--     "proto": integer ∈ [0, 255],
--     "predecessor": $block_hash,
--     "timestamp": $timestamp,
--     "validation_pass": integer ∈ [0, 255],
--     "operations_hash": $Operation_list_list_hash,
--     "fitness": $fitness,
--     "context": $Context_hash,
--     "priority": integer ∈ [0, 2^16-1],
--     "proof_of_work_nonce": /^[a-zA-Z0-9]+$/,
--     "seed_nonce_hash"?: $cycle_nonce,
--     "signature": $Signature }

data RawBlockHeader =
  RawBlockHeader
    { level            :: Int64
    , proto            :: Word8
    , predecessor      :: BlockHash
    , timestamp        :: Timestamp
    , validationPass   :: Word8
    , operationsHash   :: Unistring
    , fitness          :: Fitness
    , context          :: ContextHash
    , priority         :: Word16
    , proofOfWorkNonce :: Maybe CycleNonce
    , signature        :: Signature
    } deriving (Eq, Show, Generic)

instance ToJSON RawBlockHeader where
  toJSON (RawBlockHeader level' proto' predecessor' timestamp' validationPass'
         operationsHash' fitness' context' priority' proofOfWorkNonce'
         signature') =
    object $
      [ "level"               .= level'
      , "proto"               .= proto'
      , "predecessor"         .= predecessor'
      , "timestamp"           .= timestamp'
      , "validation_pass"     .= validationPass'
      , "operations_hash"     .= operationsHash'
      , "fitness"             .= fitness'
      , "context"             .= context'
      , "priority"            .= priority'
      , "signature"           .= signature'
      ] ++ catMaybes ["proof_of_work_nonce" .=? proofOfWorkNonce']

instance FromJSON RawBlockHeader where
  parseJSON = withObject "RawBlockHeader" $ \o ->
    RawBlockHeader <$> o .:  "level"
                   <*> o .:  "proto"
                   <*> o .:  "predecessor"
                   <*> o .:  "timestamp"
                   <*> o .:  "validation_pass"
                   <*> o .:  "operations_hash"
                   <*> o .:  "fitness"
                   <*> o .:  "context"
                   <*> o .:  "priority"
                   <*> o .:? "proof_of_work_nonce"
                   <*> o .:  "signature"

-- $alpha.operation.alpha.internal_operation_result:
--   { /* reveal */
--     "kind": "reveal",
--     "source": $alpha.contract_id,
--     "nonce": integer ∈ [0, 2^16-1],
--     "public_key": $Signature.Public_key,
--     "result": $alpha.operation.alpha.operation_result.reveal }
--   || { /* transaction */
--        "kind": "transaction",
--        "source": $alpha.contract_id,
--        "nonce": integer ∈ [0, 2^16-1],
--        "amount": $alpha.mutez,
--        "destination": $alpha.contract_id,
--        "parameters"?:
--          { "entrypoint": $alpha.entrypoint,
--            "value": $micheline.alpha.michelson_v1.expression },
--        "result": $alpha.operation.alpha.operation_result.transaction }
--   || { /* origination */
--        "kind": "origination",
--        "source": $alpha.contract_id,
--        "nonce": integer ∈ [0, 2^16-1],
--        "balance": $alpha.mutez,
--        "delegate"?: $Signature.Public_key_hash,
--        "script": $alpha.scripted.contracts,
--        "result": $alpha.operation.alpha.operation_result.origination }
--   || { /* delegation */
--        "kind": "delegation",
--        "source": $alpha.contract_id,
--        "nonce": integer ∈ [0, 2^16-1],
--        "delegate"?: $Signature.Public_key_hash,
--        "result": $alpha.operation.alpha.operation_result.delegation }

data InternalOperationResult
  = InternalOperationResultReveal
    { source       :: ContractId
    , nonce        :: Word16
    , publicKey    :: PublicKey
    , resultReveal :: Reveal
    }
  | InternalOperationResultTransaction
    { source            :: ContractId
    , nonce             :: Word16
    , amount            :: Mutez
    , destination       :: ContractId
    , parameters        :: Maybe Parameters
    , resultTransaction :: Transaction
    }
  | InternalOperationResultOrigination
    { source            :: ContractId
    , nonce             :: Word16
    , balance           :: Mutez
    , delegate          :: Maybe PublicKeyHash
    , script            :: Contract
    , resultOrigination :: Origination
    }
  | InternalOperationResultDelegation
    { source           :: ContractId
    , nonce            :: Word16
    , delegate         :: Maybe PublicKeyHash
    , resultDelegation :: Delegation
    }
  deriving (Eq, Show, Generic)

instance ToJSON InternalOperationResult where
  toJSON (InternalOperationResultReveal source' nonce' publicKey'
          resultReveal') =
    object
      [ "kind"     .= ("reveal" :: Text)
      , "source"     .= source'
      , "nonce"      .= nonce'
      , "public_key" .= publicKey'
      , "result"     .= resultReveal'
      ]
  toJSON (InternalOperationResultTransaction source' nonce' amount'
          destination' parameters' resultTransaction') =
    object $
      [ "kind"     .= ("transaction" :: Text)
      , "source"      .= source'
      , "nonce"       .= nonce'
      , "amount"      .= amount'
      , "destination" .= destination'
      , "result"      .= resultTransaction'
      ] ++ catMaybes ["parameters"  .=? parameters']
  toJSON (InternalOperationResultOrigination source' nonce' balance' delegate'
          script' resultOrigination') =
    object $
      [ "kind"     .= ("origination" :: Text)
      , "source"   .= source'
      , "nonce"    .= nonce'
      , "balance"  .= balance'
      , "script"   .= script'
      , "result"   .= resultOrigination'
      ] ++ catMaybes ["delegate" .=? delegate']
  toJSON (InternalOperationResultDelegation source' nonce' delegate'
          resultDelegate') =
    object $
      [ "kind"     .= ("delegation" :: Text)
      , "source"   .= source'
      , "nonce"    .= nonce'
      , "result"   .= resultDelegate'
      ] ++ catMaybes ["delegate" .=? delegate']
  
instance FromJSON InternalOperationResult where
  parseJSON = withObject "InternalOperationResult" $ \o ->
    case HM.lookup "kind" o of
      Just "reveal"      ->
        InternalOperationResultReveal <$> o .: "source"
                                      <*> o .: "nonce"
                                      <*> o .: "public_key"
                                      <*> o .: "result"
      Just "transaction" ->
        InternalOperationResultTransaction <$> o .:  "source"
                                           <*> o .:  "nonce"
                                           <*> o .:  "amount"
                                           <*> o .:  "destination"
                                           <*> o .:? "parameters"
                                           <*> o .:  "result"
      Just "origination" ->
        InternalOperationResultOrigination <$> o .:  "source"
                                           <*> o .:  "nonce"
                                           <*> o .:  "balance"
                                           <*> o .:? "delegate"
                                           <*> o .:  "script"
                                           <*> o .:  "result"
      Just "delegation"  ->
        InternalOperationResultDelegation <$> o .:  "source"
                                          <*> o .:  "nonce"
                                          <*> o .:? "delegate"
                                          <*> o .:  "result"
      _ -> fail "InternalOperationResult expected a key 'kind'."

-- $alpha.operation.alpha.operation_result.delegation:
--   { /* Applied */
--     "status": "applied",
--     "consumed_gas"?: $positive_bignum,
--     "consumed_milligas"?: $positive_bignum }
--   || { /* Failed */
--        "status": "failed",
--        "errors": [ $alpha.error ... ] }
--   || { /* Skipped */
--        "status": "skipped" }
--   || { /* Backtracked */
--        "status": "backtracked",
--        "errors"?: [ $alpha.error ... ],
--        "consumed_gas"?: $positive_bignum,
--        "consumed_milligas"?: $positive_bignum }

data Delegation
  = DelegationApplied
    { consumedGas :: Maybe Bignum
    , consumedMilligas :: Maybe Bignum
    }
  | DelegationFailed
    { errors :: [Error] }
  | DelegationSkipped
  | DelegationBacktracked
    { consumedGas :: Maybe Bignum
    , consumedMilligas :: Maybe Bignum
    , mErrors     :: Maybe [Error]
    }
  deriving (Eq, Show, Generic)

instance ToJSON Delegation where
  toJSON (DelegationApplied consumedGas' consumedMilligas') =
    object $
      [ "status"         .= ("applied" :: Text )
      ] ++ catMaybes ["consumed_gas" .=? consumedGas', "consumed_milligas" .=? consumedMilligas']
  toJSON (DelegationFailed errors') =
    object
      [ "status"   .= ("failed" :: Text )
      , "errors" .= errors'
      ]
  toJSON DelegationSkipped =
    object
      [ "status" .= ("skipped" :: Text )
      ]
  toJSON (DelegationBacktracked consumedGas' consumedMilligas' mErrors') =
    object $
      [ "status"         .=  ("backtracked" :: Text )
      ] ++ catMaybes
      [ "consumed_gas" .=? consumedGas'
      , "consumed_milligas" .=? consumedMilligas'
      , "errors"       .=? mErrors'
      ]
    
instance FromJSON Delegation where
  parseJSON = withObject "Delegation" $ \o ->
    case HM.lookup "status" o of
      Just "applied"    ->
        DelegationApplied <$> o .:? "consumed_gas" <*> o .:? "consumed_milligas"
      Just "failed"     ->
        DelegationFailed <$> o .: "errors"
      Just "skipped"    -> pure DelegationSkipped
      Just "backtracked" ->
        DelegationBacktracked <$> o .:? "consumed_gas"
                              <*> o .:? "consumed_milligas"
                              <*> o .:? "errors"
      _ -> fail "Delegation expected a key 'status'."

-- $alpha.operation.alpha.operation_result.origination:
--   { /* Applied */
--     "status": "applied",
--     "big_map_diff"?:
--       [ { /* update */
--           "action": "update",
--           "big_map": $bignum,
--           "key_hash": $script_expr,
--           "key": $micheline.alpha.michelson_v1.expression,
--           "value"?: $micheline.alpha.michelson_v1.expression }
--         || { /* remove */
--              "action": "remove",
--              "big_map": $bignum }
--         || { /* copy */
--              "action": "copy",
--              "source_big_map": $bignum,
--              "destination_big_map": $bignum }
--         || { /* alloc */
--              "action": "alloc",
--              "big_map": $bignum,
--              "key_type": $micheline.alpha.michelson_v1.expression,
--              "value_type": $micheline.alpha.michelson_v1.expression } ... ],
--     "balance_updates"?: $alpha.operation_metadata.alpha.balance_updates,
--     "originated_contracts"?: [ $alpha.contract_id ... ],
--     "consumed_gas"?: $positive_bignum,
--     "consumed_milligas"?: $positive_bignum,
--     "storage_size"?: $bignum,
--     "paid_storage_size_diff"?: $bignum }
--   || { /* Failed */
--        "status": "failed",
--        "errors": [ $alpha.error ... ] }
--   || { /* Skipped */
--        "status": "skipped" }
--   || { /* Backtracked */
--        "status": "backtracked",
--        "errors"?: [ $alpha.error ... ],
--        "big_map_diff"?:
--          [ { /* update */
--              "action": "update",
--              "big_map": $bignum,
--              "key_hash": $script_expr,
--              "key": $micheline.alpha.michelson_v1.expression,
--              "value"?: $micheline.alpha.michelson_v1.expression }
--            || { /* remove */
--                 "action": "remove",
--                 "big_map": $bignum }
--            || { /* copy */
--                 "action": "copy",
--                 "source_big_map": $bignum,
--                 "destination_big_map": $bignum }
--            || { /* alloc */
--                 "action": "alloc",
--                 "big_map": $bignum,
--                 "key_type": $micheline.alpha.michelson_v1.expression,
--                 "value_type": $micheline.alpha.michelson_v1.expression } ... ],
--        "balance_updates"?: $alpha.operation_metadata.alpha.balance_updates,
--        "originated_contracts"?: [ $alpha.contract_id ... ],
--        "consumed_gas"?: $positive_bignum,
--        "consumed_milligas"?: $positive_bignum,
--        "storage_size"?: $bignum,
--        "paid_storage_size_diff"?: $bignum }
    
data Origination
  = OriginationApplied
      { bigMapDiff          :: Maybe [BigMapDiff]
      , balanceUpdates      :: Maybe [BalanceUpdate]
      , originatedContracts :: Maybe [ContractId]
      , consumedGas         :: Maybe Bignum
      , consumedMilligas    :: Maybe Bignum
      , storageSize         :: Maybe Bignum
      , paidStorageSizeDiff :: Maybe Bignum
      }
  | OriginationFailed
      { errors :: [Error] }
  | OriginationSkipped
  | OriginationBacktracked
      { mErrors             :: Maybe [Error]
      , bigMapDiff          :: Maybe [BigMapDiff]
      , balanceUpdates      :: Maybe [BalanceUpdate]
      , originatedContracts :: Maybe [ContractId]
      , consumedGas         :: Maybe Bignum
      , consumedMilligas    :: Maybe Bignum      
      , storageSize         :: Maybe Bignum
      , paidStorageSizeDiff :: Maybe Bignum
      }
  deriving (Eq, Show, Generic)

instance ToJSON Origination where
  toJSON (OriginationApplied bigMapDiff' balanceUpdates' originatedContracts'
          consumedGas' consumedMilligas' storageSize' paidStorageSizeDiff') =
    object $
      [ "status"                   .=  ("applied" :: Text )
      ] ++ catMaybes
      [ "big_map_diff"           .=? bigMapDiff'
      , "balance_updates"        .=? balanceUpdates'
      , "originated_contracts"   .=? originatedContracts'
      , "consumed_gas"           .=? consumedGas'
      , "consumed_milligas"      .=? consumedMilligas'
      , "storage_size"           .=? storageSize'
      , "paid_storage_size_diff" .=? paidStorageSizeDiff'
      ]
  toJSON (OriginationFailed errors') =
    object
      [ "status"   .= ("failed" :: Text )
      , "errors" .= errors'
      ]
  toJSON OriginationSkipped =
    object
      [ "status" .= ("skipped" :: Text )
      ]
  toJSON (OriginationBacktracked mErrors' bigMapDiff' balanceUpdates'
          originatedContracts' consumedGas' consumedMilligas' storageSize' paidStorageSizeDiff') =
    object $
      [ "status"                   .=  ("backtracked" :: Text )
      ] ++ catMaybes
      [ "errors"                 .=? mErrors'
      , "big_map_diff"           .=? bigMapDiff'
      , "balance_updates"        .=? balanceUpdates'
      , "originated_contracts"   .=? originatedContracts'
      , "consumed_gas"           .=? consumedGas'
      , "consumed_milligas"      .=? consumedMilligas'
      , "storage_size"           .=? storageSize'
      , "paid_storage_size_diff" .=? paidStorageSizeDiff'
      ]
  
instance FromJSON Origination where
  parseJSON = withObject "Origination" $ \o ->
    case HM.lookup "status" o of
      Just "applied"    ->
        OriginationApplied <$> o .:? "big_map_diff"
                           <*> o .:? "balance_updates"
                           <*> o .:? "originated_contracts"
                           <*> o .:? "consumed_gas"
                           <*> o .:? "consumed_milligas"
                           <*> o .:? "storage_size"
                           <*> o .:? "paid_storage_size_diff"
      Just "failed"     ->
        OriginationFailed <$> o .: "errors"
      Just "skipped"    -> pure OriginationSkipped
      Just "backtracked" ->
        OriginationBacktracked <$> o .:? "errors"
                               <*> o .:? "big_map_diff"
                               <*> o .:? "balance_updates"
                               <*> o .:? "originated_contracts"
                               <*> o .:? "consumed_gas"
                               <*> o .:? "consumed_milligas"
                               <*> o .:? "storage_size"
                               <*> o .:? "paid_storage_size_diff"
      _ -> fail "Origination expected a key 'status'."

-- $alpha.operation.alpha.operation_result.reveal:
--   { /* Applied */
--     "status": "applied",
--     "consumed_gas"?: $positive_bignum,
--     "consumed_milligas"?: $positive_bignum }
--   || { /* Failed */
--        "status": "failed",
--        "errors": [ $alpha.error ... ] }
--   || { /* Skipped */
--        "status": "skipped" }
--   || { /* Backtracked */
--        "status": "backtracked",
--        "errors"?: [ $alpha.error ... ],
--        "consumed_gas"?: $positive_bignum,
--        "consumed_milligas"?: $positive_bignum }

data Reveal
  = RevealApplied
      { consumedGas      :: Maybe Bignum
      , consumedMilligas :: Maybe Bignum
      }
  | RevealFailed
      { errors :: [Error] }
  | RevealSkipped
  | RevealBacktracked
      { consumedGas      :: Maybe Bignum
      , consumedMilligas :: Maybe Bignum      
      , mErrors          :: Maybe [Error]
      }
  deriving (Eq, Show, Generic)

instance ToJSON Reveal where
  toJSON (RevealApplied consumedGas' consumedMilligas') =
    object $
      [ "status"       .=  ("applied" :: Text )
      ] ++ catMaybes
      [ "consumed_gas"      .=? consumedGas'
      , "consumed_milligas" .=? consumedMilligas'
      ]
  toJSON (RevealFailed errors') =
    object
      [ "status" .= ("failed" :: Text )
      , "errors" .= errors'
      ]
  toJSON RevealSkipped =
    object
      [ "status" .= ("skipped" :: Text )
      ]
  toJSON (RevealBacktracked consumedGas' consumedMilligas' mErrors') =
    object $
      [ "status"       .= ("backtracked" :: Text )
      ] ++ catMaybes 
      [ "consumed_gas"      .=? consumedGas'
      , "consumed_milligas" .=? consumedMilligas'      
      , "errors"            .=? mErrors'
      ]

instance FromJSON Reveal where
  parseJSON = withObject "Reveal" $ \o ->
    case HM.lookup "status" o of
      Just "applied"    ->
        RevealApplied <$> o .:? "consumed_gas" <*> o .:? "consumed_milligas"
      Just "failed"     ->
        RevealFailed <$> o .: "errors"
      Just "skipped"    -> pure RevealSkipped
      Just "backtracked" ->
        RevealBacktracked <$> o .:? "consumed_gas"
                          <*> o .:? "consumed_milligas"
                          <*> o .:? "errors"
      _ -> fail "Reveal expected a key 'status'."

-- $alpha.operation.alpha.operation_result.transaction:
--   { /* Applied */
--     "status": "applied",
--     "storage"?: $micheline.alpha.michelson_v1.expression,
--     "big_map_diff"?:
--       [ { /* update */
--           "action": "update",
--           "big_map": $bignum,
--           "key_hash": $script_expr,
--           "key": $micheline.alpha.michelson_v1.expression,
--           "value"?: $micheline.alpha.michelson_v1.expression }
--         || { /* remove */
--              "action": "remove",
--              "big_map": $bignum }
--         || { /* copy */
--              "action": "copy",
--              "source_big_map": $bignum,
--              "destination_big_map": $bignum }
--         || { /* alloc */
--              "action": "alloc",
--              "big_map": $bignum,
--              "key_type": $micheline.alpha.michelson_v1.expression,
--              "value_type": $micheline.alpha.michelson_v1.expression } ... ],
--     "balance_updates"?: $alpha.operation_metadata.alpha.balance_updates,
--     "originated_contracts"?: [ $alpha.contract_id ... ],
--     "consumed_gas"?: $positive_bignum,
--     "consumed_milligas"?: $positive_bignum,
--     "storage_size"?: $bignum,
--     "paid_storage_size_diff"?: $bignum,
--     "allocated_destination_contract"?: boolean }
--   || { /* Failed */
--        "status": "failed",
--        "errors": [ $alpha.error ... ] }
--   || { /* Skipped */
--        "status": "skipped" }
--   || { /* Backtracked */
--        "status": "backtracked",
--        "errors"?: [ $alpha.error ... ],
--        "storage"?: $micheline.alpha.michelson_v1.expression,
--        "big_map_diff"?:
--          [ { /* update */
--              "action": "update",
--              "big_map": $bignum,
--              "key_hash": $script_expr,
--              "key": $micheline.alpha.michelson_v1.expression,
--              "value"?: $micheline.alpha.michelson_v1.expression }
--            || { /* remove */
--                 "action": "remove",
--                 "big_map": $bignum }
--            || { /* copy */
--                 "action": "copy",
--                 "source_big_map": $bignum,
--                 "destination_big_map": $bignum }
--            || { /* alloc */
--                 "action": "alloc",
--                 "big_map": $bignum,
--                 "key_type": $micheline.alpha.michelson_v1.expression,
--                 "value_type": $micheline.alpha.michelson_v1.expression } ... ],
--        "balance_updates"?: $alpha.operation_metadata.alpha.balance_updates,
--        "originated_contracts"?: [ $alpha.contract_id ... ],
--        "consumed_gas"?: $positive_bignum,
--        "consumed_milligas"?: $positive_bignum,
--        "storage_size"?: $bignum,
--        "paid_storage_size_diff"?: $bignum,
--        "allocated_destination_contract"?: boolean }

data Transaction 
  = TransactionApplied
      { storage              :: Maybe Expression
      , bigMapDiff           :: Maybe [BigMapDiff]
      , balanceUpdates       :: Maybe [BalanceUpdate]
      , originatedContracts  :: Maybe [ContractId]
      , consumedGas          :: Maybe Bignum
      , consumedMilligas     :: Maybe Bignum      
      , storageSize          :: Maybe Bignum
      , paidStorageSizeDiff  :: Maybe Bignum
      , allocatedDestinationContract :: Maybe Bool
      }
  | TransactionFailed
      { errors  :: [Error]
      }
  | TransactionSkipped
  | TransactionBacktracked
    { mErrors                      :: Maybe [Error]
    , storage                      :: Maybe Expression
    , bigMapDiff                   :: Maybe [BigMapDiff]
    , balanceUpdates               :: Maybe [BalanceUpdate]
    , originatedContracts          :: Maybe [ContractId]
    , consumedGas                  :: Maybe Bignum
    , consumedMilligas             :: Maybe Bignum    
    , storageSize                  :: Maybe Bignum
    , paidStorageSize              :: Maybe Bignum
    , allocatedDestinationContract :: Maybe Bool
    }
  deriving (Eq, Show, Generic)

instance ToJSON Transaction where
  toJSON (TransactionApplied storage' bigMapDiff' balanceUpdates'
          originatedContracts' consumedGas' consumedMilligas' storageSize' paidStorageSizeDiff'
          allocatedDestination') =
    object $
      [ "status"                   .=  ("applied" :: Text )
      ] ++ catMaybes
      [ "storage"                .=? storage'
      , "big_map_diff"           .=? bigMapDiff'
      , "balance_updates"        .=? balanceUpdates'
      , "originated_contracts"   .=? originatedContracts'
      , "consumed_gas"           .=? consumedGas'
      , "consumed_milligas"      .=? consumedMilligas'
      , "storage_size"           .=? storageSize'
      , "paid_storage_size_diff" .=? paidStorageSizeDiff'
      , "allocated_destination_contract"  .=? allocatedDestination'
      ]
  toJSON (TransactionFailed errors') =
    object
      [ "status"   .= ("failed" :: Text )
      , "errors" .= errors'
      ]
  toJSON TransactionSkipped =
    object
      [ "status" .= ("skipped" :: Text )
      ]
  toJSON (TransactionBacktracked mErrors' storage' bigMapDiff' balanceUpdates'
          originatedContracts' consumedGas' consumedMilligas' storageSize' paidStorageSizeDiff'
          allocatedDestination') =
    object $
      [ "status"                 .=  ("backtracked" :: Text )
      ] ++ catMaybes
      [ "errors"                 .=? mErrors'
      , "storage"                .=? storage'
      , "big_map_diff"           .=? bigMapDiff'
      , "balance_updates"        .=? balanceUpdates'
      , "originated_contracts"   .=? originatedContracts'
      , "consumed_gas"           .=? consumedGas'
      , "consumed_milligas"      .=? consumedMilligas'
      , "storage_size"           .=? storageSize'
      , "paid_storage_size_diff" .=? paidStorageSizeDiff'
      , "allocated_destination_contract"  .=? allocatedDestination'
      ]
  
instance FromJSON Transaction where
  parseJSON = withObject "Transaction" $ \o ->
    case HM.lookup "status" o of
      Just "applied"    ->
        TransactionApplied <$> o .:? "storage"
                           <*> o .:? "big_map_diff"
                           <*> o .:? "balance_updates"
                           <*> o .:? "originated_contracts"
                           <*> o .:? "consumed_gas"
                           <*> o .:? "consumed_milligas"                           
                           <*> o .:? "storage_size"
                           <*> o .:? "paid_storage_size_diff"
                           <*> o .:? "allocated_destination_contract"
      Just "failed"     ->
        TransactionFailed <$> o .: "errors"
      Just "skipped"    -> pure TransactionSkipped
      Just "backtracked" ->
        TransactionBacktracked <$> o .:? "errors"
                               <*> o .:? "storage"
                               <*> o .:? "big_map_diff"
                               <*> o .:? "balance_updates"
                               <*> o .:? "originated_contracts"
                               <*> o .:? "consumed_gas"
                               <*> o .:? "consumed_milligas"
                               <*> o .:? "storage_size"
                               <*> o .:? "paid_storage_size_diff"
                               <*> o .:? "allocated_destination_contract"
      _ -> fail "Transaction expected a key 'status'."
