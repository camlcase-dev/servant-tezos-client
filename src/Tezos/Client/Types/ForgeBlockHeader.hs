{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings     #-}

module Tezos.Client.Types.ForgeBlockHeader where

import Data.Aeson
  (FromJSON(parseJSON), ToJSON(toJSON), (.=), (.:), object, withObject)
import Data.Int (Int64)
import Data.Text (Text)
import Data.Word (Word8)
import GHC.Generics (Generic)
import Tezos.Client.Types.Core

-- { "level": integer ∈ [-2^31-2, 2^31+2],
--   "proto": integer ∈ [0, 255],
--   "predecessor": $block_hash,
--   "timestamp": $timestamp,
--   "validation_pass": integer ∈ [0, 255],
--   "operations_hash": $Operation_list_list_hash,
--   "fitness": $fitness,
--   "context": $Context_hash,
--   "protocol_data": /^[a-zA-Z0-9]+$/ }

data ForgeBlockHeader =
  ForgeBlockHeader
    { level          :: Int64
    , proto          :: Word8
    , predecessor    :: BlockHash
    , timestamp      :: Timestamp
    , validationPass :: Word8
    , operationsHash :: [[Text]]
    , fitness        :: Fitness
    , context        :: ContextHash
    , protocolData   :: Text
    } deriving (Eq, Show, Generic)

instance ToJSON ForgeBlockHeader where
  toJSON (ForgeBlockHeader level' proto' predecessor' timestamp' validationPass'
          operationsHash' fitness' context' protocolData') =
    object
      [ "level"           .= level'
      , "proto"           .= proto'
      , "predecessor"     .= predecessor'
      , "timestamp"       .= timestamp'
      , "validation_pass" .= validationPass'
      , "operations_hash" .= operationsHash'
      , "fitness"         .= fitness'
      , "context"         .= context'
      , "protocol_data"   .= protocolData'
      ]

instance FromJSON ForgeBlockHeader where
  parseJSON = withObject "ForgeBlockHeader" $ \o ->
    ForgeBlockHeader <$> o .: "level"
                     <*> o .: "proto"
                     <*> o .: "predecessor"
                     <*> o .: "timestamp"
                     <*> o .: "validation_pass"
                     <*> o .: "operations_hash"
                     <*> o .: "fitness"
                     <*> o .: "context"
                     <*> o .: "protocol_data"

-- { "block": /^[a-zA-Z0-9]+$/ }

data ForgedBlockHeader =
  ForgedBlockHeader
    { block :: Text
    } deriving (Eq, Show, Generic)

instance ToJSON ForgedBlockHeader where
  toJSON (ForgedBlockHeader block') =
    object ["block" .= block']

instance FromJSON ForgedBlockHeader where
  parseJSON = withObject "ForgedBlockHeader" $ \o ->
    ForgedBlockHeader <$> o .: "block"

