{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}

module Tezos.Client.Types.Endorsement where

import Data.Aeson
import Data.Text (Text)
import GHC.Generics (Generic)
import Tezos.Client.Types.Core
import Tezos.Client.Types.Crypto

-- $inlined.endorsement:
--   { "branch": $block_hash,
--     "operations": $inlined.endorsement.contents,
--     "signature"?: $Signature }

data Endorsement =
  Endorsement
    { branch     :: BlockHash
    , operations :: EndorsementContents
    , signature  :: Maybe Signature
    } deriving (Eq, Show, Generic)

instance ToJSON Endorsement where
  toJSON (Endorsement branch' operations' signature') =
    object
      [ "branch"     .= branch'
      , "operations" .= operations'
      , "signature"  .= signature'
      ]

instance FromJSON Endorsement where
  parseJSON = withObject "Endorsement" $ \o ->
    Endorsement <$> o .: "branch"
                <*> o .: "operations"
                <*> o .: "signature"

-- $inlined.endorsement.contents:
--   { "kind": "endorsement",
--     "level": integer ∈ [-2^31-2, 2^31+2] }

newtype EndorsementContents =
  EndorsementContents
    { level :: Int
    } deriving (Eq, Show, Generic)

instance ToJSON EndorsementContents where
  toJSON (EndorsementContents level') =
    object
      [ "kind"  .= ("endorsement" :: Text)
      , "level" .= level'
      ]

instance FromJSON EndorsementContents where
  parseJSON = withObject "EndorsementContents" $ \o ->
    EndorsementContents <$> o .: "level"
