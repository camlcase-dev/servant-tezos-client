{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings     #-}

module Tezos.Client.Types.ProtocolData where

import Data.Aeson
  (FromJSON(parseJSON), ToJSON(toJSON), (.=), (.:), (.:?), object, withObject)
import Data.Maybe (catMaybes)
import Data.Text (Text)
import Data.Word (Word16)
import GHC.Generics (Generic)
import Tezos.Client.Types.Core
import Tezos.Client.Types.Crypto
import Tezos.Client.Extension.Aeson ((.=?))

-- { "protocol": "Pt24m4xiPbLDhVgVfABUjirbmda3yohdN82Sp9FeuAXJ4eV9otd",
--   "priority": integer ∈ [0, 2^16-1],
--   "proof_of_work_nonce": /^[a-zA-Z0-9]+$/,
--   "seed_nonce_hash"?: $cycle_nonce,
--   "signature": $Signature }

data ProtocolData =
  ProtocolData
    { protocol         :: ProtocolHash
    , priority         :: Word16
    , proofOfWorkNonce :: Text
    , seedNonceHash    :: Maybe CycleNonce
    , signature        :: Signature
    } deriving (Eq, Show, Generic)

instance ToJSON ProtocolData where
  toJSON (ProtocolData protocol' priority' proofOfWorkNonce' seedNonceHash'
          signature') =
    object $
      [ "protocol"            .=  protocol'
      , "priority"            .=  priority'
      , "proof_of_work_nonce" .=  proofOfWorkNonce'
      , "signature"           .=  signature'
      ] ++ catMaybes
      [ "seed_nonce_hash"     .=? seedNonceHash'
      ]

instance FromJSON ProtocolData where
  parseJSON = withObject "ProtocolData" $ \o ->
    ProtocolData <$> o .:  "protocol"
                 <*> o .:  "priority"
                 <*> o .:  "proof_of_work_nonce"
                 <*> o .:? "seed_nonce_hash"
                 <*> o .:  "signature"
