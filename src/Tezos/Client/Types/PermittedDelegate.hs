{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings     #-}

module Tezos.Client.Types.PermittedDelegate where

import Data.Aeson
  (FromJSON(parseJSON), ToJSON(toJSON), (.=), (.:), (.:?), object, withObject)
import Data.Int (Int64)
import Data.Maybe (catMaybes)
import Data.Word (Word16)
import GHC.Generics (Generic)
import Tezos.Client.Types.Core
import Tezos.Client.Types.Crypto
import Tezos.Client.Extension.Aeson ((.=?))

-- [ { "level": integer ∈ [-2^31-2, 2^31+2],
--     "delegate": $Signature.Public_key_hash,
--     "slots": [ integer ∈ [0, 2^16-1] ... ],
--     "estimated_time"?: $timestamp } ... ]

data PermittedDelegate =
  PermittedDelegate
    { level         :: Int64
    , delegate      :: PublicKeyHash
    , slots         :: [Word16] 
    , estimatedTime :: Maybe Timestamp
    } deriving (Eq, Show, Generic)

instance ToJSON PermittedDelegate where
  toJSON (PermittedDelegate level' delegate' slots' estimatedTime') =
    object $
      [ "level"          .=  level'
      , "delegate"       .=  delegate'
      , "slots"          .=  slots'
      ] ++ catMaybes
      [ "estimated_time" .=? estimatedTime'
      ]

instance FromJSON PermittedDelegate where
  parseJSON = withObject "PermittedDelegate" $ \o ->
    PermittedDelegate <$> o .:  "level"
                      <*> o .:  "delegate"
                      <*> o .:  "slots"
                      <*> o .:? "estimated_time"
