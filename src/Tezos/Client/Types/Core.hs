{-# LANGUAGE DeriveDataTypeable         #-}
{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE DerivingStrategies         #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE TypeApplications           #-}
{-# LANGUAGE ViewPatterns               #-}

module Tezos.Client.Types.Core where

import qualified Data.Aeson as Aeson
import Data.Aeson
  (FromJSON(parseJSON), ToJSON(toJSON), Value, (.=), (.:), (.:?), object,
   withObject, withText)
import Data.Bifunctor (bimap, first)
import Data.ByteString (ByteString)
import qualified Data.ByteString.Char8 as BS
import Data.Data (Data(..))
import Data.Int (Int64)
import Data.Maybe (catMaybes, fromMaybe)
import Data.Scientific (toBoundedInteger)
import Data.Text (Text)
import qualified Data.Text as T
import Data.Text.Encoding (decodeUtf8, encodeUtf8)
import Data.Time.Clock (UTCTime)
import Data.Word (Word8, Word16, Word64)

import GHC.Generics (Generic)
import GHC.Stack (HasCallStack)
import Safe (readMay)

import Tezos.Client.Extension.Aeson ((.=?), combineValues)

import Web.HttpApiData (ToHttpApiData(toUrlPiece,toQueryParam),
                        FromHttpApiData(parseUrlPiece,parseQueryParam))

import Tezos.Client.Types.Crypto (encodeBase58Check)
import Text.Hex  (decodeHex)

--------------------------------------------------------------------------------
-- InternalByteString
--------------------------------------------------------------------------------

-- | ByteString does not have an instance for ToJSON and FromJSON, to
-- avoid orphan type class instances, make a new type wrapper around it.
newtype InternalByteString = InternalByteString ByteString
  deriving stock (Data, Eq, Ord)
  deriving newtype (Show)

unInternalByteString :: InternalByteString -> ByteString
unInternalByteString (InternalByteString bs) = bs

instance ToJSON InternalByteString where
  toJSON = toJSON @Text . decodeUtf8 . unInternalByteString

instance FromJSON InternalByteString where
  parseJSON = fmap (InternalByteString . encodeUtf8) . parseJSON

--------------------------------------------------------------------------------
-- Mutez
--------------------------------------------------------------------------------

-- | Mutez is a wrapper over integer data type. 1 mutez is 1 token (μTz).
newtype Mutez = Mutez
  { unMutez :: Word64
  } deriving stock (Show, Eq, Ord, Data, Generic)
    deriving newtype (Enum)

instance ToJSON Mutez where
  toJSON = toJSON . show . unMutez

instance FromJSON Mutez where
  parseJSON = withText "Mutez" $ \s ->
    case readMay (T.unpack s) :: Maybe Word64 of
      Just i  -> pure $ Mutez i
      Nothing -> error "Unable to parse Mutez. Expected a string encoded Word64."

instance Bounded Mutez where
  minBound = Mutez 0
  -- 2⁶³ - 1
  maxBound = Mutez 9223372036854775807

-- | Safely create 'Mutez' checking for overflow.
mkMutez :: Word64 -> Maybe Mutez
mkMutez n
  | n <= unMutez maxBound = Just (Mutez n)
  | otherwise = Nothing
{-# INLINE mkMutez #-}

-- | Addition of 'Mutez' values. Returns 'Nothing' in case of overflow.
addMutez :: Mutez -> Mutez -> Maybe Mutez
addMutez (unMutez -> a) (unMutez -> b) =
  mkMutez (a + b) -- (a + b) can't overflow if 'Mutez' values are valid
{-# INLINE addMutez #-}

-- | Subtraction of 'Mutez' values. Returns 'Nothing' when the
-- subtrahend is greater than the minuend, and 'Just' otherwise.
subMutez :: Mutez -> Mutez -> Maybe Mutez
subMutez (unMutez -> a) (unMutez -> b)
  | a >= b = Just (Mutez (a - b))
  | otherwise = Nothing
{-# INLINE subMutez #-}

-- | Multiplication of 'Mutez' and an integral number. Returns
-- 'Nothing' in case of overflow.
mulMutez :: Integral a => Mutez -> a -> Maybe Mutez
mulMutez (unMutez -> a) b
    | res <= toInteger (unMutez maxBound) = Just (Mutez (fromInteger res))
    | otherwise = Nothing
  where
    res = toInteger a * toInteger b
{-# INLINE mulMutez #-}

-- | Euclidian division of two 'Mutez' values.
divModMutez :: Mutez -> Mutez -> Maybe (Word64, Mutez)
divModMutez a (unMutez -> b) = first unMutez <$> divModMutezInt a b

-- | Euclidian division of  'Mutez' and a number.
divModMutezInt :: Integral a => Mutez -> a -> Maybe (Mutez, Mutez)
divModMutezInt (toInteger . unMutez -> a) (toInteger -> b)
  | b <= 0 = Nothing
  | otherwise = Just $ bimap toMutez toMutez (a `divMod` b)
  where
    toMutez :: Integer -> Mutez
    toMutez = Mutez . fromInteger

-- | 0 XTZ
zeroMutez :: Mutez
zeroMutez = Mutez minBound

-- | 0.000001 XTZ
oneMutez :: Mutez
oneMutez = Mutez 1

-- | Partial function for 'Mutez' creation, it's pre-condition is that
-- the argument must not exceed the maximal 'Mutez' value.
unsafeMkMutez :: HasCallStack => Word64 -> Mutez
unsafeMkMutez n =
  fromMaybe (error $ "mkMutez: overflow (" <> show n <> ")") (mkMutez n)
{-# INLINE unsafeMkMutez #-}

--------------------------------------------------------------------------------
-- Timestamp
--------------------------------------------------------------------------------

type Timestamp = UTCTime
-- -- | Time in the real world.
-- -- Use the functions below to convert it to/from Unix time in seconds.
-- newtype Timestamp = Timestamp
--   { unTimestamp :: POSIXTime
--   } deriving stock (Show, Eq, Ord, Data, Generic)

-- instance ToJSON Timestamp
-- instance FromJSON Timestamp

-- timestampToSeconds :: Integral a => Timestamp -> a
-- timestampToSeconds = round . unTimestamp
-- {-# INLINE timestampToSeconds #-}

-- timestampFromSeconds :: Integral a => a -> Timestamp
-- timestampFromSeconds = Timestamp . fromIntegral
-- {-# INLINE timestampFromSeconds #-}

-- timestampFromUTCTime :: UTCTime -> Timestamp
-- timestampFromUTCTime = Timestamp . utcTimeToPOSIXSeconds
-- {-# INLINE timestampFromUTCTime #-}

-- -- | Add given amount of seconds to a 'Timestamp'.
-- timestampPlusSeconds :: Timestamp -> Integer -> Timestamp
-- timestampPlusSeconds ts sec = timestampFromSeconds (timestampToSeconds ts + sec)

-- -- | Display timestamp in human-readable way as used by Michelson.
-- -- Uses UTC timezone, though maybe we should take it as an argument.
-- formatTimestamp :: Timestamp -> Text
-- formatTimestamp =
--   formatTimeRFC3339 . utcToZonedTime utc . posixSecondsToUTCTime . unTimestamp

-- -- | Parse textual representation of 'Timestamp'.
-- parseTimestamp :: Text -> Maybe Timestamp
-- parseTimestamp = fmap (timestampFromUTCTime . zonedTimeToUTC) . parseTimeRFC3339

-- -- | Return current time as 'Timestamp'.
-- getCurrentTime :: IO Timestamp
-- getCurrentTime = Timestamp <$> getPOSIXTime

--------------------------------------------------------------------------------
-- Unistring
--------------------------------------------------------------------------------

-- $unistring:
--   /* Universal string representation
--      Either a plain UTF8 string, or a sequence of bytes for strings that
--      contain invalid byte sequences. */
--   string || { "invalid_utf8_string": [ integer ∈ [0, 255] ... ] }

data Unistring
  = UnistringText Text
  | InvalidUtf8String [Word8]
  deriving (Eq, Show, Ord, Data, Generic)

instance ToJSON Unistring where
  toJSON (UnistringText t) = toJSON t
  toJSON (InvalidUtf8String ws) = object ["invalid_utf8_string" .= ws]

instance FromJSON Unistring where
  parseJSON (Aeson.String s) = pure $ UnistringText s
  parseJSON (Aeson.Object o) = InvalidUtf8String <$> o .: "invalid_utf8_string"
  parseJSON json = fail $ "Unistring expected a string or an object, found " ++ (show json)

instance ToHttpApiData Unistring where
  toUrlPiece (UnistringText text)     = toUrlPiece text
  toUrlPiece (InvalidUtf8String ints) = T.concat $ toUrlPiece <$> ints

  toQueryParam (UnistringText text)     = toQueryParam text
  toQueryParam (InvalidUtf8String ints) = T.concat $ toQueryParam <$> ints

instance FromHttpApiData Unistring where
  parseUrlPiece = fmap UnistringText . parseUrlPiece

  parseQueryParam = fmap UnistringText . parseQueryParam


--------------------------------------------------------------------------------
-- ChainId
--------------------------------------------------------------------------------

-- $Chain_id:
--   /* Network identifier (Base58Check-encoded) */
--   $unistring

-- How is chain_id computed?
-- https://tezos.stackexchange.com/questions/503/how-is-chain-id-computed

data ChainId
  = ChainId Unistring
  | ChainIdMain
  deriving (Eq, Show, Generic, Ord)

instance ToJSON ChainId where
  toJSON (ChainId b) = toJSON b
  toJSON ChainIdMain = "main"
  
instance FromJSON ChainId where
  parseJSON = withText "ChainId" $ \s ->
    case s of
      "main" -> pure ChainIdMain
      _      -> pure $ ChainId . UnistringText $ s

instance ToHttpApiData ChainId where
  toUrlPiece (ChainId ibs) = toUrlPiece ibs
  toUrlPiece (ChainIdMain) = "main"

--------------------------------------------------------------------------------
-- BlockHash
--------------------------------------------------------------------------------

-- $block_hash:
--   /* A block identifier (Base58Check-encoded) */
--   $unistring

newtype BlockHash =
  BlockHash
    { unBlockHash :: Unistring
    } deriving (Eq, Show, Generic, Ord)
    
instance ToJSON BlockHash where
  toJSON = toJSON . unBlockHash

instance FromJSON BlockHash where
  parseJSON = fmap BlockHash . parseJSON

instance ToHttpApiData BlockHash where
  toUrlPiece = toUrlPiece . unBlockHash

  toQueryParam = toQueryParam . unBlockHash

instance FromHttpApiData BlockHash where
  parseUrlPiece = fmap BlockHash . parseUrlPiece

  parseQueryParam = fmap BlockHash . parseQueryParam


--------------------------------------------------------------------------------
-- ContextHash
--------------------------------------------------------------------------------

-- $Context_hash:
--   /* A hash of context (Base58Check-encoded) */
--   $unistring

newtype ContextHash =
  ContextHash
    { unContextHash :: Unistring
    } deriving (Eq, Show, Generic, Ord)

instance ToJSON ContextHash where
  toJSON = toJSON . unContextHash

instance FromJSON ContextHash where
  parseJSON = fmap ContextHash . parseJSON

instance ToHttpApiData ContextHash where
  toUrlPiece = toUrlPiece . unContextHash

  toQueryParam = toQueryParam . unContextHash

instance FromHttpApiData ContextHash where
  parseUrlPiece = fmap ContextHash . parseUrlPiece

  parseQueryParam = fmap ContextHash . parseQueryParam

--------------------------------------------------------------------------------
-- OperationHash
--------------------------------------------------------------------------------

-- $Operation_hash:
--   /* A Tezos operation ID (Base58Check-encoded) */
--   $unistring

newtype OperationHash =
  OperationHash
    { unOperationHash :: Unistring
    } deriving (Eq, Show, Generic, Ord)

instance ToJSON OperationHash where
  toJSON = toJSON . unOperationHash

instance FromJSON OperationHash where
  parseJSON = fmap OperationHash . parseJSON

instance ToHttpApiData OperationHash where
  toUrlPiece = toUrlPiece . unOperationHash

  toQueryParam = toQueryParam . unOperationHash

instance FromHttpApiData OperationHash where
  parseUrlPiece = fmap OperationHash . parseUrlPiece

  parseQueryParam = fmap OperationHash . parseQueryParam

--------------------------------------------------------------------------------
-- ProtocolHash
--------------------------------------------------------------------------------

-- $Protocol_hash:
--   /* A Tezos protocol ID (Base58Check-encoded) */
--   $unistring

newtype ProtocolHash =
  ProtocolHash
    { unProtocolHash :: Unistring
    } deriving (Eq, Show, Generic, Ord)

instance ToJSON ProtocolHash where
  toJSON = toJSON . unProtocolHash

instance FromJSON ProtocolHash where
  parseJSON = fmap ProtocolHash . parseJSON

instance ToHttpApiData ProtocolHash where
  toUrlPiece = toUrlPiece . unProtocolHash

  toQueryParam = toQueryParam . unProtocolHash

instance FromHttpApiData ProtocolHash where
  parseUrlPiece = fmap ProtocolHash . parseUrlPiece

  parseQueryParam = fmap ProtocolHash . parseQueryParam

--------------------------------------------------------------------------------
-- CycleNonce
--------------------------------------------------------------------------------

-- $cycle_nonce:
--   /* A nonce hash (Base58Check-encoded) */
--   $unistring

newtype CycleNonce =
  CycleNonce
    { unCycleNonce :: Unistring
    } deriving (Eq, Show, Generic, Ord)

instance ToJSON CycleNonce where
  toJSON (CycleNonce cycleNonce') = toJSON cycleNonce'

instance FromJSON CycleNonce where
  parseJSON = fmap CycleNonce . parseJSON

--------------------------------------------------------------------------------
-- Bignum
--------------------------------------------------------------------------------

-- $bignum:
--   /* Big number
--      Decimal representation of a big number */
--   string

newtype Bignum =
  Bignum
    { unBignum :: Integer
    } deriving (Eq, Show, Generic, Ord)

instance ToJSON Bignum where
  toJSON = toJSON . show . unBignum

instance FromJSON Bignum where
  parseJSON = withText "Bignum" $ \s ->
    case readMay (T.unpack s) :: Maybe Integer of
      Just i  -> pure $ Bignum i
      Nothing -> error "Unable to parse Bignum. Expected a string encoded integer."

--------------------------------------------------------------------------------
-- PositiveBignum
--------------------------------------------------------------------------------

-- $positive_bignum:
--     /* Positive big number
--        Decimal representation of a positive big number */
--    string

newtype PositiveBignum =
  PositiveBignum
    { unPositiveBignum :: Integer
    } deriving (Eq, Show, Generic, Ord)

instance ToJSON PositiveBignum where
  toJSON = toJSON . show . unPositiveBignum

instance FromJSON PositiveBignum where
  parseJSON = withText "PositiveBignum" $ \s ->
    case readMay (T.unpack s) :: Maybe Integer of
      Just i  -> pure $ PositiveBignum i
      Nothing -> error "Unable to parse PositiveBignum. Expected a string encoded integer."

--------------------------------------------------------------------------------
-- Error
--------------------------------------------------------------------------------
    
newtype Error =
  Error
    { unError :: Value
    } deriving (Eq, Show, Generic)

instance ToJSON Error where
  toJSON (Error error') = error'

instance FromJSON Error where
  parseJSON = pure . Error

--------------------------------------------------------------------------------
-- Fitness
--------------------------------------------------------------------------------

-- $fitness:
--   /* Block fitness
--      The fitness, or score, of a block, that allow the Tezos to decide
--      which chain is the best. A fitness value is a list of byte sequences.
--      They are compared as follows: shortest lists are smaller; lists of the
--      same length are compared according to the lexicographical order. */
--   [ /^[a-zA-Z0-9]+$/ ... ]

newtype Fitness =
  Fitness
    { unFitness :: [Text]
    } deriving (Eq, Show, Generic, Ord)

instance ToJSON Fitness where
  toJSON (Fitness fitness') = toJSON fitness'

instance FromJSON Fitness where
  parseJSON = fmap Fitness . parseJSON

--------------------------------------------------------------------------------
-- ScriptExpr
--------------------------------------------------------------------------------

-- $script_expr:
--   /* A script expression ID (Base58Check-encoded) */
--   $unistring

newtype ScriptExpr =
  ScriptExpr
    { unScriptExpr :: Unistring
    } deriving (Eq, Show, Generic, Ord)

instance ToJSON ScriptExpr where
  toJSON (ScriptExpr scriptExpr') = toJSON scriptExpr'

instance FromJSON ScriptExpr where
  parseJSON = fmap ScriptExpr . parseJSON

--------------------------------------------------------------------------------
-- ContractId
--------------------------------------------------------------------------------

-- $contract_id:
--   /* A contract handle
--      A contract notation as given to an RPC or inside scripts. Can be a
--      base58 implicit contract hash or a base58 originated contract hash. */
--   $unistring

newtype ContractId =
  ContractId
    { unContractId :: Unistring
    } deriving (Eq, Show, Generic, Ord)

instance ToJSON ContractId where
  toJSON (ContractId contractId') = toJSON contractId'

instance FromJSON ContractId where
  parseJSON = fmap ContractId . parseJSON

instance ToHttpApiData ContractId where
  toUrlPiece = toUrlPiece . unContractId
  toQueryParam = toQueryParam . unContractId

instance FromHttpApiData ContractId where
  parseUrlPiece = fmap ContractId . parseUrlPiece
  parseQueryParam =  fmap ContractId . parseQueryParam

parseBytes :: Text -> Maybe ContractId
parseBytes text =
  case decodeHex text of
    Nothing -> Nothing
    Just hex ->
      case BS.length hex of
        22 ->
          let (prefix,address) = BS.splitAt 2 hex in
          case BS.take 1 prefix of
            "\00" -> parseImplicitAccount (BS.tail prefix) address
            "\01" -> parseContract prefix address
            _     -> Nothing
          where
            parseImplicitAccount prefix address =
              let prefix' = case prefix of
                    "\00" -> Just "\006\161\159"
                    "\01" -> Just "\006\161\161"
                    "\02" -> Just "\006\161\164"
                    _ -> Nothing
              in case prefix' of
                   Nothing -> Nothing
                   Just prefix'' -> Just . ContractId . UnistringText . encodeBase58Check $ prefix'' <> address

            parseContract prefix address =
              if [BS.last address] == "\00"
              then
                -- drop last byte of address
                Just . ContractId . UnistringText . encodeBase58Check $ "\2\90\121" <> BS.drop 1 prefix <> BS.init address
              else Nothing
        _ -> Nothing

-- contractAddressPrefix = "\2\90\121"

--------------------------------------------------------------------------------
-- Int64AsString
--------------------------------------------------------------------------------

-- $int64:
--   /* 64 bit integers
--      Decimal representation of 64 bit integers */
--   string

newtype Int64AsString =
  Int64AsString
    { unInt64AsString :: Int64
    } deriving (Eq, Show, Generic, Ord)

instance ToJSON Int64AsString where
  toJSON = toJSON . show . unInt64AsString

instance FromJSON Int64AsString where
  parseJSON = withText "Int64AsString" $ \s ->
    case readMay (T.unpack s) :: Maybe Int64 of
      Just i  -> pure $ Int64AsString i
      Nothing -> error "Unable to parse Int64AsString. Expected a string encoded integer."

--------------------------------------------------------------------------------
-- Ballot
--------------------------------------------------------------------------------

data Ballot
  = Yay
  | Nay
  | Pass
  deriving (Eq, Show, Generic, Ord)

instance ToJSON Ballot where
  toJSON Yay  = "yay"
  toJSON Nay  = "nay"
  toJSON Pass = "pass"

instance FromJSON Ballot where
  parseJSON = withText "Ballot" $ \t ->
    case t of
      "yay" -> pure Yay
      "nay" -> pure Nay
      "pass" -> pure Pass
      err -> error (show err)

--------------------------------------------------------------------------------
-- PeerId
--------------------------------------------------------------------------------

newtype PeerId =
  PeerId
    { unPeerId :: Unistring
    } deriving (Eq, Show, Generic, Ord)

instance ToJSON PeerId where
  toJSON = toJSON . unPeerId

instance FromJSON PeerId where
  parseJSON = fmap PeerId . parseJSON

instance ToHttpApiData PeerId where
  toUrlPiece = toUrlPiece . unPeerId

  toQueryParam = toQueryParam . unPeerId

instance FromHttpApiData PeerId where
  parseUrlPiece = fmap PeerId . parseUrlPiece

  parseQueryParam = fmap PeerId . parseQueryParam

--------------------------------------------------------------------------------
-- IdPoint
--------------------------------------------------------------------------------

data IdPoint =
  IdPoint
    { addr :: Unistring
    , port :: Maybe Word16
    } deriving (Eq, Show, Generic, Ord)

instance ToJSON IdPoint where
  toJSON (IdPoint addr' port') =
    object $
      [ "addr" .= addr'
      ] ++ catMaybes ["port" .=? port']
      
instance FromJSON IdPoint where
  parseJSON = withObject "IdPoint" $ \o ->
    IdPoint <$> o .:  "addr"
            <*> o .:? "port"

--------------------------------------------------------------------------------
-- BlockId
--------------------------------------------------------------------------------

data BlockId
  = BlockIdGenesis
  | BlockIdHead
  | BlockId BlockHash
  | BlockIdLevel Word64
  | BlockIdGenesisPredecessor Word64
  | BlockIdHeadPredecessor Word64
  | BlockIdPredecessor BlockHash Word64
  deriving (Eq,Show,Generic, Ord)

appendNumber :: ByteString -> Word64 -> Value
appendNumber bs n = toJSON . InternalByteString $ (bs <> (BS.pack . show $ n))

instance ToJSON BlockId where
  toJSON BlockIdGenesis = "genesis"
  toJSON BlockIdHead = "head"
  toJSON (BlockId b) = toJSON b
  toJSON (BlockIdLevel n) = toJSON n
  toJSON (BlockIdGenesisPredecessor n) = appendNumber "genesis~" n
  toJSON (BlockIdHeadPredecessor n) = appendNumber "head~" n 
  toJSON (BlockIdPredecessor bh n) = combineValues (toJSON bh) (Aeson.String $ "~" <> (T.pack $ show n))

instance FromJSON BlockId where
  parseJSON json = do
    case json of
      Aeson.Number scientific ->
        case toBoundedInteger scientific of
          Just n -> pure $ BlockIdLevel n
          Nothing -> fail "BlockIdLevel Word64: Out of bounds"
      Aeson.String s ->
        case s of
          "genesis" -> pure BlockIdGenesis
          "head"    -> pure BlockIdHead
          _ -> do
            let predecessor = T.splitOn "~" s
            if length predecessor > 1
              then do
                let prefix = T.concat $ init predecessor  
                    mNum   = readMay (T.unpack $ last predecessor) :: Maybe Word64
                case mNum of
                  Just num ->
                    case prefix of
                      "genesis" -> pure $ BlockIdGenesisPredecessor num
                      "head"    -> pure $ BlockIdHeadPredecessor num
                      _         -> pure $ BlockIdPredecessor (BlockHash . UnistringText $ prefix) num
                  Nothing  -> fail ""
              else 
                pure . BlockId . BlockHash . UnistringText $ s
      _ -> fail "" 

instance ToHttpApiData BlockId where
  toUrlPiece BlockIdGenesis = "genesis"
  toUrlPiece BlockIdHead    = "head"
  toUrlPiece (BlockId bh)   = toUrlPiece bh
  toUrlPiece (BlockIdLevel n)   = toUrlPiece n
  toUrlPiece (BlockIdGenesisPredecessor n) = decodeUtf8 ("genesis~" <> (BS.pack . show $ n))
  toUrlPiece (BlockIdHeadPredecessor n) = decodeUtf8 ("head~" <> (BS.pack . show $ n))
  toUrlPiece (BlockIdPredecessor bh n) = (toUrlPiece bh) <> (decodeUtf8 ("~" <> (BS.pack . show $ n)))

--------------------------------------------------------------------------------
-- ContractHash
--------------------------------------------------------------------------------

-- $Contract_hash:
--   /* A contract ID (Base58Check-encoded) */
--   $unistring

newtype ContractHash =
  ContractHash
    { unContractHash :: Unistring
    } deriving (Eq, Show, Generic, Ord)
    
instance ToJSON ContractHash where
  toJSON = toJSON . unContractHash

instance FromJSON ContractHash where
  parseJSON = fmap ContractHash . parseJSON

instance ToHttpApiData ContractHash where
  toUrlPiece = toUrlPiece . unContractHash

  toQueryParam = toQueryParam . unContractHash

instance FromHttpApiData ContractHash where
  parseUrlPiece = fmap ContractHash . parseUrlPiece

  parseQueryParam = fmap ContractHash . parseQueryParam

--------------------------------------------------------------------------------
-- Entrypoint
--------------------------------------------------------------------------------

-- $entrypoint:
--   /* entrypoint
--      Named entrypoint to a Michelson smart contract */
--   "default"
--   || "root"
--   || "do"
--   || "set_delegate"
--   || "remove_delegate"
--   || $unistring

data Entrypoint
  = EDefault
  | ERoot
  | EDo
  | ESetDelegate
  | ERemoveDelegate
  | EUnistring Unistring
  deriving (Eq, Show, Generic, Ord)

instance ToJSON Entrypoint where
  toJSON EDefault                = "default"
  toJSON ERoot                   = "root"
  toJSON EDo                     = "do"
  toJSON ESetDelegate            = "set_delegate"
  toJSON ERemoveDelegate         = "remove_delegate"
  toJSON (EUnistring unistring') = toJSON unistring'

instance FromJSON Entrypoint where
  parseJSON (Aeson.String s) =
    case s of
      "default"         -> pure EDefault
      "root"            -> pure ERoot
      "do"              -> pure EDo
      "set_delegate"    -> pure ESetDelegate
      "remove_delegate" -> pure ERemoveDelegate
      _                 -> pure $ EUnistring $ UnistringText s
  parseJSON (Aeson.Object o) = EUnistring . InvalidUtf8String <$> o .: "invalid_utf8_string"
  parseJSON json = fail $ "Entrypoint expected a string or an object, found " ++ (show json)
