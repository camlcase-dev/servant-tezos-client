{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings     #-}

module Tezos.Client.Types.BlockLevel where

import Data.Aeson
  (FromJSON(parseJSON), ToJSON(toJSON), (.=), (.:), object, withObject)
import Data.Int (Int64)
import GHC.Generics (Generic)

-- { "level": integer ∈ [-2^31-2, 2^31+2],
--   "level_position": integer ∈ [-2^31-2, 2^31+2],
--   "cycle": integer ∈ [-2^31-2, 2^31+2],
--   "cycle_position": integer ∈ [-2^31-2, 2^31+2],
--   "voting_period": integer ∈ [-2^31-2, 2^31+2],
--   "voting_period_position": integer ∈ [-2^31-2, 2^31+2],
--   "expected_commitment": boolean }

data BlockLevel =
  BlockLevel
    { level                :: Int64
    , levelPosition        :: Int64
    , cycle                :: Int64
    , cyclePosition        :: Int64
    , votingPeriod         :: Int64
    , votingPeriodPosition :: Int64
    , expectedCommitment   :: Bool
    } deriving (Eq, Show, Generic)

instance ToJSON BlockLevel where
  toJSON (BlockLevel level' levelPosition' cycle' cyclePosition' votingPeriod'
          votingPeriodPosition' expectedCommitment') =
    object
      [ "level"                  .= level'
      , "level_position"         .= levelPosition'
      , "cycle"                  .= cycle'
      , "cycle_position"         .= cyclePosition'
      , "voting_period"          .= votingPeriod'
      , "voting_period_position" .= votingPeriodPosition'
      , "expected_commitment"    .= expectedCommitment'
      ]

instance FromJSON BlockLevel where
  parseJSON = withObject "BlockLevel" $ \o ->
    BlockLevel <$> o .: "level"
               <*> o .: "level_position"
               <*> o .: "cycle"
               <*> o .: "cycle_position"
               <*> o .: "voting_period"
               <*> o .: "voting_period_position"
               <*> o .: "expected_commitment"
