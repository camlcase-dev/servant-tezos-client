{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings #-}

module Tezos.Client.Types.ForgeOperations where

import Data.Aeson
  (FromJSON(parseJSON), ToJSON(toJSON), (.=), (.:),
   object, withObject)
import GHC.Generics (Generic)
import Tezos.Client.Types.Block
import Tezos.Client.Types.Core

-- $operation.alpha.unsigned_operation:
--    { "branch": $block_hash,
--      "contents": [ $operation.alpha.contents ... ] }

data UnsignedOperation =
  UnsignedOperation
    { branch   :: BlockHash
    , contents :: [OperationContents]
    } deriving (Eq, Show, Generic)

instance ToJSON UnsignedOperation where
  toJSON (UnsignedOperation branch' contents') =
    object
      [ "branch"   .= branch'
      , "contents" .= contents'
      ]

instance FromJSON UnsignedOperation where
  parseJSON = withObject "UnsignedOperation" $ \o ->
    UnsignedOperation <$> o .: "branch"
                      <*> o .: "contents"
