{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}

module Tezos.Client.Types.Micheline where

import Data.Aeson
  (ToJSON(toJSON), FromJSON(parseJSON), Value(Object, Array, String), object,
   (.:), (.:?), (.=), withObject, withText)
import Data.Char (isLower)
import Data.Int (Int64)
import Data.Maybe (catMaybes)
import Data.Text (Text, unpack)
import Data.Vector (toList)
import qualified Data.HashMap.Strict as HM
import GHC.Generics (Generic)
import Tezos.Client.Types.Core
import Tezos.Client.Extension.Aeson ((.=?), withInteger)

-- $scripted.contracts:
--   { "code": $micheline.michelson_v1.expression,
--     "storage": $micheline.michelson_v1.expression }

data Contract =
  Contract
    { code    :: Expression
    , storage :: Expression
    } deriving (Eq, Show, Generic)

instance ToJSON Contract where
  toJSON (Contract code' storage') =
    object
      [ "code"    .= code'
      , "storage" .= storage'
      ]

instance FromJSON Contract where
  parseJSON = withObject "Contract" $ \o ->
    Contract <$> o .: "code"
              <*> o .: "storage"

data BigMap =
  BigMap
    { bmKey  :: Expression
    , bmType :: Expression
    } deriving (Eq, Show, Generic)

instance ToJSON BigMap where
  toJSON (BigMap key' type') =
    object
      [ "key"  .= key'
      , "type" .= type'
      ]

instance FromJSON BigMap where
  parseJSON = withObject "BigMap" $ \o ->
    BigMap <$> o .: "key"
           <*> o .: "type"

-- $micheline.location:
--   /* Canonical location in a Micheline expression
--      The location of a node in a Micheline expression tree in prefix order,
--      with zero being the root and adding one for every basic node, sequence
--      and primitive application. */
--   integer ∈ [-2^30-2, 2^30+2]

newtype Location =
  Location
    { unLocation :: Int64
    } deriving (Eq, Show, Generic)

instance ToJSON Location where
  toJSON = toJSON . unLocation

instance FromJSON Location where
  parseJSON = withInteger "Location" $ \i -> pure $ Location i

-- $micheline.alpha.michelson_v1.expression:
--   { /* Int */
--     "int": $bignum }
--   || { /* String */
--        "string": $unistring }
--   || { /* Bytes */
--        "bytes": /^[a-zA-Z0-9]+$/ }
--   || [ $micheline.alpha.michelson_v1.expression ... ]
--   /* Sequence */
--   || { /* Generic prim (any number of args with or without annot) */
--        "prim": $alpha.michelson.v1.primitives,
--        "args"?: [ $micheline.alpha.michelson_v1.expression ... ],
--        "annots"?: [ string ... ] }

data Expression
  = IntExpression    Bignum
  | StringExpression Unistring
  | BytesExpression  Text
  | Expressions      [Expression]
  | Expression
    { prim   :: Primitives
    , args   :: Maybe [Expression]
    , annots :: Maybe [Text]
    }
  deriving (Eq, Show, Generic)

instance ToJSON Expression where
  toJSON (IntExpression bignum') =
    object ["int" .= bignum']

  toJSON (StringExpression unistring') =
    object ["string" .= unistring']

  toJSON (BytesExpression bytestring') =
    object ["bytes" .= bytestring']

  toJSON (Expressions expressions') = toJSON expressions'

  toJSON (Expression prim' args' annots') =
    object $ ( [ "prim" .=  prim']) ++
      catMaybes ["args" .=? args', "annots" .=? annots']
    
instance FromJSON Expression where
  parseJSON (Object o) = do
    let hasInt    = HM.member "int" o
        hasString = HM.member "string" o
        hasBytes  = HM.member "bytes" o
        hasPrim   = HM.member "prim" o
        parseExpression
          | hasInt    = IntExpression    <$> o .: "int"
          | hasString = StringExpression <$> o .: "string"
          | hasBytes  = BytesExpression  <$> o .: "bytes"
          | hasPrim   = Expression       <$> o .: "prim" <*> o .:? "args" <*> o .:? "annots"
          | otherwise = fail $ "Expression found an object but it was not valid " ++ (show o)
    parseExpression

  parseJSON (Array xs) = do
    expressions <- sequence . toList $ (parseJSON <$> xs)
    pure $ Expressions expressions

  parseJSON json = fail $ "Expression expected an object or an array, found " ++ show json

data Primitives
  = PrimitiveInstruction PrimitiveInstruction
  | PrimitiveData PrimitiveData
  | PrimitiveType PrimitiveType
  deriving (Eq, Show, Generic)

instance ToJSON Primitives where
  toJSON (PrimitiveInstruction primitiveInstruction') = toJSON primitiveInstruction'
  toJSON (PrimitiveData primitiveData') = toJSON primitiveData'
  toJSON (PrimitiveType primitiveType') = toJSON primitiveType'

instance FromJSON Primitives where
  parseJSON = withText "Primitives" $ \t -> do
    let s = unpack t
        l = length s

    if l > 1
    then
      if isLower (head s)
      then
        PrimitiveType <$> parseJSON (String t)
      else
        if isLower (head . tail $ s)
        then
          PrimitiveData <$> parseJSON (String t)
        else
          PrimitiveInstruction <$> parseJSON (String t)
      
    else fail "Primitives"


data PrimitiveInstruction
  = PIAdd
  | PILe
  | PIUnit
  | PICompare
  | PILambda
  | PILoop
  | PIImplicitAccount
  | PINone
  | PIBlake2B
  | PISha256
  | PIXor
  | PIRename
  | PIMap
  | PISetDelegate
  | PIDip
  | PIPack
  | PISize
  | PIIfCons
  | PILsr
  | PITransferTokens
  | PIUpdate
  | PICdr
  | PISwap
  | PISome
  | PISha512
  | PICheckSignature
  | PIBalance
  | PIEmptyBigMap
  | PIEmptySet
  | PISub
  | PIMem
  | PIRight
  | PIAddress
  | PIConcat
  | PIUnpack
  | PINot
  | PILeft
  | PIAmount
  | PIDrop
  | PIAbs
  | PIGe
  | PIPush
  | PILt
  | PINeq
  | PINeg
  | PICons
  | PIExec
  | PINil
  | PIIsnat
  | PIMul
  | PILoopLeft
  | PIEdiv
  | PISlice
  | PIStepsToQuota
  | PIInt
  | PISource
  | PICar
  | PICreateAccount
  | PILsl
  | PIOr
  | PIIfNone
  | PISelf
  | PIIf
  | PISender
  | PIDup
  | PIEq
  | PINow
  | PIGet
  | PIGt
  | PIIfLeft
  | PIFailwith
  | PIPair
  | PIIter
  | PICast
  | PIEmptyMap
  | PICreateContract
  | PIHashKey
  | PIContract
  | PIAnd
  | PIDig
  | PIDug
  | PIApply
  | PIKeccak
  | PITotalVotingPower
  | PIVotingPower
  | PISha3
  | PIReadTicket
  | PIPairingCheck
  | PILevel
  | PINever
  | PIGetAndUpdate
  | PIChainId
  | PISplitTicket
  | PISaplingVerifyUpdate
  | PITicket
  | PIJoinTickets
  | PISaplingEmptyState
  deriving (Eq, Show, Generic)

instance ToJSON PrimitiveInstruction where
  toJSON PIAdd             = "ADD"
  toJSON PILe              = "LE"
  toJSON PIUnit            = "UNIT"
  toJSON PICompare         = "COMPARE"
  toJSON PILambda          = "LAMBDA"
  toJSON PILoop            = "LOOP"
  toJSON PIImplicitAccount = "IMPLICIT_ACCOUNT"
  toJSON PINone            = "NONE"
  toJSON PIBlake2B         = "BLAKE2B"
  toJSON PISha256          = "SHA256"
  toJSON PIXor             = "XOR"
  toJSON PIRename          = "RENAME"
  toJSON PIMap             = "MAP"
  toJSON PISetDelegate     = "SET_DELEGATE"
  toJSON PIDip             = "DIP"
  toJSON PIPack            = "PACK"
  toJSON PISize            = "SIZE"
  toJSON PIIfCons          = "IF_CONS"
  toJSON PILsr             = "LSR"
  toJSON PITransferTokens  = "TRANSFER_TOKENS"
  toJSON PIUpdate          = "UPDATE"
  toJSON PICdr             = "CDR"
  toJSON PISwap            = "SWAP"
  toJSON PISome            = "SOME"
  toJSON PISha512          = "SHA512"
  toJSON PICheckSignature  = "CHECK_SIGNATURE"
  toJSON PIBalance         = "BALANCE"
  toJSON PIEmptyBigMap     = "EMPTY_BIG_MAP"
  toJSON PIEmptySet        = "EMPTY_SET"
  toJSON PISub             = "SUB"
  toJSON PIMem             = "MEM"
  toJSON PIRight           = "RIGHT"
  toJSON PIAddress         = "ADDRESS"
  toJSON PIConcat          = "CONCAT"
  toJSON PIUnpack          = "UNPACK"
  toJSON PINot             = "NOT"
  toJSON PILeft            = "LEFT"
  toJSON PIAmount          = "AMOUNT"
  toJSON PIDrop            = "DROP"
  toJSON PIAbs             = "ABS"
  toJSON PIGe              = "GE"
  toJSON PIPush            = "PUSH"
  toJSON PILt              = "LT"
  toJSON PINeq             = "NEQ"
  toJSON PINeg             = "NEG"
  toJSON PICons            = "CONS"
  toJSON PIExec            = "EXEC"
  toJSON PINil             = "NIL"
  toJSON PIIsnat           = "ISNAT"
  toJSON PIMul             = "MUL"
  toJSON PILoopLeft        = "LOOP_LEFT"
  toJSON PIEdiv            = "EDIV"
  toJSON PISlice           = "SLICE"
  toJSON PIStepsToQuota    = "STEPS_TO_QUOTA"
  toJSON PIInt             = "INT"
  toJSON PISource          = "SOURCE"
  toJSON PICar             = "CAR"
  toJSON PICreateAccount   = "CREATE_ACCOUNT"
  toJSON PILsl             = "LSL"
  toJSON PIOr              = "OR"
  toJSON PIIfNone          = "IF_NONE"
  toJSON PISelf            = "SELF"
  toJSON PIIf              = "IF"
  toJSON PISender          = "SENDER"
  toJSON PIDup             = "DUP"
  toJSON PIEq              = "EQ"
  toJSON PINow             = "NOW"
  toJSON PIGet             = "GET"
  toJSON PIGt              = "GT"
  toJSON PIIfLeft          = "IF_LEFT"
  toJSON PIFailwith        = "FAILWITH"
  toJSON PIPair            = "PAIR"
  toJSON PIIter            = "ITER"
  toJSON PICast            = "CAST"
  toJSON PIEmptyMap        = "EMPTY_MAP"
  toJSON PICreateContract  = "CREATE_CONTRACT"
  toJSON PIHashKey         = "HASH_KEY"
  toJSON PIContract        = "CONTRACT"
  toJSON PIAnd             = "AND"
  toJSON PIDig             = "DIG"
  toJSON PIDug             = "DUG"
  toJSON PIApply           = "APPLY"
  toJSON PIKeccak          = "KECCAK"
  toJSON PITotalVotingPower = "TOTAL_VOTING_POWER"
  toJSON PIVotingPower     = "VOTING_POWER"
  toJSON PISha3            = "SHA3"
  toJSON PIReadTicket      = "READ_TICKET"
  toJSON PIPairingCheck    = "PAIRING_CHECK"
  toJSON PILevel           = "LEVEL"
  toJSON PINever           = "NEVER"
  toJSON PIGetAndUpdate    = "GET_AND_UPDATE"
  toJSON PIChainId         = "CHAIN_ID"
  toJSON PISplitTicket     = "SPLIT_TICKET"
  toJSON PISaplingVerifyUpdate = "SAPLING_VERIFY_UPDATE"
  toJSON PITicket            = "TICKET"
  toJSON PIJoinTickets       = "JOIN_TICKETS"
  toJSON PISaplingEmptyState = "SAPLING_EMPTY_STATE"

    
instance FromJSON PrimitiveInstruction where
  parseJSON = withText "PrimitiveInstruction" $ \s ->
    case s of
      "ADD"              -> pure PIAdd
      "LE"               -> pure PILe
      "UNIT"             -> pure PIUnit
      "COMPARE"          -> pure PICompare
      "LAMBDA"           -> pure PILambda
      "LOOP"             -> pure PILoop
      "IMPLICIT_ACCOUNT" -> pure PIImplicitAccount
      "NONE"             -> pure PINone
      "BLAKE2B"          -> pure PIBlake2B
      "SHA256"           -> pure PISha256
      "XOR"              -> pure PIXor
      "RENAME"           -> pure PIRename
      "MAP"              -> pure PIMap
      "SET_DELEGATE"     -> pure PISetDelegate
      "DIP"              -> pure PIDip
      "PACK"             -> pure PIPack
      "SIZE"             -> pure PISize
      "IF_CONS"          -> pure PIIfCons
      "LSR"              -> pure PILsr
      "TRANSFER_TOKENS"  -> pure PITransferTokens
      "UPDATE"           -> pure PIUpdate
      "CDR"              -> pure PICdr
      "SWAP"             -> pure PISwap
      "SOME"             -> pure PISome
      "SHA512"           -> pure PISha512
      "CHECK_SIGNATURE"  -> pure PICheckSignature
      "BALANCE"          -> pure PIBalance
      "EMPTY_BIG_MAP"    -> pure PIEmptyBigMap
      "EMPTY_SET"        -> pure PIEmptySet
      "SUB"              -> pure PISub
      "MEM"              -> pure PIMem
      "RIGHT"            -> pure PIRight
      "ADDRESS"          -> pure PIAddress
      "CONCAT"           -> pure PIConcat
      "UNPACK"           -> pure PIUnpack
      "NOT"              -> pure PINot
      "LEFT"             -> pure PILeft
      "AMOUNT"           -> pure PIAmount
      "DROP"             -> pure PIDrop
      "ABS"              -> pure PIAbs
      "GE"               -> pure PIGe
      "PUSH"             -> pure PIPush
      "LT"               -> pure PILt
      "NEQ"              -> pure PINeq
      "NEG"              -> pure PINeg
      "CONS"             -> pure PICons
      "EXEC"             -> pure PIExec
      "NIL"              -> pure PINil
      "ISNAT"            -> pure PIIsnat
      "MUL"              -> pure PIMul
      "LOOP_LEFT"        -> pure PILoopLeft
      "EDIV"             -> pure PIEdiv
      "SLICE"            -> pure PISlice
      "STEPS_TO_QUOTA"   -> pure PIStepsToQuota
      "INT"              -> pure PIInt
      "SOURCE"           -> pure PISource
      "CAR"              -> pure PICar
      "CREATE_ACCOUNT"   -> pure PICreateAccount
      "LSL"              -> pure PILsl
      "OR"               -> pure PIOr
      "IF_NONE"          -> pure PIIfNone
      "SELF"             -> pure PISelf
      "IF"               -> pure PIIf
      "SENDER"           -> pure PISender
      "DUP"              -> pure PIDup
      "EQ"               -> pure PIEq
      "NOW"              -> pure PINow
      "GET"              -> pure PIGet
      "GT"               -> pure PIGt
      "IF_LEFT"          -> pure PIIfLeft
      "FAILWITH"         -> pure PIFailwith
      "PAIR"             -> pure PIPair
      "ITER"             -> pure PIIter
      "CAST"             -> pure PICast
      "EMPTY_MAP"        -> pure PIEmptyMap
      "CREATE_CONTRACT"  -> pure PICreateContract
      "HASH_KEY"         -> pure PIHashKey
      "CONTRACT"         -> pure PIContract
      "AND"              -> pure PIAnd
      "DIG"              -> pure PIDig
      "DUG"              -> pure PIDug
      "APPLY"            -> pure PIApply
      "KECCAK"           -> pure PIKeccak
      "TOTAL_VOTING_POWER" -> pure PITotalVotingPower
      "VOTING_POWER"     -> pure PIVotingPower
      "SHA3"             -> pure PISha3
      "READ_TICKET"      -> pure PIReadTicket
      "PAIRING_CHECK"    -> pure PIPairingCheck
      "LEVEL"            -> pure PILevel
      "NEVER"            -> pure PINever
      "GET_AND_UPDATE"   -> pure PIGetAndUpdate
      "CHAIN_ID"         -> pure PIChainId
      "SPLIT_TICKET"     -> pure PISplitTicket
      "SAPLING_VERIFY_UPDATE" -> pure PISaplingVerifyUpdate
      "TICKET"           -> pure PITicket
      "JOIN_TICKETS"     -> pure PIJoinTickets
      "SAPLING_EMPTY_STATE" -> pure PISaplingEmptyState      
      _                  -> fail $ "PrimitiveInstruction found unknown json " ++ show s

      

data PrimitiveData
  = PDElt
  | PDRight
  | PDFalse
  | PDUnit
  | PDSome
  | PDNone
  | PDLeft
  | PDTrue
  | PDPair
  deriving (Eq, Show, Generic)


instance ToJSON PrimitiveData where
  toJSON PDElt   = "Elt"
  toJSON PDRight = "Right"
  toJSON PDFalse = "False"
  toJSON PDUnit  = "Unit"
  toJSON PDSome  = "Some"
  toJSON PDNone  = "None"
  toJSON PDLeft  = "Left"
  toJSON PDTrue  = "True"
  toJSON PDPair  = "Pair"

instance FromJSON PrimitiveData where
  parseJSON = withText "PrimitiveData" $ \s ->
    case s of
      "Elt"   -> pure PDElt
      "Right" -> pure PDRight
      "False" -> pure PDFalse
      "Unit"  -> pure PDUnit
      "Some"  -> pure PDSome
      "None"  -> pure PDNone
      "Left"  -> pure PDLeft
      "True"  -> pure PDTrue
      "Pair"  -> pure PDPair
      _ -> fail "PrimitiveData"

data PrimitiveType
  = PTTimestamp
  | PTSignature
  | PTSet
  | PTPair
  | PTBytes
  | PTAddress
  | PTOr
  | PTList
  | PTStorage
  | PTKeyHash
  | PTUnit
  | PTOption
  | PTBigMap
  | PTString
  | PTMutez
  | PTBool
  | PTOperation
  | PTContract
  | PTMap
  | PTNat
  | PTKey
  | PTLambda
  | PTInt
  | PTParameter
  | PTCode
  | PTTicket
  | PTChainId
  | PTBls12_381_G1
  | PTSaplingTransaction
  | PTSaplingState
  | PTBls12_381_G2
  | PTBls12_381_FR
  deriving (Eq, Show, Generic)

instance ToJSON PrimitiveType where
  toJSON PTTimestamp = "timestamp"
  toJSON PTSignature = "signature"
  toJSON PTSet       = "set"
  toJSON PTPair      = "pair"
  toJSON PTBytes     = "bytes"
  toJSON PTAddress   = "address"
  toJSON PTOr        = "or"
  toJSON PTList      = "list"
  toJSON PTStorage   = "storage"
  toJSON PTKeyHash   = "key_hash"
  toJSON PTUnit      = "unit"
  toJSON PTOption    = "option"
  toJSON PTBigMap    = "big_map"
  toJSON PTString    = "string"
  toJSON PTMutez     = "mutez"
  toJSON PTBool      = "bool"
  toJSON PTOperation = "operation"
  toJSON PTContract  = "contract"
  toJSON PTMap       = "map"
  toJSON PTNat       = "nat"
  toJSON PTKey       = "key"
  toJSON PTLambda    = "lambda"
  toJSON PTInt       = "int"
  toJSON PTParameter = "parameter"
  toJSON PTCode      = "code"
  toJSON PTTicket    = "ticket"
  toJSON PTChainId   = "chain_id"
  toJSON PTBls12_381_G1 = "bls12_381_g1"
  toJSON PTSaplingTransaction = "sapling_transaction"
  toJSON PTSaplingState = "sapling_state"
  toJSON PTBls12_381_G2 = "bls12_381_g2"
  toJSON PTBls12_381_FR = "bls12_381_fr"

instance FromJSON PrimitiveType where
  parseJSON = withText "PrimitiveType" $ \s ->
    case s of
      "timestamp"  -> pure PTTimestamp
      "signature"  -> pure PTSignature
      "set"        -> pure PTSet
      "pair"       -> pure PTPair
      "bytes"      -> pure PTBytes
      "address"    -> pure PTAddress
      "or"         -> pure PTOr
      "list"       -> pure PTList
      "storage"    -> pure PTStorage
      "key_hash"   -> pure PTKeyHash
      "unit"       -> pure PTUnit
      "option"     -> pure PTOption
      "big_map"    -> pure PTBigMap
      "string"     -> pure PTString
      "mutez"      -> pure PTMutez
      "bool"       -> pure PTBool
      "operation"  -> pure PTOperation
      "contract"   -> pure PTContract
      "map"        -> pure PTMap
      "nat"        -> pure PTNat
      "key"        -> pure PTKey
      "lambda"     -> pure PTLambda
      "int"        -> pure PTInt
      "parameter"  -> pure PTParameter
      "code"       -> pure PTCode
      "ticket"     -> pure PTTicket
      "chain_id"   -> pure PTChainId
      "bls12_381_g1" -> pure PTBls12_381_G1
      "sapling_transaction" -> pure PTSaplingTransaction
      "sapling_state" -> pure PTSaplingState
      "bls12_381_g2" -> pure PTBls12_381_G2
      "bls12_381_fr" -> pure PTBls12_381_FR
      _ -> fail "PrimitiveType" 

data Parameters =
  Parameters
    { entrypoint :: Entrypoint
    , value      :: Expression
    } deriving (Eq, Show, Generic)

instance ToJSON Parameters where
  toJSON (Parameters entrypoint' value') =
    object $
      [ "entrypoint" .= entrypoint'
      , "value"      .= value'
      ]

instance FromJSON Parameters where
  parseJSON = withObject "Parameters" $ \o ->
    Parameters <$> o .:  "entrypoint"
               <*> o .:  "value"
