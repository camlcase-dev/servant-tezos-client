{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings     #-}

module Tezos.Client.Types.Connection where

import Data.Aeson
  (FromJSON(parseJSON), ToJSON(toJSON), (.=), (.:), object, withObject)
import Data.Word (Word16)
import GHC.Generics (Generic)
import Tezos.Client.Types.Core

-- [ { "incoming": boolean,
--     "peer_id": $Crypto_box.Public_key_hash,
--     "id_point": { "addr": $unistring,
--                   "port"?: integer ∈ [0, 2^16-1] },
--     "remote_socket_port": integer ∈ [0, 2^16-1],
--     "announced_version":
--       { "chain_name": $unistring,
--         "distributed_db_version": integer ∈ [0, 2^16-1],
--         "p2p_version": integer ∈ [0, 2^16-1] },
--     "private": boolean,
--     "local_metadata":
--       { "disable_mempool": boolean,
--         "private_node": boolean },
--     "remote_metadata":
--       { "disable_mempool": boolean,
--         "private_node": boolean } } ... ]

data P2PConnection =
  P2PConnection
    { incoming         :: Bool
    , peerId           :: PeerId
    , idPoint          :: IdPoint
    , remoteSocketPort :: Word16
    , announcedVersion :: AnnouncedVersion
    , private          :: Bool
    , localMetadata    :: P2PMetadata
    , remoteMetadata   :: P2PMetadata
    } deriving (Eq, Show, Generic)

instance ToJSON P2PConnection where
  toJSON (P2PConnection incoming' peerId' idPoint' remoteSocketPort' announcedVersion' private' localMetadata' remoteMetadata') =
    object
      [ "incoming"           .= incoming'
      , "peer_id"            .= peerId'
      , "id_point"           .= idPoint'
      , "remote_socket_port" .= remoteSocketPort'
      , "announced_version"  .= announcedVersion'
      , "private"            .= private'
      , "local_metadata"     .= localMetadata'
      , "remote_metadata"    .= remoteMetadata'
      ]
  
instance FromJSON P2PConnection where
  parseJSON = withObject "P2PConnection" $ \o ->
    P2PConnection <$> o .: "incoming"
                  <*> o .: "peer_id"
                  <*> o .: "id_point"
                  <*> o .: "remote_socket_port"
                  <*> o .: "announced_version"
                  <*> o .: "private"
                  <*> o .: "local_metadata"
                  <*> o .: "remote_metadata"

-- { "disable_mempool": boolean,
--   "private_node": boolean },

data P2PMetadata =
  P2PMetadata
    { disableMempool :: Bool
    , privateNode    :: Bool
    } deriving (Eq, Show, Generic)

instance ToJSON P2PMetadata where
  toJSON (P2PMetadata disableMempool' privateNode') =
    object
      [ "disable_mempool" .= disableMempool'
      , "private_node"    .= privateNode'
      ]

instance FromJSON P2PMetadata where
  parseJSON = withObject "P2PMetadata" $ \o ->
    P2PMetadata <$> o .: "disable_mempool"
                <*> o .: "private_node"

-- "announced_version":
--   { "chain_name": $unistring,
--     "distributed_db_version": integer ∈ [0, 2^16-1],
--     "p2p_version": integer ∈ [0, 2^16-1] }

data AnnouncedVersion =
  AnnouncedVersion
    { chainName            :: Unistring
    , distributedDbVersion :: Word16
    , p2pVersion           :: Word16
    } deriving (Eq, Show, Generic)

instance ToJSON AnnouncedVersion where
  toJSON (AnnouncedVersion chainName' distributedDbVersion' p2pVersion') =
    object
      [ "chain_name"             .= chainName'
      , "distributed_db_version" .= distributedDbVersion'
      , "p2p_version"            .= p2pVersion'
      ]

instance FromJSON AnnouncedVersion where
  parseJSON = withObject "AnnouncedVersion" $ \o ->
    AnnouncedVersion <$> o .: "chain_name"
                     <*> o .: "distributed_db_version"
                     <*> o .: "p2p_version"
