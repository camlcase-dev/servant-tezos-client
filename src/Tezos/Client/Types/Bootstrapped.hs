{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings     #-}

module Tezos.Client.Types.Bootstrapped where

import Data.Aeson
  (FromJSON(parseJSON), ToJSON(toJSON), (.=), (.:), object, withObject)
import GHC.Generics (Generic)
import Tezos.Client.Types.Core

data Bootstrapped
  = Bootstrapped
    { block     :: BlockHash
    , timestamp :: Timestamp
    } deriving (Eq, Show, Generic)

instance ToJSON Bootstrapped where
  toJSON (Bootstrapped block' timestamp') =
    object
      [ "block"     .= block'
      , "timestamp" .= timestamp'
      ]

instance FromJSON Bootstrapped where
  parseJSON = withObject "Bootstrapped" $ \o ->
    Bootstrapped <$> o .: "block"
                 <*> o .: "timestamp"
