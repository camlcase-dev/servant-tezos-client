{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings     #-}

module Tezos.Client.Types.ActiveChain where

import Data.Aeson
import qualified Data.HashMap.Lazy as HM
import GHC.Generics (Generic)
import Tezos.Client.Types.Core

--  [ { "chain_id": $Chain_id }
--    || { "chain_id": $Chain_id,
--         "test_protocol": $Protocol_hash,
--         "expiration_date": $timestamp }
--    || { "stopping": $Chain_id } ... ]

data ActiveChain
  = ActiveChain
    { chainId :: ChainId }
  | ActiveChainTestProtocol
    { chainId      :: ChainId
    , testProtocol :: ProtocolHash
    , expirationDate :: Timestamp
    }
  | ActiveChainStopping
    { chainId :: ChainId }
  deriving (Eq, Show, Generic)

instance ToJSON ActiveChain where
  toJSON (ActiveChain chainId') = object ["chain_id" .= chainId']
  toJSON (ActiveChainTestProtocol chainId' testProtocol' expirationDate') =
    object
      [ "chain_id" .= chainId'
      , "test_protocol" .= testProtocol'
      , "expiration_date" .= expirationDate'
      ]
  toJSON (ActiveChainStopping chainId') =
    object
      [ "stopping" .= chainId'
      ]
  
instance FromJSON ActiveChain where
  parseJSON = withObject "ActiveChain" $ \o ->
    if HM.member "stopping" o
    then
      ActiveChainStopping <$> o .: "stopping"
    else
      if HM.member "expiration_date" o
        then
          ActiveChainTestProtocol <$> o .: "chain_id"
                                  <*> o .: "test_protocol"
                                  <*> o .: "expiration_date"
        else
          ActiveChain <$> o .: "chain_id"
