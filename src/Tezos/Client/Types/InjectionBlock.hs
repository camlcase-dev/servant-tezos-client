{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings     #-}

module Tezos.Client.Types.InjectionBlock where

import Data.Aeson
  (ToJSON(toJSON), FromJSON(parseJSON), (.=), (.:), withObject, object)
import Data.Text (Text)
import GHC.Generics (Generic)
import Tezos.Client.Types.Core

-- { "data": /^[a-zA-Z0-9]+$/,
--   "operations":
--     [ [ { "branch": $block_hash,
--           "data": /^[a-zA-Z0-9]+$/ } ... ] ... ] }

data InjectionBlock =
  InjectionBlock
    { injectionBlockData :: Text
    , operations :: [[InjectionBlockOperation]]
    } deriving (Eq,Show,Generic)

instance ToJSON InjectionBlock where
  toJSON (InjectionBlock injectionBlockData' operations') =
    object
      [ "data"       .= injectionBlockData'
      , "operations" .= operations'
      ]

instance FromJSON InjectionBlock where
  parseJSON = withObject "InjectionBlock" $ \o ->
    InjectionBlock <$> o .: "data"
                   <*> o .: "operations"

-- { "branch": $block_hash,
--   "data": /^[a-zA-Z0-9]+$/ }

data InjectionBlockOperation =
  InjectionBlockOperation
    { branch :: BlockHash
    , data_  :: Text
    } deriving (Eq,Show,Generic)

instance ToJSON InjectionBlockOperation where
  toJSON (InjectionBlockOperation branch' blockData') =
    object
      [ "branch" .= branch'
      , "data"   .= blockData'
      ] 

instance FromJSON InjectionBlockOperation where
  parseJSON = withObject "InjectionBlockOperation" $ \o ->
    InjectionBlockOperation <$> o .: "branch"
                            <*> o .: "data"
