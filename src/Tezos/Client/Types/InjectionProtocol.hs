{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings     #-}

module Tezos.Client.Types.InjectionProtocol where

import Data.Aeson
  (ToJSON(toJSON), FromJSON(parseJSON), (.=), (.:), (.:?), withObject, object)
import Data.Maybe (catMaybes)
import Data.Int (Int32)
import Data.Text (Text)
import GHC.Generics (Generic)
import Tezos.Client.Extension.Aeson ((.=?))

-- { "expected_env_version": integer ∈ [-2^15, 2^15-1],
--    "components":
--      [ { "name": $unistring,
--          "interface"?: /^[a-zA-Z0-9]+$/,
--          "implementation": /^[a-zA-Z0-9]+$/ } ... ] }

data InjectionProtocol =
  InjectionProtocol
    { expectedEnvVersion :: Int32
    , components         :: [InjectionProtocolComponent]
    } deriving (Eq,Show,Generic)

instance ToJSON InjectionProtocol where
  toJSON (InjectionProtocol expectedEnvVersion' components') =
    object
      [ "expected_env_version" .= expectedEnvVersion'
      , "components"           .= components'
      ]

instance FromJSON InjectionProtocol where
  parseJSON = withObject "InjectionProtocol" $ \o ->
    InjectionProtocol <$> o .: "expected_env_version"
                      <*> o .: "components"

-- { "name": $unistring,
--   "interface"?: /^[a-zA-Z0-9]+$/,
--   "implementation": /^[a-zA-Z0-9]+$/ }

data InjectionProtocolComponent =
  InjectionProtocolComponent
    { name           :: Text
    , interface      :: Maybe Text
    , implementation :: Text
    } deriving (Eq,Show,Generic)

instance ToJSON InjectionProtocolComponent where
  toJSON (InjectionProtocolComponent name' interface' implementation') =
    object $
      [ "name"           .= name'
      , "implementation" .= implementation'
      ] ++ catMaybes ["interface" .=? interface']

instance FromJSON InjectionProtocolComponent where
  parseJSON = withObject "InjectionProtocolComponent" $ \o ->
    InjectionProtocolComponent <$> o .:  "name"
                               <*> o .:? "interface"
                               <*> o .:  "implementation"

