{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings     #-}

module Tezos.Client.Types.NetworkLog where

import Data.Aeson
  (FromJSON(parseJSON), ToJSON(toJSON), (.=), (.:), (.:?), object, withObject)
import Data.Aeson.Types (Parser)
import Data.Maybe (catMaybes)
import Data.Text (Text)
import GHC.Generics (Generic)
import Tezos.Client.Types.Core
import Tezos.Client.Types.Crypto
import Tezos.Client.Extension.Aeson ((.=?))

-- { "event": "too_few_connections" }
-- || { "event": "too_many_connections" }
-- || { "event": "new_point",
--      "point": $unistring }
-- || { "event": "new_peer",
--      "peer_id": $Crypto_box.Public_key_hash }
-- || { "event": "incoming_connection",
--      "point": $unistring }
-- || { "event": "outgoing_connection",
--      "point": $unistring }
-- || { "event": "authentication_failed",
--      "point": $unistring }
-- || { "event": "accepting_request",
--      "point": $unistring,
--      "id_point": { "addr": $unistring,
--                    "port"?: integer ∈ [0, 2^16-1] },
--      "peer_id": $Crypto_box.Public_key_hash }
-- || { "event": "rejecting_request",
--      "point": $unistring,
--      "id_point": { "addr": $unistring,
--                    "port"?: integer ∈ [0, 2^16-1] },
--      "peer_id": $Crypto_box.Public_key_hash }
-- || { "event": "request_rejected",
--      "point": $unistring,
--      "identity"?:
--        [ { "addr": $unistring,
--            "port"?: integer ∈ [0, 2^16-1] },
--          $Crypto_box.Public_key_hash ] }
-- || { "event": "connection_established",
--      "id_point": { "addr": $unistring,
--                    "port"?: integer ∈ [0, 2^16-1] },
--      "peer_id": $Crypto_box.Public_key_hash }
-- || { "event": "disconnection",
--      "peer_id": $Crypto_box.Public_key_hash }
-- || { "event": "external_disconnection",
--      "peer_id": $Crypto_box.Public_key_hash }
-- || { "event": "gc_points" }
-- || { "event": "gc_peer_ids" }
-- || { "event": "swap_request_received",
--      "source": $Crypto_box.Public_key_hash }
-- || { "event": "swap_ack_received",
--      "source": $Crypto_box.Public_key_hash }
-- || { "event": "swap_request_sent",
--      "source": $Crypto_box.Public_key_hash }
-- || { "event": "swap_ack_sent",
--      "source": $Crypto_box.Public_key_hash }
-- || { "event": "swap_request_ignored",
--      "source": $Crypto_box.Public_key_hash }
-- || { "event": "swap_success",
--      "source": $Crypto_box.Public_key_hash }
-- || { "event": "swap_failure",
--      "source": $Crypto_box.Public_key_hash }

data NetworkLog
  = TooFewConnections
  | TooManyConnections
  | NewPoint Unistring
  | NewPeer PublicKeyHash
  | IncomingConnection Unistring
  | OutgoingConnection Unistring
  | AuthenticationFailed Unistring
  | AcceptingRequest IdPoint PublicKeyHash
  | RejectingRequest IdPoint PublicKeyHash
  | RequestRejected (Maybe [(IdPoint, PublicKeyHash)])
  | ConnectionEstablished IdPoint PublicKeyHash
  | Disconnection PublicKeyHash
  | ExternalDisconnection PublicKeyHash
  | GcPoints
  | GcPeerIds
  | SwapRequestReceived PublicKeyHash
  | SwapAckReceived PublicKeyHash
  | SwapRequestSent PublicKeyHash
  | SwapAckSent PublicKeyHash
  | SwapRequestIgnored PublicKeyHash
  | SwapSuccess PublicKeyHash
  | SwapFailure PublicKeyHash
  deriving (Eq, Show, Generic)

instance ToJSON NetworkLog where
  toJSON TooFewConnections  = object ["event" .= ("too_few_connections" :: Text)]
  toJSON TooManyConnections = object ["event" .= ("too_many_connections" :: Text)]
  toJSON (NewPoint point')  = object  ["event" .= ("new_point" :: Text), "point" .= point' ]
  toJSON (NewPeer peerId')  = object  ["event" .= ("new_peer" :: Text), "peer_id" .= peerId' ]
  toJSON (IncomingConnection point') = object  ["event" .= ("incoming_connection" :: Text), "point" .= point' ]
  toJSON (OutgoingConnection point') = object  ["event" .= ("outgoing_connection" :: Text), "point" .= point' ]
  toJSON (AuthenticationFailed point') = object  ["event" .= ("authentication_failed" :: Text), "point" .= point' ]
  toJSON (AcceptingRequest idPoint' peerId') =
    object
      [ "event" .= ("accepting_request" :: Text)
      , "id_point" .= idPoint'
      , "peer_id" .= peerId'
      ]
  toJSON (RejectingRequest idPoint' peerId') =
    object
      [ "event" .= ("rejecting_request" :: Text)
      , "id_point" .= idPoint'
      , "peer_id" .= peerId'
      ]
  toJSON (RequestRejected identity') =
    object $
      [ "event" .= ("request_rejected" :: Text)
      ] ++ catMaybes ["identity" .=? identity']
  toJSON (ConnectionEstablished idPoint' peerId') =
    object
      [ "event" .= ("connection_established" :: Text)
      , "id_point" .= idPoint'
      , "peer_id" .= peerId'
      ]
  toJSON (Disconnection peerId') =
    object
      [ "event" .= ("disconnection" :: Text)
      , "peer_id" .= peerId'
      ]
  toJSON (ExternalDisconnection peerId') =
    object
      [ "event" .= ("external_disconnection" :: Text)
      , "peer_id" .= peerId'
      ]
  toJSON GcPoints = object ["event" .= ("gc_points" :: Text)]
  toJSON GcPeerIds = object ["event" .= ("gc_peer_ids" :: Text)]
  toJSON (SwapRequestReceived source') = object ["event" .= ("swap_request_received" :: Text), "source" .= source']
  toJSON (SwapAckReceived source') = object ["event" .= ("swap_ack_received" :: Text), "source" .= source']
  toJSON (SwapRequestSent source') = object ["event" .= ("swap_request_sent" :: Text), "source" .= source']
  toJSON (SwapAckSent source') = object ["event" .= ("swap_ack_sent" :: Text), "source" .= source']
  toJSON (SwapRequestIgnored source') = object ["event" .= ("swap_request_ignored" :: Text), "source" .= source']
  toJSON (SwapSuccess source') = object ["event" .= ("swap_success" :: Text), "source" .= source']
  toJSON (SwapFailure source') = object ["event" .= ("swap_failure" :: Text), "source" .= source']
  
instance FromJSON NetworkLog where
  parseJSON = withObject "NetworkLog" $ \o -> do
    let match s =
          case s of
            "too_few_connections" -> pure TooFewConnections
            "too_many_connections" -> pure TooManyConnections
            "new_point" -> NewPoint <$> o .: "point"
            "new_peer" -> NewPeer <$> o .: "peer_id"
            "incoming_connection" -> IncomingConnection <$> o .: "point"
            "outgoing_connection" -> OutgoingConnection <$> o .: "point"
            "authentication_failed" -> AuthenticationFailed <$> o .: "point"
            "accepting_request" -> AcceptingRequest <$> o .: "id_point" <*> o .: "peer_id"
            "rejecting_request" -> RejectingRequest <$> o .: "id_point" <*> o .: "peer_id"
            "request_rejected" -> RequestRejected <$> o .:? "identity"
            "connection_established" -> ConnectionEstablished <$> o .: "id_point" <*> o .: "peer_id"
            "disconnection" -> Disconnection <$> o .: "peer_id"
            "external_disconnection" -> ExternalDisconnection <$> o .: "peer_id"
            "gc_points" -> pure GcPoints
            "gc_peer_ids" -> pure GcPeerIds 
            "swap_request_received" -> SwapRequestReceived <$> o .: "source"
            "swap_ack_received" -> SwapAckReceived <$> o .: "source"
            "swap_request_sent" -> SwapRequestSent <$> o .: "source"
            "swap_ack_sent" -> SwapAckSent <$> o .: "source"
            "swap_request_ignored" -> SwapRequestIgnored <$> o .: "source"
            "swap_success" -> SwapSuccess <$> o .: "source"
            "swap_failure" -> SwapFailure <$> o .: "source"
            _ -> fail $ "expected NetworkLog, encountered " ++ (show s)
    s <- (o .: "event") :: Parser Text
    match s
