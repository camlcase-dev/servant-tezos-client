{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings     #-}

module Tezos.Client.Types.Prevalidator where

import Data.Aeson
  (FromJSON(parseJSON), ToJSON(toJSON), Value, (.=), (.:), (.:?), object,
   withObject)
import Data.Aeson.Types (Parser)
import qualified Data.HashMap.Lazy as HM
import Data.Maybe (catMaybes)
import Data.Text (Text)
import GHC.Generics (Generic)
import Tezos.Client.Types.BlockValidator
import Tezos.Client.Types.Core
import Tezos.Client.Types.Crypto
import Tezos.Client.Extension.Aeson ((.=?))

-- { "status":
--     { "phase": "launching",
--       "since": $timestamp }
--     || { "phase": "running",
--          "since": $timestamp }
--     || { "phase": "closing",
--          "birth": $timestamp,
--          "since": $timestamp }
--     || { "phase": "closed",
--          "birth": $timestamp,
--          "since": $timestamp }
--     || { "phase": "crashed",
--          "birth": $timestamp,
--          "since": $timestamp,
--          "errors": $error },
--   "pending_requests":
--     [ { "pushed": $timestamp,
--         "request":
--           { "request": "flush",
--             "block": $block_hash }
--           || { "request": "notify",
--                "peer": $Crypto_box.Public_key_hash,
--                "mempool":
--                  { "known_valid": [ $Operation_hash ... ],
--                    "pending": [ $Operation_hash ... ] } }
--           || { "request": "inject",
--                "operation":
--                  { "branch": $block_hash,
--                    "data": /^[a-zA-Z0-9]+$/ } }
--           || { "request": "arrived",
--                "operation_hash": $Operation_hash,
--                "operation":
--                  { "branch": $block_hash,
--                    "data": /^[a-zA-Z0-9]+$/ } }
--           || { "request": "advertise" } } ... ],
--   "backlog":
--     [ { "level":
--           "info" | "debug" | "error" | "fatal" | "warning" | "notice",
--         "events":
--           [ { "message": $unistring }
--             || { "request":
--                    { "request": "flush",
--                      "block": $block_hash }
--                    || { "request": "notify",
--                         "peer": $Crypto_box.Public_key_hash,
--                         "mempool":
--                           { "known_valid": [ $Operation_hash ... ],
--                             "pending": [ $Operation_hash ... ] } }
--                    || { "request": "inject",
--                         "operation":
--                           { "branch": $block_hash,
--                             "data": /^[a-zA-Z0-9]+$/ } }
--                    || { "request": "arrived",
--                         "operation_hash": $Operation_hash,
--                         "operation":
--                           { "branch": $block_hash,
--                             "data": /^[a-zA-Z0-9]+$/ } }
--                    || { "request": "advertise" },
--                  "status":
--                    { "pushed": $timestamp,
--                      "treated": $timestamp,
--                      "completed": $timestamp } }
--             || { "error": $error,
--                  "failed_request":
--                    { "request": "flush",
--                      "block": $block_hash }
--                    || { "request": "notify",
--                         "peer": $Crypto_box.Public_key_hash,
--                         "mempool":
--                           { "known_valid": [ $Operation_hash ... ],
--                             "pending": [ $Operation_hash ... ] } }
--                    || { "request": "inject",
--                         "operation":
--                           { "branch": $block_hash,
--                             "data": /^[a-zA-Z0-9]+$/ } }
--                    || { "request": "arrived",
--                         "operation_hash": $Operation_hash,
--                         "operation":
--                           { "branch": $block_hash,
--                             "data": /^[a-zA-Z0-9]+$/ } }
--                    || { "request": "advertise" },
--                  "status":
--                    { "pushed": $timestamp,
--                      "treated": $timestamp,
--                      "completed": $timestamp } } ... ] } ... ],
--   "current_request"?:
--     { "pushed": $timestamp,
--       "treated": $timestamp,
--       "request":
--         { "request": "flush",
--           "block": $block_hash }
--         || { "request": "notify",
--              "peer": $Crypto_box.Public_key_hash,
--              "mempool":
--                { "known_valid": [ $Operation_hash ... ],
--                  "pending": [ $Operation_hash ... ] } }
--         || { "request": "inject",
--              "operation":
--                { "branch": $block_hash,
--                  "data": /^[a-zA-Z0-9]+$/ } }
--         || { "request": "arrived",
--              "operation_hash": $Operation_hash,
--              "operation":
--                { "branch": $block_hash,
--                  "data": /^[a-zA-Z0-9]+$/ } }
--         || { "request": "advertise" } } }

data Prevalidator =
  Prevalidator
    { status          :: BlockValidatorStatus
    , pendingRequests :: [PrevalidatorPendingRequest]
    , backlog         :: [PrevalidatorBacklog]
    , currentRequests :: Maybe PrevalidatorCurrentRequest
    } deriving (Eq, Show, Generic)

instance ToJSON Prevalidator where
  toJSON (Prevalidator status' pendingRequests' backlog' currentRequests') =
    object $
      [ "status"           .= status'
      , "pending_requests" .= pendingRequests'
      , "backlog"          .= backlog'
      ] ++ catMaybes
      [ "current_requests" .=? currentRequests'
      ]

instance FromJSON Prevalidator where
  parseJSON = withObject "Prevalidator" $ \o ->
    Prevalidator <$> o .:  "status"
                 <*> o .:  "pending_requests"
                 <*> o .:  "backlog"
                 <*> o .:? "current_requests"

data PrevalidatorPendingRequest =
  PrevalidatorPendingRequest
    { pushed :: Timestamp
    , request :: PrevalidatorRequest
    } deriving (Eq, Show, Generic)

instance ToJSON PrevalidatorPendingRequest
instance FromJSON PrevalidatorPendingRequest

data PrevalidatorBacklogEvent
  = PBMessage { message :: Text }
  | PBRequest { request :: PrevalidatorRequest, status :: BacklogStatus}
  | PBFailedRequest { request :: PrevalidatorRequest, status :: BacklogStatus, error :: Value }
  deriving (Eq, Show, Generic)
  
instance ToJSON PrevalidatorBacklogEvent where
  toJSON (PBMessage message') = object [ "message" .= message' ]
  toJSON (PBRequest request' status') = object ["request" .= request', "status" .= status']
  toJSON (PBFailedRequest request' status' errors') = object ["failed_request" .= request', "status" .= status', "errors" .= errors']
 
instance FromJSON PrevalidatorBacklogEvent where
  parseJSON = withObject "PrevalidatorBacklogEvent" $ \o -> do
    let hasMessage       = HM.member "message" o
        hasRequest       = HM.member "request" o
        hasFailedRequest = HM.member "failed_request" o
        parsePrevalidatorBacklogEvent
          | hasMessage       = PBMessage       <$> o .: "message"
          | hasRequest       = PBRequest       <$> o .: "request" <*> o .: "status"
          | hasFailedRequest = PBFailedRequest <$> o .: "failed_request" <*> o .: "status" <*> o .: "error"
          | otherwise = fail $ "expected PrevalidatorBacklogEvent, encountered " ++ (show o)
    parsePrevalidatorBacklogEvent


-- "request":
--   { "request": "flush",
--     "block": $block_hash }
--   || { "request": "notify",
--        "peer": $Crypto_box.Public_key_hash,
--        "mempool":
--          { "known_valid": [ $Operation_hash ... ],
--            "pending": [ $Operation_hash ... ] } }
--   || { "request": "inject",
--        "operation":
--          { "branch": $block_hash,
--            "data": /^[a-zA-Z0-9]+$/ } }
--   || { "request": "arrived",
--        "operation_hash": $Operation_hash,
--        "operation":
--          { "branch": $block_hash,
--            "data": /^[a-zA-Z0-9]+$/ } }
--   || { "request": "advertise" } } ... ]

data PrevalidatorRequest
  = PRFlush   { block :: BlockHash }
  | PRNotify  { peer :: PublicKeyHash, mempool :: Mempool }
  | PRInject  { operation :: OperationData }
  | PRArrived { operationHash :: OperationHash, operation :: OperationData }
  | PRAdvertise 
  deriving (Eq, Show, Generic)

instance ToJSON PrevalidatorRequest where
  toJSON (PRFlush block') = object [ "request" .= ("flush" :: Text), "block" .= block' ]
  toJSON (PRNotify peer' mempool') = object [ "request" .= ("notify" :: Text), "peer" .= peer', "mempool" .= mempool' ]
  toJSON (PRInject operation') = object [ "request" .= ("inject" :: Text), "operation" .= operation' ]
  toJSON (PRArrived operationHash' operation') = object [ "request" .= ("arrived" :: Text), "operation_hash" .= operationHash', "operation" .= operation' ]
  toJSON PRAdvertise = object [ "request" .= ("advertise" :: Text) ]

instance FromJSON PrevalidatorRequest where
  parseJSON = withObject "PrevalidatorRequest" $ \o -> do
    let match s =
          case s of
            "flush"     -> PRFlush <$> o .: "block"
            "notify"    -> PRNotify <$> o .: "peer" <*> o .: "mempool"
            "inject"    -> PRInject <$> o .: "operation"
            "arrived"   -> PRArrived <$> o .: "operation_hash" <*> o .: "operation"
            "advertise" -> pure PRAdvertise
            _ -> fail $ "expected PrevalidatorRequest, encountered " ++ (show s)
    request' <- (o .: "request") :: Parser Text
    match request'

data PrevalidatorBacklog =
  PrevalidatorBacklog
    { level  :: Unistring
    , events :: [PrevalidatorBacklogEvent]
    } deriving (Eq, Show, Generic)

instance ToJSON PrevalidatorBacklog
instance FromJSON PrevalidatorBacklog

data PrevalidatorCurrentRequest =
  PrevalidatorCurrentRequest
    { pushed  :: Timestamp
    , treated :: Timestamp
    , request :: PrevalidatorRequest
    } deriving (Eq, Show, Generic)

instance ToJSON PrevalidatorCurrentRequest
instance FromJSON PrevalidatorCurrentRequest

-- "mempool":
--   { "known_valid": [ $Operation_hash ... ],
--     "pending": [ $Operation_hash ... ] }

data Mempool =
  Mempool
    { knownValid :: [OperationHash]
    , pending    :: [OperationHash]
    } deriving (Eq, Show, Generic)

instance ToJSON Mempool where
  toJSON (Mempool knownValid' pending') =
    object
      [ "known_valid" .= knownValid'
      , "pending"  .= pending'
      ]

instance FromJSON Mempool where
  parseJSON = withObject "Mempool" $ \o ->
    Mempool <$> o .: "known_valid"
            <*> o .: "pending"

data OperationData =
  OperationData
    { branch :: BlockHash
    , operationData :: Text
    } deriving (Eq, Show, Generic)

instance ToJSON OperationData where
  toJSON (OperationData branch' operationData') =
    object
      [ "branch" .= branch'
      , "operation_data" .= operationData'
      ]

instance FromJSON OperationData where
  parseJSON = withObject "OperationData" $ \o ->
    OperationData <$> o .: "branch"
                  <*> o .: "operation_data"
