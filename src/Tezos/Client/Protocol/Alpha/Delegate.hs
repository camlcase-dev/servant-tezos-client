{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeOperators #-}

module Tezos.Client.Protocol.Alpha.Delegate where

import Data.Int (Int64)
import Servant.API
import Tezos.Client.Types

-- | Lists all registered delegates.
--
-- GET ../<block_id>/context/delegates?[active]&[inactive]
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-block-id-context-delegates
type GetDelegates
  = Capture "block-id" BlockId
  :> "context"
  :> "delegates"
  :> QueryFlag "active"
  :> QueryFlag "inactive"
  :> Get '[JSON] [PublicKeyHash]
  

-- | Everything about a delegate.
--
-- GET ../<block_id>/context/delegates/<pkh>
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-block-id-context-delegates-pkh
type GetDelegate
  = Capture "block-id" BlockId
  :> "context"
  :> "delegates"
  :> Capture "pkh" Pkh
  :> Get '[JSON] DelegateInfo



-- | Returns the full balance of a given delegate, including the frozen balances.
--
-- GET ../<block_id>/context/delegates/<pkh>/balance
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-block-id-context-delegates-pkh-balance
type GetDelegateBalance
  = Capture "block-id" BlockId
  :> "context"
  :> "delegates"
  :> Capture "pkh" Pkh
  :> "balance"
  :> Get '[JSON] Mutez


-- | Tells whether the delegate is currently tagged as deactivated or not.
--
-- GET ../<block_id>/context/delegates/<pkh>/deactivated
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-block-id-context-delegates-pkh-deactivated
type GetDelegateDeactivated
  = Capture "block-id" BlockId
  :> "context"
  :> "delegates"
  :> Capture "pkh" Pkh
  :> "deactivated"
  :> Get '[JSON] Bool

-- | Returns the balances of all the contracts that delegate to a given
-- delegate. This excludes the delegate's own balance and its frozen balances.
--
-- GET ../<block_id>/context/delegates/<pkh>/delegated_balance
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-block-id-context-delegates-pkh-delegated-balance
type GetDelegateDelegatedBalance
  = Capture "block-id" BlockId
  :> "context"
  :> "delegates"
  :> Capture "pkh" Pkh
  :> "delegated_balance"
  :> Get '[JSON] Mutez

-- | Returns the list of contracts that delegate to a given delegate.
--
-- GET ../<block_id>/context/delegates/<pkh>/delegated_contracts
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-block-id-context-delegates-pkh-delegated-contracts
type GetDelegateDelegatedContracts
  = Capture "block-id" BlockId
  :> "context"
  :> "delegates"
  :> Capture "pkh" Pkh
  :> "delegated_contracts"
  :> Get '[JSON] [ContractHash]

-- | Returns the total frozen balances of a given delegate, this includes the
-- frozen deposits, rewards and fees.
--
-- GET ../<block_id>/context/delegates/<pkh>/frozen_balance
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-block-id-context-delegates-pkh-frozen-balance
type GetDelegateFrozenBalance
  = Capture "block-id" BlockId
  :> "context"
  :> "delegates"
  :> Capture "pkh" Pkh
  :> "frozen_balance"
  :> Get '[JSON] Mutez

-- | Returns the frozen balances of a given delegate, indexed by the cycle by
-- which it will be unfrozen
--
-- GET ../<block_id>/context/delegates/<pkh>/frozen_balance_by_cycle
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-block-id-context-delegates-pkh-frozen-balance-by-cycle
type GetDelegateFrozenBalanceByCycle
  = Capture "block-id" BlockId
  :> "context"
  :> "delegates"
  :> Capture "pkh" Pkh
  :> "frozen_balance_by_cycle"
  :> Get '[JSON] [Balance]

-- | Returns the cycle by the end of which the delegate might be deactivated
-- if she fails to execute any delegate action. A deactivated delegate might
-- be reactivated (without loosing any rolls) by simply re-registering as a
-- delegate. For deactivated delegates, this value contains the cycle by
-- which they were deactivated.
--
-- GET ../<block_id>/context/delegates/<pkh>/grace_period
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-block-id-context-delegates-pkh-grace-period
type GetDelegateGracePeriod
  = Capture "block-id" BlockId
  :> "context"
  :> "delegates"
  :> Capture "pkh" Pkh
  :> "grace_period"
  :> Get '[JSON] Int64

-- | Returns the total amount of tokens delegated to a given delegate. This
-- includes the balances of all the contracts that delegate to it, but also the
-- balance of the delegate itself and its frozen fees and deposits. The rewards
-- do not count in the delegated balance until they are unfrozen.
--
-- GET ../<block_id>/context/delegates/<pkh>/staking_balance
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-block-id-context-delegates-pkh-staking-balance
type GetDelegateStakingBalance
  = Capture "block-id" BlockId
  :> "context"
  :> "delegates"
  :> Capture "pkh" Pkh
  :> "staking_balance"
  :> Get '[JSON] Mutez
