{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeOperators #-}

module Tezos.Client.Protocol.Alpha.Context where

import Data.Aeson (Value)
import Data.Text (Text)
import Servant.API
import Tezos.Client.Types
import Tezos.Client.Extension.Servant (RealJSON)

-- | All constants
--
-- GET ../<block_id>/context/constants
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-block-id-context-constants
type GetBlockContextConstants
  = "chains"
  :> Capture "chain-id" ChainId
  :> "blocks"
  :> Capture "block-id" BlockId
  :> "context"
  :> "contents"
  :> Get '[JSON] BlockConstants

-- | Schema for all the RPC errors from this protocol version
--
-- GET ../<block_id>/context/constants/errors
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-block-id-context-constants-errors  
type GetBlockContextConstantsErrors
  = "chains"
  :> Capture "chain-id" ChainId
  :> "blocks"
  :> Capture "block-id" BlockId
  :> "context"
  :> "contents"
  :> "errors"
  :> Get '[JSON] Value

-- | All existing contracts (including non-empty default contracts).
--
-- GET ../<block_id>/context/contracts/<contract_id>/storage
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-block-id-context-contracts
type GetBlockContextContracts
  = "chains"
  :> Capture "chain-id" ChainId
  :> "blocks"
  :> Capture "block-id" BlockId
  :> "context"
  :> "contracts"
  :> Get '[JSON] [ContractId]

-- | Access the complete status of a contract.
--
-- GET ../<block_id>/context/contracts/<contract_id>
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-block-id-context-contracts-contract-id
type GetBlockContextContract
  = "chains"
  :> Capture "chain-id" ChainId
  :> "blocks"
  :> Capture "block-id" BlockId
  :> "context"
  :> "contracts"
  :> Capture "contract-id" ContractId
  :> Get '[JSON] ContractStatus

-- | Access the balance of a contract.
--
-- GET ../<block_id>/context/contracts/<contract_id>/balance
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-block-id-context-contracts-contract-id-balance
type GetBlockContextContractBalance
  = "chains"
  :> Capture "chain-id" ChainId
  :> "blocks"
  :> Capture "block-id" BlockId
  :> "context"
  :> "contracts"
  :> Capture "contract-id" ContractId
  :> "balance"
  :> Get '[JSON] Mutez

-- | Access the value associated with a key in the big map storage of the contract.
--
-- POST ../<block_id>/context/contracts/<contract_id>/big_map_get
-- http://tezos.gitlab.io/mainnet/api/rpc.html#post-block-id-context-contracts-contract-id-big-map-get
type PostContractBigMap
  = "chains"
  :> Capture "chain-id" ChainId
  :> "blocks"
  :> Capture "block-id" BlockId
  :> "context"
  :> "contracts"
  :> Capture "contract-id" ContractId
  :> "big_map_get"
  :> ReqBody '[RealJSON] BigMap
  :> Post '[JSON] (Maybe Expression)

-- | Access the counter of a contract, if any.
--
-- GET ../<block_id>/context/contracts/<contract_id>/counter
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-block-id-context-contracts-contract-id-counter
type GetContractCounter
  = "chains"
  :> Capture "chain-id" ChainId
  :> "blocks"
  :> Capture "block-id" BlockId
  :> "context"
  :> "contracts"
  :> Capture "contract-id" ContractId
  :> "counter"
  :> Get '[JSON] Integer

-- | Tells if the contract delegate can be changed.
--
-- GET ../<block_id>/context/contracts/<contract_id>/delegatable
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-block-id-context-contracts-contract-id-delegatable
type GetContractDelegatable
  = "chains"
  :> Capture "chain-id" ChainId
  :> "blocks"
  :> Capture "block-id" BlockId
  :> "context"
  :> "contracts"
  :> Capture "contract-id" ContractId
  :> "delegatable"
  :> Get '[JSON] Bool

-- | Access the delegate of a contract, if any.
--
-- GET ../<block_id>/context/contracts/<contract_id>/delegate
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-block-id-context-contracts-contract-id-delegate
type GetContractDelegate
  = "chains"
  :> Capture "chain-id" ChainId
  :> "blocks"
  :> Capture "block-id" BlockId
  :> "context"
  :> "contracts"
  :> Capture "contract-id" ContractId
  :> "delegate"
  :> Get '[JSON] PublicKeyHash

-- | Access the manager of a contract.
--
-- GET ../<block_id>/context/contracts/<contract_id>/manager
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-block-id-context-contracts-contract-id-manager
type GetContractManager
  = "chains"
  :> Capture "chain-id" ChainId
  :> "blocks"
  :> Capture "block-id" BlockId
  :> "context"
  :> "contracts"
  :> Capture "contract-id" ContractId
  :> "manager"
  :> Get '[JSON] PublicKeyHash

-- | Access the manager of a contract.
--
-- GET ../<block_id>/context/contracts/<contract_id>/manager
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-block-id-context-contracts-contract-id-manager
type GetContractManagerKey
  = "chains"
  :> Capture "chain-id" ChainId
  :> "blocks"
  :> Capture "block-id" BlockId
  :> "context"
  :> "contracts"
  :> Capture "contract-id" ContractId
  :> "manager_key"
  :> Get '[JSON] ManagerKey

-- | Access the code and data of the contract.
--
-- GET ../<block_id>/context/contracts/<contract_id>/script
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-block-id-context-contracts-contract-id-script
type GetContractScript
  = "chains"
  :> Capture "chain-id" ChainId
  :> "blocks"
  :> Capture "block-id" BlockId
  :> "context"
  :> "contracts"
  :> Capture "contract-id" ContractId
  :> "script"
  :> Get '[JSON] Contract

-- | Tells if the contract tokens can be spent by the manager.
--
-- GET ../<block_id>/context/contracts/<contract_id>/spendable
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-block-id-context-contracts-contract-id-spendable
type GetContractSpendable
  = "chains"
  :> Capture "chain-id" ChainId
  :> "blocks"
  :> Capture "block-id" BlockId
  :> "context"
  :> "contracts"
  :> Capture "contract-id" ContractId
  :> "spendable"
  :> Get '[JSON] Bool


-- | Access the data of the contract.
--
-- GET ../<block_id>/context/contracts/<contract_id>/storage
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-block-id-context-contracts-contract-id-storage
type GetContractStorage
  = "chains"
  :> Capture "chain-id" ChainId
  :> "blocks"
  :> Capture "block-id" BlockId
  :> "context"
  :> "contracts"
  :> Capture "contract-id" ContractId
  :> "storage"
  :> Get '[JSON] Expression

-- | Info about the nonce of a previous block.
--
-- GET ../<block_id>/context/nonces/<block_level>
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-block-id-context-nonces-block-level
type GetNonce
  = "chains"
  :> Capture "chain-id" ChainId
  :> "blocks"
  :> Capture "block-id" BlockId
  :> "context"
  :> "nonces"
  :> Capture "block-level" Int
  :> Get '[JSON] Nonce

-- | Returns the raw context.
--
-- GET ../<block_id>/context/raw/bytes?[depth=<int>]
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-block-id-context-raw-bytes
type GetRawContext
  = "chains"
  :> Capture "chain-id" ChainId
  :> "blocks"
  :> Capture "block-id" BlockId
  :> "context"
  :> "raw"
  :> "bytes"
  :> QueryParam "depth" Int
  :> Get '[JSON] Text

-- | Seed of the cycle to which the block belongs.
--
-- POST ../<block_id>/context/seed
-- http://tezos.gitlab.io/mainnet/api/rpc.html#post-block-id-context-seed
type PostSeed
  = "chains"
  :> Capture "chain-id" ChainId
  :> "blocks"
  :> Capture "block-id" BlockId
  :> "context"
  :> "seed"
  :> ReqBody '[RealJSON] NoContent
  :> Post '[JSON] Text
