{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeOperators #-}

module Tezos.Client.Protocol.Alpha.Helpers where

import Data.Int (Int32)
import Data.Text (Text)
import Servant.API
import Tezos.Client.Types
import Tezos.Client.Extension.Servant (RealJSON)

-- | Retrieves the list of delegates allowed to bake a block. By default, it
-- gives the best baking priorities for bakers that have at least one
-- opportunity below the 64th priority for the next block. Parameters `level`
-- and `cycle` can be used to specify the (valid) level(s) in the past or future
-- at which the baking rights have to be returned. Parameter `delegate` can be
-- used to restrict the results to the given delegates. If parameter `all` is
-- set, all the baking opportunities for each baker at each level are returned,
-- instead of just the first one. Returns the list of baking slots. Also
-- returns the minimal timestamps that correspond to these slots. The timestamps
-- are omitted for levels in the past, and are only estimates for levels later
-- that the next block, based on the hypothesis that all predecessor blocks were
-- baked at the first priority.
--
-- GET ../<block_id>/helpers/baking_rights?(level=<block_level>)*&(cycle=<block_cycle>)*&(delegate=<pkh>)*&[max_priority=<int>]&[all]
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-block-id-helpers-baking-rights
type GetBakingRights
  = Capture "block-id" BlockId
  :> "helpers"
  :> "baking_rights"
  :> QueryParam "level" Int
  :> QueryParam "cycle" Int
  :> QueryParam "delegate" Pkh
  :> QueryParam "max-priority" Int
  :> Get '[JSON] [BakingRights]

-- | Try to complete a prefix of a Base58Check-encoded data. This RPC is
-- actually able to complete hashes of block, operations, public_keys
-- and contracts.
--
-- GET ../<block_id>/helpers/complete/<prefix>
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-block-id-helpers-complete-prefix
type GetTryComplete
  = Capture "block-id" BlockId
  :> "helpers"
  :> "complete"
  :> Get '[JSON] [Text]


-- |  Returns the level of the interrogated block, or the one of a block located
-- `offset` blocks after in the chain (or before when negative). For instance,
-- the next block if `offset` is 1.
--
-- GET ../<block_id>/helpers/current_level?[offset=<int32>]
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-block-id-helpers-current-level
type GetBlockLevel
  = Capture "block-id" BlockId
  :> "helpers"
  :> "current_level"
  :> QueryParam "offset" Int32
  :> Get '[JSON] BlockLevel


-- | Retrieves the delegates allowed to endorse a block. By default, it gives
-- the endorsement slots for delegates that have at least one in the next block.
-- Parameters `level` and `cycle` can be used to specify the (valid) level(s) in
-- the past or future at which the endorsement rights have to be returned.
-- Parameter `delegate` can be used to restrict the results to the given
-- delegates. Returns the list of endorsement slots. Also returns the minimal
-- timestamps that correspond to these slots. The timestamps are omitted for
-- levels in the past, and are only estimates for levels later that the next
-- block, based on the hypothesis that all predecessor blocks were baked at
-- the first priority.
--
-- GET ../<block_id>/helpers/endorsing_rights?(level=<block_level>)*&(cycle=<block_cycle>)*&(delegate=<pkh>)*
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-block-id-helpers-endorsing-rights
type GetPermittedDelegates
  = Capture "block-id" BlockId
  :> "helpers"
  :> "endorsing_rights"
  :> QueryParam "level" Int
  :> QueryParam "cycle" Int
  :> QueryParam "delegate" Pkh
  :> Get '[JSON] [PermittedDelegate]

-- | Forge an operation
--
-- POST ../<block_id>/helpers/forge/operations
-- http://tezos.gitlab.io/mainnet/api/rpc.html#post-block-id-helpers-forge-operations
type PostForgeOperations
  = Capture "block-id" BlockId
  :> "helpers"
  :> "forge"
  :> "operations"
  :> ReqBody '[RealJSON] UnsignedOperation
  :> Post '[JSON] Text

-- | Forge the protocol-specific part of a block header
--
-- POST ../<block_id>/helpers/forge/protocol_data
-- http://tezos.gitlab.io/mainnet/api/rpc.html#post-block-id-helpers-forge-protocol-data
type PostForgeProtocolData
  = Capture "block-id" BlockId
  :> "helpers"
  :> "forge"
  :> "protocol_data"
  :> ReqBody '[RealJSON] ForgeProtocolData
  :> Post '[JSON] ForgedProtocolData

-- | Forge a block header
--
-- POST ../<block_id>/helpers/forge_block_header
-- http://tezos.gitlab.io/mainnet/api/rpc.html#post-block-id-helpers-forge-block-header
type PostForgeBlockHeader
  = Capture "block-id" BlockId
  :> "helpers"
  :> "forge_block_header"
  :> ReqBody '[RealJSON] ForgeBlockHeader
  :> Post '[JSON] ForgedBlockHeader

-- | Levels of a cycle
--
-- GET ../<block_id>/helpers/levels_in_current_cycle?[offset=<int32>]
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-block-id-helpers-levels-in-current-cycle
type GetLevelsInCurrentCycle
  = Capture "block-id" BlockId
  :> "helpers"
  :> "levels_in_current_cycle"
  :> QueryParam "offset" Int32
  :> Get '[JSON] LevelsInCurrentCycle

-- | Parse a block
--
-- POST ../<block_id>/helpers/parse/block
-- http://tezos.gitlab.io/mainnet/api/rpc.html#post-block-id-helpers-parse-block
type PostLevelsInCurrentCycle
  = Capture "block-id" BlockId
  :> "helpers"
  :> "parse"
  :> "block"
  :> ReqBody '[RealJSON] BlockHeaderShell
  :> Post '[JSON] SignedContents

-- | Parse operations
--
-- POST ../<block_id>/helpers/parse/operations
-- tezos.gitlab.io/mainnet/api/rpc.html#post-block-id-helpers-parse-operations
type PostParseOperations
  = Capture "block-id" BlockId
  :> "helpers"
  :> "parse"
  :> "operations"
  :> ReqBody '[RealJSON] ParseOperationsRequest
  :> Post '[JSON] [ParseOperationsResponse]

-- | Simulate the validation of a block that would contain the given operations
-- and return the resulting fitness and context hash.
--
-- POST ../<block_id>/helpers/preapply/block?[sort]&[timestamp=<date>]
-- http://tezos.gitlab.io/mainnet/api/rpc.html#post-block-id-helpers-preapply-block
type PostSimulateBlockValidation
  = Capture "block-id" BlockId
  :> "helpers"
  :> "preapply"
  :> "block"
  :> QueryFlag "sort"
  :> QueryParam "timestamp" Timestamp
  :> ReqBody '[RealJSON] SimulateBlockRequest
  :> Post '[JSON] SimulateBlockResponse

-- | Simulate the validation of an operation.
--
-- POST ../<block_id>/helpers/preapply/operations
-- http://tezos.gitlab.io/mainnet/api/rpc.html#post-block-id-helpers-preapply-operationsa
type PostSimulateOperationValidation
  = Capture "block-id" BlockId
  :> "helpers"
  :> "preapply"
  :> "operations"
  :> ReqBody '[RealJSON] [NextOperation]
  :> Post '[JSON] OperationWithMetadata

-- | Computes the serialized version of some data expression using the same
-- algorithm as script instruction PACK
--
-- POST ../<block_id>/helpers/scripts/pack_data
-- http://tezos.gitlab.io/mainnet/api/rpc.html#post-block-id-helpers-scripts-pack-data
type PostPackData
  = Capture "block-id" BlockId
  :> "helpers"
  :> "scripts"
  :> "pack_data"
  :> ReqBody '[RealJSON] PackDataRequest
  :> Post '[JSON] PackDataResponse

-- | Run a piece of code in the current context
--
-- POST ../<block_id>/helpers/scripts/run_code
-- http://tezos.gitlab.io/mainnet/api/rpc.html#post-block-id-helpers-scripts-run-code
type PostRunCode
  = Capture "block-id" BlockId
  :> "helpers"
  :> "scripts"
  :> "run_code"
  :> ReqBody '[RealJSON] RunCodeRequest
  :> Post '[JSON] RunCodeResponse

-- | Run an operation without signature checks
--
-- POST ../<block_id>/helpers/scripts/run_operation
-- http://tezos.gitlab.io/mainnet/api/rpc.html#post-block-id-helpers-scripts-run-operation
type PostRunOperation
  = Capture "block-id" BlockId
  :> "helpers"
  :> "scripts"
  :> "run_operation"
  :> ReqBody '[RealJSON] RunOperationRequest
  :> Post '[JSON] OperationWithMetadata

-- | Run a piece of code in the current context, keeping a trace
--
-- POST ../<block_id>/helpers/scripts/trace_code
-- http://tezos.gitlab.io/mainnet/api/rpc.html#post-block-id-helpers-scripts-trace-code
type PostRunTraceCode
  = Capture "block-id" BlockId
  :> "helpers"
  :> "scripts"
  :> "trace_code"
  :> ReqBody '[RealJSON] RunTraceRequest
  :> Post '[JSON] RunTraceResponse

-- | Typecheck a piece of code in the current context
--
-- POST ../<block_id>/helpers/scripts/typecheck_code
-- http://tezos.gitlab.io/mainnet/api/rpc.html#post-block-id-helpers-scripts-typecheck-code
type PostRunTypecheckCode
  = Capture "block-id" BlockId
  :> "helpers"
  :> "scripts"
  :> "typecheck_code"
  :> ReqBody '[RealJSON] TypecheckCodeRequest
  :> Post '[JSON] TypecheckCodeResponse

-- | Check that some data expression is well formed and of a given type in the current context
--
-- POST ../<block_id>/helpers/scripts/typecheck_data
-- http://tezos.gitlab.io/mainnet/api/rpc.html#post-block-id-helpers-scripts-typecheck-data
type PostRunTypecheckData
  = Capture "block-id" BlockId
  :> "helpers"
  :> "scripts"
  :> "typecheck_code"
  :> ReqBody '[RealJSON] TypecheckDataRequest
  :> Post '[JSON] Gas
