{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeOperators #-}

module Tezos.Client.Protocol.Alpha where

import Data.Int (Int64)
import Data.Text (Text)
import Servant.API
import Tezos.Client.Types

-- | All the information about a block.
--
-- GET ../<block_id>
-- https://tezos.gitlab.io/007/rpc.html#get-block-id
type GetBlock
  =  "chains"
  :> Capture "chain-id" ChainId
  :> "blocks"
  :> Capture "block-id" BlockId
  :> Get '[JSON] Block


-- | Access the value associated with a key in a big map.
--
-- GET ../<block_id>/context/big_maps/<big_map_id>/<script_expr>
-- https://tezos.gitlab.io/007/rpc.html#get-block-id-context-big-maps-big-map-id-script-expr
type GetBigMap
  =  "chains"
  :> Capture "chain-id" ChainId
  :> "blocks"
  :> Capture "block-id" BlockId
  :> "context"
  :> "big_maps"
  :> Capture "big-map-id" Integer
  :> Capture "script-expr" Text
  :> Get '[JSON] Expression

  
-- | The block's hash, its unique identifier.
--
-- GET ../<block_id>/hash
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-block-id-hash
type GetBlockHash
  = "chains"
  :> Capture "chain-id" ChainId
  :> "blocks"
  :> Capture "block-id" BlockId
  :> "hash"
  :> Get '[JSON] BlockHash

-- | The whole block header.
--
-- GET ../<block_id>/header
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-block-id-header
type GetBlockHeader
  = "chains"
  :> Capture "chain-id" ChainId
  :> "blocks"
  :> Capture "block-id" BlockId
  :> "header"
  :> Get '[JSON] BlockHeader


-- | The version-specific fragment of the block header.
--
-- GET ../<block_id>/header/protocol_data
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-block-id-header-protocol-data
type GetProtocolHeader
  = "chains"
  :> Capture "chain-id" ChainId
  :> "blocks"
  :> Capture "block-id" BlockId
  :> "header"
  :> "protocol_data"
  :> Get '[JSON] ProtocolData

-- | The version-specific fragment of the block header (unparsed).
--
-- GET ../<block_id>/header/protocol_data/raw
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-block-id-header-protocol-data-raw
type GetProtocolDataRaw
  = "chains"
  :> Capture "chain-id" ChainId
  :> "blocks"
  :> Capture "block-id" BlockId
  :> "header"
  :> "protocol_data"
  :> "raw"
  :> Get '[JSON] Text

-- | The whole block header (unparsed).
--
-- GET ../<block_id>/header/raw
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-block-id-header-raw
type GetHeaderRaw
  = "chains"
  :> Capture "chain-id" ChainId
  :> "blocks"
  :> Capture "block-id" BlockId
  :> "header"
  :> "raw"
  :> Get '[JSON] Text

-- | The shell-specific fragment of the block header.
--
-- GET ../<block_id>/header/shell
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-block-id-header-shell
type GetHeaderShell
  = "chains"
  :> Capture "chain-id" ChainId
  :> "blocks"
  :> Capture "block-id" BlockId
  :> "header"
  :> "shell"
  :> Get '[JSON] BlockHeaderShell


-- | List the ancestors of the given block which, if referred to as the branch
-- in an operation header, are recent enough for that operation to be included
-- in the current block.
--
-- GET ../<block_id>/live_blocks
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-block-id-live-blocks
type GetLiveBlocks
  = "chains"
  :> Capture "chain-id" ChainId
  :> "blocks"
  :> Capture "block-id" BlockId
  :> "live_blocks"
  :> Get '[JSON] [BlockHash]

-- All the metadata associated to the block.
--
-- GET ../<block_id>/metadata
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-block-id-metadata
type GetMetadata
  = "chains"
  :> Capture "chain-id" ChainId
  :> "blocks"
  :> Capture "block-id" BlockId
  :> "metadata"
  :> Get '[JSON] BlockHeaderMetadata

----
-- Operation Hashes
----

-- | The hashes of all the operations included in the block.
--
-- GET ../<block_id>/operation_hashes
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-block-id-operation-hashes
type GetOperationHashes
  = "chains"
  :> Capture "chain-id" ChainId
  :> "blocks"
  :> Capture "block-id" BlockId
  :> "operation_hashes"
  :> Get '[JSON] [[OperationHash]]

-- | All the operations included in `n-th` validation pass of the block.
--
-- GET ../<block_id>/operation_hashes/<list_offset>
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-block-id-operation-hashes-list-offset
type GetOperationHashesOffset
  = "chains"
  :> Capture "chain-id" ChainId
  :> "blocks"
  :> Capture "block-id" BlockId
  :> "operation_hashes"
  :> Capture "list_offset" Integer
  :> Get '[JSON] [OperationHash]


-- | The hash of then `m-th` operation in the `n-th` validation pass of the block.
--
-- GET ../<block_id>/operation_hashes/<list_offset>/<operation_offset>
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-block-id-operation-hashes-list-offset-operation-offset
type GetOperationHashesOffset2
  = "chains"
  :> Capture "chain-id" ChainId
  :> "blocks"
  :> Capture "block-id" BlockId
  :> "operation_hashes"
  :> Capture "list_offset" Integer
  :> Capture "operation_offset" Integer
  :> Get '[JSON] OperationHash


----
-- Operations
----

-- | All the operations included in the block.
--
-- GET ../<block_id>/operations
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-block-id-operations
type GetBlockOperations
  = "chains"
  :> Capture "chain-id" ChainId
  :> "blocks"
  :> Capture "block-id" BlockId
  :> "operations"
  :> Get '[JSON] [[Operation]]

-- | All the operations included in `n-th` validation pass of the block.
--
-- GET ../<block_id>/operations/<list_offset>
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-block-id-operations-list-offset
type GetBlockOperationsOffset
  = "chains"
  :> Capture "chain-id" ChainId
  :> "blocks"
  :> Capture "block-id" BlockId
  :> "operations"
  :> Capture "list_offset" Integer
  :> Get '[JSON] [Operation]


-- | The `m-th` operation in the `n-th` validation pass of the block.
--
-- GET ../<block_id>/operations/<list_offset>/<operation_offset>
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-block-id-operations-list-offset-operation-offset
type GetBlockOperationsOffset2
  = "chains"
  :> Capture "chain-id" ChainId
  :> "blocks"
  :> Capture "block-id" BlockId
  :> "operations"
  :> Capture "list_offset" Integer
  :> Capture "operation_offset" Integer
  :> Get '[JSON] Operation

----
-- Votes
----

-- | Ballots casted so far during a voting period.
--
-- GET ../<block_id>/votes/ballot_list
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-block-id-votes-ballot-list
type GetBallotList
  = "chains"
  :> Capture "chain-id" ChainId
  :> "blocks"
  :> Capture "block-id" BlockId
  :> "votes"
  :> "ballot_list"
  :> Get '[JSON] [Vote]

-- | Sum of ballots casted so far during a voting period.
--
-- GET ../<block_id>/votes/ballots
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-block-id-votes-ballots
type GetBallotCount
  = "chains"
  :> Capture "chain-id" ChainId
  :> "blocks"
  :> Capture "block-id" BlockId
  :> "votes"
  :> "ballots"
  :> Get '[JSON] BallotCount

-- | Current period kind.
--
-- GET ../<block_id>/votes/current_period_kind
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-block-id-votes-current-period-kind
type GetVotePeriodKind
  = "chains"
  :> Capture "chain-id" ChainId
  :> "blocks"
  :> Capture "block-id" BlockId
  :> "votes"
  :> "current_period_kind"
  :> Get '[JSON] VotingPeriodKind

-- | Current proposal under evaluation.
--
-- GET ../<block_id>/votes/current_proposal
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-block-id-votes-current-proposal
type GetCurrentProposal
  = "chains"
  :> Capture "chain-id" ChainId
  :> "blocks"
  :> Capture "block-id" BlockId
  :> "votes"
  :> "current_proposal"
  :> Get '[JSON] ProtocolHash

-- | Current expected quorum.
--
-- GET ../<block_id>/votes/current_quorum
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-block-id-votes-current-quorum
type GetCurrentQuorom
  = "chains"
  :> Capture "chain-id" ChainId
  :> "blocks"
  :> Capture "block-id" BlockId
  :> "votes"
  :> "current_quorom"
  :> Get '[JSON] Int64

-- | List of delegates with their voting weight, in number of rolls.
--
-- GET ../<block_id>/votes/listings
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-block-id-votes-listings
type GetDelegatesVotes
  = "chains"
  :> Capture "chain-id" ChainId
  :> "blocks"
  :> Capture "block-id" BlockId
  :> "votes"
  :> "listings"
  :> Get '[JSON] [DelegateWeight]

-- | List of proposals with number of supporters.
--
-- GET ../<block_id>/votes/proposals
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-block-id-votes-proposals
type GetProposals
  = "chains"
  :> Capture "chain-id" ChainId
  :> "blocks"
  :> Capture "block-id" BlockId
  :> "votes"
  :> "proposals"
  :> Get '[JSON] [(ProtocolHash, Int64)]
