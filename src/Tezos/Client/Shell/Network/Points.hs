{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeOperators #-}

module Tezos.Client.Shell.Network.Points where

import Servant.API
import Tezos.Client.Types
import Tezos.Client.Extension.Servant (RealJSON)

-- | List the pool of known `IP:port` used for establishing P2P connections.
--
-- GET /network/points?(filter=<p2p.point.state_filter>)*
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-network-points
type GetNetworkPoints
  = "network"
  :> "points"
  :> QueryParam "filter" P2PPointStateFilter
  :> Get '[JSON] [(Unistring, P2PPoint)]

-- | Details about a given `IP:addr`.
--
-- GET /network/points/<point>
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-network-points-point
type GetNetworkPoint
  = "network"
  :> "points"
  :> Capture "point" Point
  :> Get '[JSON] P2PPoint


-- | Connect to a peer.
--
-- PUT /network/points/<point>?[timeout=<float>]
-- http://tezos.gitlab.io/mainnet/api/rpc.html#put-network-points-point
type PutNetworkPoint
  = "network"
  :> "points"
  :> Capture "point" Point
  :> QueryParam "timeout" Double
  :> ReqBody '[RealJSON] NoContent
  :> Put '[JSON] NoContent

-- | Blacklist the given address and remove it from the whitelist if present.
--
-- GET /network/points/<point>/ban
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-network-points-point-ban
type GetNetworkPointBan
  = "network"
  :> "points"
  :> Capture "point" Point
  :> "ban"
  :> Get '[JSON] NoContent

-- | Check is a given address is blacklisted or greylisted.
--
-- GET /network/points/<point>/banned
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-network-points-point-banned
type GetNetworkPointBanned
  = "network"
  :> "points"
  :> Capture "point" Point
  :> "banned"
  :> Get '[JSON] Bool

-- | Monitor network events related to an `IP:addr`.
--
-- GET /network/points/<point>/log?[monitor]
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-network-points-point-log
type GetNetworkPointLog
  = "network"
  :> "points"
  :> Capture "point" Point
  :> "log"
  :> QueryFlag "monitor"
  :> Get '[JSON] [PointLog]

-- | Trust a given address permanently and remove it from the blacklist if
-- present. Connections from this address can still be closed on authentication
-- if the peer is greylisted.
--
-- GET /network/points/<point>/trust
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-network-points-point-trust
type GetNetworkPointTrust
  = "network"
  :> "points"
  :> Capture "point" Point
  :> "trust"
  :> Get '[JSON] NoContent

-- | Remove an address from the blacklist.
--
-- GET /network/points/<point>/unban
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-network-points-point-unban
type GetNetworkPointUnban
  = "network"
  :> "points"
  :> Capture "point" Point
  :> "unban"
  :> Get '[JSON] NoContent

-- | Remove an address from the whitelist.
--
-- GET /network/points/<point>/untrust
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-network-points-point-untrust
type GetNetworkPointUntrust
  = "network"
  :> "points"
  :> Capture "point" Point
  :> "untrust"
  :> Get '[JSON] NoContent
