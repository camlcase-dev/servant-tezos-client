{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeOperators #-}

module Tezos.Client.Shell.Network.Peers where

import Servant.API
import Tezos.Client.Types

-- | List the peers the node ever met.
--
-- GET /network/peers?(filter=<p2p.point.state_filter>)*
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-network-peers
type GetNetworkPeers
  =  "network"
  :> "peers"
  :> QueryParam "filter" P2PPointStateFilter
  :> Get '[JSON] [(PublicKeyHash, Peer)]

-- | Details about a given peer.
--
-- GET /network/peers/<peer_id>
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-network-peers-peer-id
type GetNetworkPeer
  = "network"
  :> "peers"
  :> Capture "peer_id" PeerId
  :> Get '[JSON] Peer

-- | Blacklist the given peer and remove it from the whitelist if present.
--
-- GET /network/peers/<peer_id>/banned
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-network-peers-peer-id-ban
type GetNetworkPeerBan
  = "network"
  :> "peers"
  :> Capture "peer_id" PeerId
  :> "ban" :> Get '[JSON] NoContent

-- | Check if a given peer is blacklisted or greylisted.
--
-- GET /network/peers/<peer_id>/banned
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-network-peers-peer-id-banned
type GetNetworkPeerBanned
  = "network"
  :> "peers"
  :> Capture "peer_id" PeerId
  :> "banned" :> Get '[JSON] Bool

-- | Monitor network events related to a given peer.
--
-- GET /network/peers/<peer_id>/log?[monitor]
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-network-peers-peer-id-log
type GetNetworkPeerLog
  = "network"
  :> "peers"
  :> Capture "peer_id" PeerId
  :> "log"
  :> QueryFlag "monitor"
  :> Get '[JSON] [PeerLog]

-- | Whitelist a given peer permanently and remove it from the blacklist if
-- present. The peer cannot be blocked (but its host IP still can).
--
-- GET /network/peers/<peer_id>/trust
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-network-peers-peer-id-trust
type GetNetworkPeerTrust
  = "network"
  :> "peers"
  :> Capture "peer_id" PeerId
  :> "trust"
  :> Get '[JSON] NoContent

-- | Remove the given peer from the blacklist.
--
-- GET /network/peers/<peer_id>/unban
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-network-peers-peer-id-unban
type GetNetworkPeerUnban
  = "network"
  :> "peers"
  :> Capture "peer_id" PeerId
  :> "unban"
  :> Get '[JSON] NoContent

-- | Remove a given peer from the whitelist.
--
-- GET /network/peers/<peer_id>/untrust
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-network-peers-peer-id-untrust
type GetNetworkPeerUntrust
  = "network"
  :> "network"
  :> "peers"
  :> Capture "peer_id" PeerId
  :> "untrust"
  :> Get '[JSON] NoContent
