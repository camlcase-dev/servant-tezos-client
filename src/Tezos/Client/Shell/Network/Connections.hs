{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeOperators #-}

module Tezos.Client.Shell.Network.Connections where

import Servant.API
import Tezos.Client.Types

-- | List the running P2P connection.
--
-- GET /network/connections
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-network-connections
type GetNetworkConnections
  =  "network"
  :> "connections"
  :> Get '[JSON] [P2PConnection]

-- | Details about the current P2P connection to the given peer.
--
-- GET /network/connections/<peer_id>
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-network-connections-peer-id
type GetNetworkConnectionPeer
  =  "network"
  :> "connections"
  :> Capture "peer_id" PeerId
  :> Get '[JSON] P2PConnection

-- | Forced close of the current P2P connection to the given peer.
--
-- DELETE /network/connections/<peer_id>?[wait]
-- http://tezos.gitlab.io/mainnet/api/rpc.html#delete-network-connections-peer-id
type DeleteNetworkConnectionPeer
  =  "network"
  :> "connections"
  :> QueryParam "peer_id" PeerId
  :> QueryFlag "wait"
  :> Delete '[JSON] NoContent
