{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeOperators #-}

module Tezos.Client.Shell.Monitor where

import Data.Text (Text)
import Servant.API
import Tezos.Client.Types

-- | Monitor every chain creation and destruction. Currently active chains will
-- be given as first elements
--
-- GET /monitor/active_chains
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-monitor-active-chains
type GetMonitorActiveChains
  = "monitor"
  :> "active_chains"
  :> Get '[JSON] [ActiveChain]

-- | Wait for the node to have synchronized its chain with a few peers
-- (configured by the node's administrator), streaming head updates that happen
-- during the bootstrapping process, and closing the stream at the end. If the
-- node was already bootstrapped, returns the current head immediately.
--
-- GET /monitor/bootstrapped
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-monitor-bootstrapped
type GetMonitorNodeBootstrapped
  = "monitor"
  :> "bootstrapped"
  :> Get '[JSON] Bootstrapped

-- | Get information on the build of the node.
--
-- GET /monitor/commit_hash
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-monitor-commit-hash
type GetMonitorCommitHash
  = "monitor"
  :> "commit_hash"
  :> Get '[JSON] Text

-- |  Monitor all blocks that are successfully validated by the node and
-- selected as the new head of the given chain.
--
-- GET /monitor/heads/<chain_id>?(next_protocol=<Protocol_hash>)*
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-monitor-heads-chain-id
type GetMonitorHeadsChain
  = "monitor"
  :> "heads"
  :> Capture "chain_id" ChainId
  :> QueryParam "next_protocol" ProtocolHash
  :> Get '[JSON] ChainHead

-- | Monitor all economic protocols that are retrieved and successfully loaded
-- and compiled by the node.
-- 
-- GET /monitor/protocols
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-monitor-protocols
type GetMonitorProtocols
  = "monitor"
  :> "protocols"
  :> Get '[JSON] ProtocolHash

-- | Monitor all blocks that are successfully validated by the node,
-- disregarding whether they were selected as the new head or not.
--
-- GET /monitor/valid_blocks?(protocol=<Protocol_hash>)*&(next_protocol=<Protocol_hash>)*&(chain=<chain_id>)*
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-monitor-valid-blocks
type GetMonitorValidBlocks
  = "monitor"
  :> "valid_blocks"
  :> QueryParam "protocol" ProtocolHash
  :> QueryParam "next_protocol" ProtocolHash
  :> QueryParam "chain_id" ChainId
  :> Get '[JSON] ValidBlock
