{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeOperators #-}

module Tezos.Client.Shell.Network
  ( module Tezos.Client.Shell.Network
  , module Tezos.Client.Shell.Network.Connections
  , module Tezos.Client.Shell.Network.Peers
  , module Tezos.Client.Shell.Network.Points
  ) where

import Data.Text (Text)
import Servant.API
import Tezos.Client.Types

import Tezos.Client.Shell.Network.Connections
import Tezos.Client.Shell.Network.Peers
import Tezos.Client.Shell.Network.Points

-- | Clear all greylists tables.
--
-- GET /network/greylist/clear
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-network-greylist-clear
type GetGreylistClear
  = "network"
  :> "greylist"
  :> "clear"
  :> Get '[JSON] NoContent

-- | Stream of all network events
--
-- GET /network/log
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-network-log
type GetNetworkLog
  = "network"
  :> "log"
  :> Get '[JSON] NetworkLog

-- | Return the node's peer id
--
--
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-network-self
type GetNetworkSelf
  = "network"
  :> "self"
  :> Get '[JSON] Text -- PublicKeyHash

-- | Global network bandwidth statistics in B/s.
--
-- GET /network/self
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-network-stat
type GetNetworkStat
  = "network"
  :> "stat"
  :> Get '[JSON] NetworkStat

-- | Supported network layer versions.
--
-- GET /network/version
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-network-version
type GetNetworkVersion
  = "network"
  :> "versions"
  :> Get '[JSON] AnnouncedVersion
