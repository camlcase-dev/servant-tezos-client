{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeOperators #-}

module Tezos.Client.Shell.Chain where

import Servant.API
import Tezos.Client.Types

-- |  Lists known heads of the blockchain sorted with decreasing fitness.
-- Optional arguments allows to returns the list of predecessors for known
-- heads or the list of predecessors for a given list of blocks.
--
-- GET /chains/<chain_id>/blocks?[length=<int>]&(head=<block_hash>)*&[min_date=<date>]
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-chains-chain-id-blocks
type GetBlockchainHeads
  =  "chains"
  :> Capture "chain_id" ChainId
  :> "blocks"
  :> QueryParam "length" Int
  :> QueryParam "head" BlockHash
  :> QueryParam "min_date" Timestamp
  :> Get '[JSON] [[BlockHash]]

-- | The chain unique identifier.
--
-- GET /chains/<chain_id>/chain_id
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-chains-chain-id-chain-id
type GetBlockchainId
  =  "chains"
  :> Capture "chain_id" ChainId
  :> "chain_id"
  :> Get '[JSON] ChainId

-- | Lists blocks that have been declared invalid along with the errors that
-- led to them being declared invalid.
--
-- GET /chains/<chain_id>/invalid_blocks
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-chains-chain-id-invalid-blocks
type GetInvalidBlocks
  =  "chains"
  :> Capture "chain_id" ChainId
  :> "invalid_blocks"
  :> Get '[JSON] [InvalidBlock]

-- | The errors that appears during the block (in)validation.
--
-- GET /chains/<chain_id>/invalid_blocks/<block_hash>
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-chains-chain-id-invalid-blocks-block-hash
type GetInvalidBlock
  =  "chains"
  :> Capture "chain_id" ChainId
  :> "invalid_blocks"
  :> Capture "block_hash" BlockHash
  :> Get '[JSON] InvalidBlock

-- | Remove an invalid block for the tezos storage
--
-- DELETE /chains/<chain_id>/invalid_blocks/<block_hash>
-- http://tezos.gitlab.io/mainnet/api/rpc.html#delete-chains-chain-id-invalid-blocks-block-hash
type DeleteInvalidBlock
  =  "chains"
  :> Capture "chain_id" ChainId
  :> "invalid_blocks"
  :> Capture "block_hash" BlockHash
  :> Get '[JSON] NoContent
