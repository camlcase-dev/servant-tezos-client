{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeOperators #-}

module Tezos.Client.Shell.Workers where

import Servant.API
import Tezos.Client.Types

-- | Introspect the state of the block_validator worker.
--
-- GET /workers/block_validator
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-workers-block-validator
type GetWorkersBlockValidator
  = "workers"
  :> "block_validator"
  :> Get '[JSON] BlockValidator

-- | Lists the chain validator workers and their status.
--
-- GET /workers/chain_validators
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-workers-chain-validators
type GetWorkersChainValidators
  = "workers"
  :> "chain_validators"
  :> Get '[JSON] [ChainValidatorMini]

-- | Introspect the state of a chain validator worker.
--
-- GET /workers/chain_validators/<chain_id>
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-workers-chain-validators-chain-id
type GetWorkersChainValidator
  = "workers"
  :> "chain_validators"
  :> Capture "chain-id" ChainId
  :> Get '[JSON] ChainValidator -- need better name

-- | Lists the peer validator workers and their status.
--
-- GET /workers/chain_validators/<chain_id>/peers_validators
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-workers-chain-validators-chain-id-peers-validators
type GetWorkersPeersValidators
  = "workers"
  :> "chain_validators"
  :> Capture "chain-id" ChainId
  :> "peers_validators"
  :> Get '[JSON] [PeerValidatorMini]

-- | Introspect the state of a peer validator worker.
--
-- GET /workers/chain_validators/<chain_id>/peers_validators/<peer_id>
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-workers-chain-validators-chain-id-peers-validators-peer-id
type GetWorkersPeersValidator
  = "workers"
  :> "chain_validators"
  :> Capture "chain-id" ChainId
  :> "peers_validators"
  :> Capture "peer-id" PeerId
  :> Get '[JSON] PeerValidator

-- | Lists the Prevalidator workers and their status.
--
-- GET /workers/prevalidators
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-workers-prevalidators
type GetWorkersPrevalidators
  = "workers"
  :> "prevalidators"
  :> Get '[JSON] [ChainValidatorMini]

-- | Introspect the state of prevalidator workers.
--
-- GET /workers/prevalidators/<chain_id>
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-workers-prevalidators-chain-id
type GetWorkersPrevalidator
  = "workers"
  :> "prevalidators"
  :> Capture "chain-id" ChainId
  :> Get '[JSON] Prevalidator
