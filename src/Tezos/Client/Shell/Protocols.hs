{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeOperators #-}

module Tezos.Client.Shell.Protocols where

import Servant.API
import Tezos.Client.Types

-- | GET /protocols
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-protocols
type GetProtocols
  = "protocols"
  :> Get '[JSON] [ProtocolHash]

-- | GET /protocols/<Protocol_hash>
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-protocols-protocol-hash
type GetProtocol
  = "protocols"
  :> Capture "protocol-hash" ProtocolHash
  :> Get '[JSON] ProtocolHashData
