{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeOperators #-}

module Tezos.Client.Shell.Injection where

import Data.Text (Text)
import Servant.API
import Tezos.Client.Types
import Tezos.Client.Extension.Servant (RealJSON)

-- |
--
-- POST /injection/block?[async]&[force]&[chain=<chain_id>]
-- http://tezos.gitlab.io/mainnet/api/rpc.html#post-injection-block
type PostInjectionBlock
  =  "injection"
  :> "block"
  :> QueryFlag "async"
  :> QueryFlag "force"
  :> QueryParam "chain_id" ChainId
  :> ReqBody '[RealJSON] InjectionBlock
  :> Post '[JSON] BlockHash

-- |
-- 
-- POST /injection/operation?[async]&[chain=<chain_id>]
-- http://tezos.gitlab.io/mainnet/api/rpc.html#post-injection-operation
type PostInjectionOperation
  =  "injection"
  :> "operation"
  :> QueryFlag "async"
  :> QueryParam "chain" ChainId
  :> ReqBody '[RealJSON] Text
  :> Post '[JSON] OperationHash

-- |
--
-- POST /injection/protocol?[async]&[force]
-- http://tezos.gitlab.io/mainnet/api/rpc.html#post-injection-protocol
type PostInjectionProtocol
  =  "injection"
  :> "protocol"
  :> QueryFlag "async"
  :> QueryFlag "force"
  :> ReqBody '[RealJSON] InjectionProtocol
  :> Post '[JSON] ProtocolHash
