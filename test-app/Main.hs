{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
module Main where

import Data.Proxy
import Network.HTTP.Client (newManager)
import Network.HTTP.Client.TLS    (tlsManagerSettings)
import Servant.Client (BaseUrl(BaseUrl), Scheme(Https), ClientM, mkClientEnv, runClientM, client)
import Tezos.Client.Types(ChainId(ChainIdMain), BlockId(BlockIdHead), Block)
import Tezos.Client.Protocol.Alpha (GetBlock)

tezosHost :: String
tezosHost = "mainnet-tezos.giganode.io"
tezosPort :: Int
tezosPort = 443

myCall :: ClientM Block
myCall = getBlock ChainIdMain BlockIdHead

getBlock :: ChainId -> BlockId -> ClientM Block
getBlock  = client (Proxy :: Proxy GetBlock)

main :: IO ()
main = do
  manager' <- newManager tlsManagerSettings
  res <- runClientM myCall
       $ mkClientEnv manager'
       $ BaseUrl Https tezosHost tezosPort ""
  case res of
    Left err -> putStrLn $ "Error: " ++ show err
    Right books -> print books
