import Network.HTTP.Client (newManager, defaultManagerSettings)
import Servant.Client (BaseUrl(BaseUrl), Scheme(Http), ClientM, client, mkClientEnv, runClientM, ClientEnv)
import Servant.API (NoContent)
import Data.Proxy (Proxy(Proxy))
import Tezos.Client.Protocol.Alpha
import Tezos.Client.Shell
import Tezos.Client.Types
import qualified Data.Text as T

manager' <- newManager defaultManagerSettings
let manager'' = (mkClientEnv manager' (BaseUrl Http "127.0.0.1" 9323 ""))
let r = flip runClientM manager''

-- ~/alphanet.sh start --rpc-port 9323

{-
type GetBlockchainHeads
  =  "chains"
  :> Capture "chain_id" ChainId
  :> "blocks"
  :> QueryParam "length" Int
  :> QueryParam "head" BlockHash
  :> QueryParam "min_date" Date
  :> Get '[JSON] [[BlockHash]]
-}

getBlockchainHeads :: ChainId -> Maybe Int -> Maybe BlockHash -> Maybe Timestamp -> ClientM [[BlockHash]]
getBlockchainHeads = client (Proxy :: Proxy GetBlockchainHeads)

r (getBlockchainHeads (ChainId . InternalByteString $ "NetXgtSLGNJvNye" ) Nothing Nothing Nothing)

r (getBlockchainHeads (ChainId . InternalByteString $ "main" ) Nothing Nothing Nothing)

r (getBlockchainHeads (ChainId . InternalByteString $ "main" ) (Just 5) (Just $ BlockHash $ InternalByteString "BLGPcAAKXcehjb8sgNyiySQ7uLHpaZ8sXkMXHJTc5EtmLqni7d1") Nothing)

r (getBlockchainHeads (ChainId . InternalByteString $ "main" ) (Just 5) (Just $ BlockHash $ InternalByteString "BLzP2q7jvng49QfEHvP7zMtSzEkSVeHEFfwJqSU7A476CZabur7") Nothing)

r (getBlockchainHeads (ChainId . InternalByteString $ "main" ) Nothing (Just $ BlockHash $ UnistringText "BMWWQWiBJM3um7GJhWxCNhfpNS7wkbYGFHH7s5sjCjSprqXgXPG") Nothing
-- (Just $ BlockHash "BLGPcAAKXcehjb8sgNyiySQ7uLHpaZ8sXkMXHJTc5EtmLqni7d1")

{-
Right [[BlockHash {unBlockHash = "BLGPcAAKXcehjb8sgNyiySQ7uLHpaZ8sXkMXHJTc5EtmLqni7d1"}],[BlockHash {unBlockHash = "BLzP2q7jvng49QfEHvP7zMtSzEkSVeHEFfwJqSU7A476CZabur7"}],[BlockHash {unBlockHash = "BM1kPNPSxxLFY48GKhXV6D2J7JYrrnnXmqLfz3F3KFLmgbRCMXe"}],[BlockHash {unBlockHash = "BLa5RTkj5hNmgXhm1fwLyW5oycmFWtMBTvKFmW69PTZuRc82QEQ"}],[BlockHash {unBlockHash = "BKuUT6SJZzMXQZMHFP2VRDk6EBpanwwhv1tNMaWcsJcJVQuEtV8"}],[BlockHash {unBlockHash = "BL4uJ47755g6VS8zDE9eQGNhZGkmqPyGXh8H4rCizotSxQmzAcw"}],[BlockHash {unBlockHash = "BM4tFFqwyHx48giH74eNXykDBTnC1BEyRsDjQJQBaiCaa2MRmVs"}],[BlockHash {unBlockHash = "BLPyVbofYJ179PinduwZFSNNPadXNEGzAKPmD4qFCdVQGhrQdj2"}],[BlockHash {unBlockHash = "BLHtFccpvzUYaYnZD2s5HpsDiq8K7vVzmH3NoUPVLzmMePMn9Uj"}],[BlockHash {unBlockHash = "BMeVTErp7XdcDCXNTwnFNwHnXFoxbLJwdTMr5vdx3MGRsBXQ9Rb"}],[BlockHash {unBlockHash = "BKj3TDJmTvrGvecvohTgVsSUe4khhZMJXrkQqXs8yR71oHfjMcV"}],[BlockHash {unBlockHash = "BL6r9CtQL3gJZS2jNRBsQQHrWj67HYmL7jDv59XmZBhdaBd1mqc"}],[BlockHash {unBlockHash = "BLkbmvpc2HHRfazvZqDMibRdGuhPGvFWbHeqvY8mMd7Xb9MrpPu"}],[BlockHash {unBlockHash = "BL68bKNoUmDpjDQGUSXGPmTPGXxbK9H1UbDCQG8iP7JH4ARjjBD"}],[BlockHash {unBlockHash = "BLFkmQbCk3Z3DLygTZdRwrzofYgeWQ7q5uQBrgPPUVXeFND42dn"}],[BlockHash {unBlockHash = "BMPaxCbn8vCD7NzoaD2YEuxng7rGaoKFam9ADwPxzAZ1GwK3VVd"}],[BlockHash {unBlockHash = "BL18DzXosi34jPB6nFUKqMYvVqZPDbWoySmDhniQhhZrdBhnUbL"}]]

-}


getBlockChainId :: ChainId -> ClientM ChainId
getBlockChainId = client (Proxy :: Proxy GetBlockchainId)

r (getBlockChainId (ChainId . InternalByteString $ "NetXgtSLGNJvNye" ))

r (getBlockChainId (ChainId . InternalByteString $ "main" ))

getInvalidBlocks :: ChainId -> ClientM [InvalidBlock]
getInvalidBlocks = client (Proxy :: Proxy GetInvalidBlocks)

r (getInvalidBlocks (ChainId . InternalByteString $ "NetXgtSLGNJvNye" ))
r (getInvalidBlocks (ChainId . InternalByteString $ "main" ))

getInvalidBlock :: ChainId -> BlockHash -> ClientM InvalidBlock
getInvalidBlock c = client (Proxy :: Proxy GetInvalidBlock) c

r (getInvalidBlock (ChainId . InternalByteString $ "NetXgtSLGNJvNye" ) (BlockHash . InternalByteString $ "BM9rhpdKmvEdZ2dTbKeZnBtJiEiokBML6HE52pjJNUAN4jg6L49"))

deleteInvalidBlock :: ChainId -> BlockHash -> ClientM NoContent
deleteInvalidBlock = client (Proxy :: Proxy DeleteInvalidBlock)

getNetworkConnections :: ClientM [P2PConnection]
getNetworkConnections = client (Proxy :: Proxy GetNetworkConnections)

r getNetworkConnections

getNetworkConnectionPeer :: PeerId -> ClientM P2PConnection
getNetworkConnectionPeer = client (Proxy :: Proxy GetNetworkConnectionPeer)

r (getNetworkConnectionPeer . PeerId . InternalByteString $ "idtW5BsMRVZdEA5DKAJ4yyWpSB77Dh")

getNetworkPoints :: Maybe P2PPointStateFilter -> ClientM [(Unistring, P2PPoint)]
getNetworkPoints = client (Proxy :: Proxy GetNetworkPoints)

r $ getNetworkPoints Nothing

getNetworkPoint :: Point -> ClientM P2PPoint
getNetworkPoint = client (Proxy :: Proxy GetNetworkPoint)


-- 157.47.14.167:9732
r $ getNetworkPoint $ PointIPV4 "157.47.14.167" 9732


getNetworkSelf :: ClientM Text
getNetworkSelf = client (Proxy :: Proxy GetNetworkSelf)

r getNetworkSelf

getNetworkStat :: ClientM NetworkStat
getNetworkStat = client (Proxy :: Proxy GetNetworkStat)
  
r getNetworkStat

getNetworkVersion :: ClientM [AnnouncedVersion]
getNetworkVersion = client (Proxy :: Proxy GetNetworkVersion)

r getNetworkVersion

getProtocols :: ClientM [ProtocolHash]
getProtocols = client (Proxy :: Proxy GetProtocols)

r getProtocols

getProtocol :: ProtocolHash -> ClientM ProtocolHashData
getProtocol = client (Proxy :: Proxy GetProtocol)

r (getProtocol $ ProtocolHash $ InternalByteString "Ps6mwMrF2ER2s51cp9yYpjDcuzQjsc2yAz8bQsRgdaRxw4Fk95H")

getWorkersBlockValidator :: ClientM BlockValidator
getWorkersBlockValidator = client (Proxy :: Proxy GetWorkersBlockValidator)

r getWorkersBlockValidator

getWorkersChainValidators :: ClientM [ChainValidatorMini]
getWorkersChainValidators = client (Proxy :: Proxy GetWorkersChainValidators)

r getWorkersChainValidators

getWorkersChainValidator :: ChainId -> ClientM ChainValidator
getWorkersChainValidator = client (Proxy :: Proxy GetWorkersChainValidator)

r (getWorkersChainValidator $ ChainId $ InternalByteString $ "NetXgtSLGNJvNye")

getWorkersPeersValidators :: ChainId -> ClientM [PeerValidatorMini]
getWorkersPeersValidators = client (Proxy :: Proxy GetWorkersPeersValidators)

r (getWorkersPeersValidators $ ChainId $ InternalByteString $ "NetXgtSLGNJvNye")

getWorkersPeersValidator :: ChainId -> PeerId -> ClientM PeerValidator
getWorkersPeersValidator c = client (Proxy :: Proxy GetWorkersPeersValidator) c

r (getWorkersPeersValidator (ChainId $ InternalByteString $ "NetXgtSLGNJvNye") (PeerId $ InternalByteString "idtW5BsMRVZdEA5DKAJ4yyWpSB77Dh"))


getWorkersPrevalidators :: ClientM [ChainValidatorMini]
getWorkersPrevalidators = client (Proxy :: Proxy GetWorkersPrevalidators)

r getWorkersPrevalidators

getWorkersPrevalidator :: ChainId -> ClientM Prevalidator
getWorkersPrevalidator = client (Proxy :: Proxy GetWorkersPrevalidator)

r (getWorkersPrevalidator $ ChainId $ InternalByteString "NetXgtSLGNJvNye")



getWorkersPrevalidator :: ChainId -> ClientM Prevalidator
getWorkersPrevalidator = client (Proxy :: Proxy GetWorkersPrevalidator)


getBlock :: ChainId -> BlockId -> ClientM Block
getBlock c = client (Proxy :: Proxy GetBlock) c

getBlockOffset :: ChainId -> BlockId -> Int -> ClientM Block
getBlockOffset c b = client (Proxy :: Proxy GetOperationHashesOffset) c b


r (getBlock ChainIdMain BlockIdHead)
r (getBlockOffset ChainIdMain BlockIdHead 3)

getBlockOperations :: ChainId -> BlockId -> ClientM [[Operation]]
getBlockOperations c = client (Proxy :: Proxy GetBlockOperations) c

r (getBlockOperations ChainIdMain BlockIdHead)

getBlockOperationsOffset :: ChainId -> BlockId -> Integer -> ClientM [Operation]
getBlockOperationsOffset c b = client (Proxy :: Proxy GetBlockOperationsOffset) c b

r (getBlockOperationsOffset ChainIdMain BlockIdHead 2)

{-
type GetContractStorage
  = "chains"
  :> Capture "chain-id" ChainId
  :> "blocks"
  :> Capture "block-id" BlockId
  :> "context"
  :> "contracts"
  :> Capture "contract-id" ContractId
  :> "storage"
  :> Get '[JSON] Expression

curl 127.0.0.1:9323/chains/main/blocks/head/context/contracts/KT1G2D45tpJ9f1iGVwQHqvupv2syhvMqeWPe/storage | jq '.' --sort-keys > getContractStorage.json
-}
getContractStorage :: ChainId -> BlockId -> ContractId -> ClientM Expression
getContractStorage c b = client (Proxy :: Proxy GetContractStorage) c b

{-
Tezos.Client.Shell.Chain GetBlockchainHeads
/chains/main/blocks?[length=]

Tezos.Client.Protocol.Alpha GetBlockOperations
/chains/main/blocks/<block_id>/operations/3

Tezos.Client.Protocol.Alpha.Context GetContractStorage
/chains/main/blocks/<block_id>/context/contracts/<contract_id>/storage


data ChainId
  = ChainId InternalByteString
  | ChainIdMain
  | ChainIdTest
  deriving (Eq, Show, Generic)

data BlockId
  = BlockIdGenesis
  | BlockIdHead
  | BlockId InternalByteString
  | BlockIdGenesisPredecessor Word64
  | BlockIdHeadPredecessor Word64
  | BlockIdPredecessor InternalByteString Word64
  | BlockIdGenesisSuccessor Word64
  | BlockIdHeadSuccessor Word64
  | BlockIdSuccessor InternalByteString Word64
  deriving (Eq,Show,Generic)


curl 127.0.0.1:9323/chains/main/blocks/head
curl 127.0.0.1:9323/chains/main/blocks/genesis
curl 127.0.0.1:9323/chains/main/blocks/hash

curl -vs google.com 2>&1 | less
curl -vs 127.0.0.1:9323/chains/main/blocks/head 2>&1 | less
curl -s 127.0.0.1:9323/chains/main/blocks/head | jq '.' --sort-keys | less

-}


-- monitor blocks: /chains/main/blocks?[length=]
getBlockchainHeads :: ChainId -> Maybe Int -> Maybe BlockHash -> Maybe Timestamp -> ClientM [[BlockHash]]
getBlockchainHeads = client (Proxy :: Proxy GetBlockchainHeads)

r (getBlockchainHeads (ChainId . InternalByteString $ "NetXgtSLGNJvNye" ) Nothing Nothing Nothing)

r (getBlockchainHeads (ChainId . InternalByteString $ "main" ) Nothing Nothing Nothing)

r (getBlockchainHeads (ChainId . InternalByteString $ "main" ) (Just 5) (Just $ BlockHash $ InternalByteString "BLGPcAAKXcehjb8sgNyiySQ7uLHpaZ8sXkMXHJTc5EtmLqni7d1") Nothing)


-- GET ../<block_id>/operations
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-block-id-operations
type GetBlockOperations
  = Capture "block-id" BlockId
  :> "operations"
  :> Get '[JSON] [[Operation]]

-- | All the operations included in `n-th` validation pass of the block.
--
-- GET ../<block_id>/operations/<list_offset>
-- http://tezos.gitlab.io/mainnet/api/rpc.html#get-block-id-operations-list-offset
type GetBlockOperationsOffset
  = Capture "block-id" BlockId
  :> "operations"
  :> Capture "list_offset" Integer
  :> Get '[JSON] [Operation]


-- list operations in blocks: /chains/main/blocks/<block_id>/operations/3

type GetContractStorage
  = "chains"
  :> Capture "chain-id" ChainId
  :> "blocks"
  :> Capture "block-id" BlockId
  :> "context"
  :> "contracts"
  :> Capture "contract-id" ContractId
  :> "storage"
  :> Get '[JSON] Expression

getContractStorage :: ChainId -> BlockId -> ContractId -> ClientM Expression
getContractStorage c b = client (Proxy :: Proxy GetContractStorage) c b

r (getContractStorage ChainIdMain BlockIdHead (ContractId $ UnistringText "KT1G2D45tpJ9f1iGVwQHqvupv2syhvMqeWPe"))

{-
tezosGoldExchange: KT1G2D45tpJ9f1iGVwQHqvupv2syhvMqeWPe
broker: KT1MkNg2N6XqZmbcE5kq4iVKhqvvdN84LnH1
tokenToTezBroker: KT1Fgu5G6Q4bg6qCu5KqxGXkhrM1if5Q5qYd
tezToTokenBroker: KT1WpW6hifdGhkXZUwNZQLUs41zie3JBrU7g
removeLiquidityBroker: KT1QRQ3gfnCSBnoQA1sFhLFqmHpc27ghkQfT
addLiquidityBroker: KT1WArVoPFdACBj325JwcHUfxPPdqMQYM265
tezosGold: KT1WQAW1sRaykMPYEPpqiL4nrYvdnb8SWTV7
dexterExchangeTezosSilver: KT1NUjuMDLJMeAa9ZSaHQLrDc7fsKrRv2Giq
tokenTezosSilver: KT1R4ga92SLKCwrJEcE6KteU5BCFAH7gnTnP

-}


{-
type PostContractBigMap
  = "chains"
  :> Capture "chain-id" ChainId
  :> "blocks"
  :> Capture "block-id" BlockId
  :> "context"
  :> "contracts"
  :> Capture "contract-id" ContractId
  :> "big_map_get"
  :> ReqBody '[JSON] BigMap
  :> Post '[JSON] (Maybe Expression)

-}

getContractBigMap :: ChainId -> BlockId -> ContractId -> BigMap -> ClientM (Maybe Expression)
getContractBigMap c b cid = client (Proxy :: Proxy PostContractBigMap) c b cid

let bigMapQuery =
      BigMap
        -- key
        (StringExpression (UnistringText "tz1Z1nn9Y7vzyvtf6rAYMPhPNGqMJXw88xGH"))
        -- type
        (ContractExpression (PrimitiveType PTAddress) Nothing Nothing)

r (getContractBigMap ChainIdMain BlockIdHead (ContractId $ UnistringText "KT1G2D45tpJ9f1iGVwQHqvupv2syhvMqeWPe") bigMapQuery)

r (getContractBigMap ChainIdMain BlockIdHead (ContractId $ UnistringText "KT1WQAW1sRaykMPYEPpqiL4nrYvdnb8SWTV7") bigMapQuery)

{-
KT1WQAW1sRaykMPYEPpqiL4nrYvdnb8SWTV7

type GetBlockHeader
  = "chains"
  :> Capture "chain-id" ChainId
  :> "blocks"
  :> Capture "block-id" BlockId
  :> "header"
  :> Get '[JSON] BlockHeader

curl 127.0.0.1:9323/chains/main/blocks/head/header
-}

getBlockHeader :: ChainId -> BlockId -> ClientM BlockHeader
getBlockHeader c = client (Proxy :: Proxy GetBlockHeader) c

r (getBlockHeader ChainIdMain BlockIdHead)

getBlockchainHeads :: ChainId -> Maybe Int -> Maybe BlockHash -> Maybe Timestamp -> ClientM [[BlockHash]]
getBlockchainHeads = client (Proxy :: Proxy GetBlockchainHeads)

{-
POST ../<block_id>/context/contracts/<contract_id>/big_map_get

curl -X POST -H "Content-Type: application/json" -d "{\"key\":{\"string\":\"tz1Z1nn9Y7vzyvtf6rAYMPhPNGqMJXw88xGH\"},\"type\":{\"prim\":\"address\"}}" 127.0.0.1:9323/chains/main/blocks/head/context/contracts/KT1G2D45tpJ9f1iGVwQHqvupv2syhvMqeWPe/big_map_get
-}

postContractBigMap :: ChainId -> BlockId -> ContractId -> BigMap -> ClientM (Maybe Expression)
postContractBigMap c b cid = client (Proxy :: Proxy PostContractBigMap) c b cid

r (postContractBigMap ChainIdMain BlockIdHead (ContractId $ UnistringText "KT1WQAW1sRaykMPYEPpqiL4nrYvdnb8SWTV7") (BigMap (StringExpression "tz1Z1nn9Y7vzyvtf6rAYMPhPNGqMJXw88xGH") (ContractExpression (PrimitiveInstruction PIAddress) Nothing Nothing)))


getContractBigMap :: ChainId -> BlockId -> ContractId -> BigMap -> ClientM (Maybe Expression)
getContractBigMap c b cid = client (Proxy :: Proxy PostContractBigMap) c b cid

let bigMapQuery =
      BigMap
        -- key
        (StringExpression (UnistringText "tz1Z1nn9Y7vzyvtf6rAYMPhPNGqMJXw88xGH"))
        -- type
        (ContractExpression (PrimitiveType PTAddress) Nothing Nothing)

r (getContractBigMap ChainIdMain BlockIdHead (ContractId $ UnistringText "KT1G2D45tpJ9f1iGVwQHqvupv2syhvMqeWPe") bigMapQuery)
