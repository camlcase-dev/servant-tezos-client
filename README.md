# servant-tezos-client

Data types and servant-client API types for the [Tezos RPCs](http://tezos.gitlab.io/mainnet/api/rpc.html).

## Status

#### Shell

Preliminary implementation. Completely untested.

#### Protocol Alpha

Not ready. Only code scaffolding. 
